<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/classes/Checkout.php';
require_once dirname(__FILE__) . '/classes/Orders.php';

require_once dirname(__FILE__) . '/classes/GroupCommission.php';
require_once dirname(__FILE__) . '/classes/TransactionHistory.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();


$uid = $_SESSION['uid'];

$id = getOrders($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1 ",array("uid"),array($uid),"s");
$asd = $id[0]->getId();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];



if($_SERVER['REQUEST_METHOD'] == 'POST'){

    $id = rewrite($_POST["order_id"]);
    $username = rewrite($_POST["insert_username"]);
    $bankName = rewrite($_POST["insert_bankname"]);
    $bankAccountHolder = rewrite($_POST["insert_bankaccountholder"]);
    $bankAccountNo = rewrite($_POST["insert_bankaccountnumber"]);
    $name = rewrite($_POST["insert_name"]);
    $contactNo = rewrite($_POST["insert_contactNo"]);
    $address_1 = rewrite($_POST["insert_address_1"]);
    $address_2 = rewrite($_POST["insert_address_2"]);
    $city = rewrite($_POST["insert_city"]);
    $zipcode = rewrite($_POST["insert_zipcode"]);
    $state = rewrite($_POST["insert_state"]);
    $country = rewrite($_POST["insert_country"]);
}

if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart']){
    $productListHtml = getShoppingCart($conn,2);
}else
{}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://bossinternational.asia/checkout.php" />
<meta property="og:title" content="Information | Boss" />
<title>Information | Boss</title>
<meta property="og:description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
<meta name="description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
<meta name="keywords" content="Impotence, Boss, man, men sex, sexual, cure, product, unable to ejaculate, Penile Erectile Dysfunction, Sexual Desire Disorder, Sexual Intercourse Disorder, low sexual desire,阳痿,性冷淡,性功能障碍,不举,  etc">
<link rel="canonical" href="https://bossinternational.asia/checkout.php" />
<?php include 'css.php'; ?>
<?php require_once dirname(__FILE__) . '/header.php'; ?>
</head>

<body class="body">


<!-- Start Menu -->
<?php include 'header-sherry.php'; ?>
      
<div class="flex-container">    
    <div class="left-status-div">
    	<!-- Start of Check Out Status Div-->
        <div class="check-out-status">
            <div class="status-container">
                <a href="viewCart.php" class="status-a">
                    <div class="black-round-div"><img src="img/tick2.png" class="yellow-tick" alt="Completed" title="Completed"></div>
                    CART
                </a>
            </div>
            <div class="status-div">></div>
            <div class="status-container">        
                <div class="white-round-div">2</div>
                SHIPPING INFORMATION
            </div>
            <div class="status-div">></div>
            <div class="status-container">        
                <div class="white-round-div">3</div>
                <!-- SHIPPING -->
                PAYMENT
            </div>        
        

        </div>
        <!-- End of Check Out Status Div-->
        <div class="clear"></div>

    <form method="POST"  action="shipping.php">
        <p class="info-title"><b>CONTACT INFORMATION</b></p>

        <input class="clean white-input two-box-input" type="hidden" id="order_id" name="order_id" value="<?php echo $asd;?>">
        <input class="clean white-input two-box-input" type="hidden" id="insert_username" name="insert_username" value="<?php echo $userDetails->getUsername();?>">
        <input class="clean white-input two-box-input" type="hidden" id="insert_bankname" name="insert_bankname" value="<?php echo $userDetails->getBankName();?>">
        <input class="clean white-input two-box-input" type="hidden" id="insert_bankaccountholder" name="insert_bankaccountholder" value="<?php echo $userDetails->getBankAccountHolder();?>">
        <input class="clean white-input two-box-input" type="hidden" id="insert_bankaccountnumber" name="insert_bankaccountnumber" value="<?php echo $userDetails->getBankAccountNo();?>">
        
        <input class="clean white-input two-box-input" type="text" id="insert_name" name="insert_name" placeholder="Name">
        <input class="clean white-input two-box-input" type="number" id="insert_contactNo" name="insert_contactNo"  placeholder="Contact Number"> 

 
        <p class="info-title spacing2"><b>SHIPPING ADDRESS</b></p>      
        
            <input class="clean white-input two-box-input" type="text" id="insert_address_1" name="insert_address_1" placeholder="Address Line 1">
            <input class="clean white-input two-box-input" type="text" id="insert_address_2" name="insert_address_2" placeholder="Address Line 2">
            <input class="clean white-input half-white-input left-half two-box-input-double" type="text" id="insert_city" name="insert_city" placeholder="City">  
            <input class="clean white-input half-white-input two-box-input-double right-part" type="number" id="insert_zipcode" name="insert_zipcode" placeholder="Postcode"> 
            <input class="clean white-input two-box-input" type="text" id="insert_state" name="insert_state" placeholder="State">
            <input class="clean white-input two-box-input" type="text" id="insert_country" name="insert_country" placeholder="Country">
                    
        <div class="clear"></div>
        <div class="cart-bottom-div spacing2">
            <div class="left-cart-bottom-div">
                <p class="continue-shopping pointer continue2"><a href="viewCart.php" class="black-white-link"><img src="img/back.png" class="back-btn" alt="back" title="back" > Return to Cart</a></p>
            </div>
            <div class="right-cart-div">
            <button class="clean black-button add-to-cart-btn checkout-btn">Continue To Shipping</button>
            </div>
        </div>
    </form>
    </div>

    
    
    
    <div class="right-status-div">   
        
    <?php echo $productListHtml; ?>
    </div>	
</div>    


<?php include 'js.php'; ?>


<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Please Fill Up The Required Details !";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "Fail To Make Order !";
        }

        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>