<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/TransferPointReport.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$conn = connDB();
$uid = $_SESSION['uid'];

$adminList = getTransferPointReport($conn," WHERE receive_uid = ? ",array("receive_uid"),array($uid),"s");
$adminList2 = getTransferPointReport($conn," WHERE send_uid = ? ",array("send_uid"),array($uid),"s");
$userDetails = getUser($conn,"WHERE uid = ?",array("uid"),array($uid),"s");
//for memberlist
// $adminList = getUser($conn," WHERE user_type = ? ",array("user_type"),array(1),"i");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://bossinternational.asia/transferPointReport.php" />
    <meta property="og:title" content="Transfer Point Report | Boss" />
    <title>Transfer Point Report | Boss</title>
    <meta property="og:description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="keywords" content="Impotence, Boss, man, men sex, sexual, cure, product, unable to ejaculate, Penile Erectile Dysfunction, Sexual Desire Disorder, Sexual Intercourse Disorder, low sexual desire,阳痿,性冷淡,性功能障碍,不举,  etc">
    <link rel="canonical" href="https://bossinternational.asia/transferPointReport.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php include 'header-sherry.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">
    <!-- <h1 class="h1-title h1-before-border shipping-h1">Transfer Point Report</h1> -->
    <h1 class="h1-title h1-before-border shipping-h1"><?php echo _MAINJS_XFERPTSREP_XFERPTSREPORT ?></h1>

    <div class="width100 shipping-div2">
    	<div class="overflow-scroll-div">
        <?php   if($adminList)
          { ?>
            <!-- <h3 class="h1-title h1-before-border shipping-h1">Receive :</h3> -->
            <h3 class="h1-title h1-before-border shipping-h1"><?php echo _MAINJS_XFERPTSREP_RECEIVE ?> :</h3>
            <table class="shipping-table">
                <thead>
                    <tr>
                        <!-- <th>NO.</th>
                        <th>SENDING NAME</th>
                        <th>ACCEPT NAME</th>
                        <th>DATE</th>
                        <th>TIME</th>
                        <th>SENDING AMOUNT (Pts)</th>
                        <th>STATUS</th> -->

                        <!-- <th>NO.</th>
                        <th>SENDING NAME</th>
                        <th>ACCEPT NAME</th>
                        <th>DATE</th>
                        <th>TIME</th>
                        <th>SENDING AMOUNT (Pts)</th>
                        <th>STATUS</th> -->


                        <th><?php echo _MAINJS_XFERPTSREP_NO ?></th>
                        <th><?php echo _MAINJS_XFERPTSREP_SENT_NAME ?></th>
                        <th><?php echo _MAINJS_XFERPTSREP_ACCPET_NAME ?></th>
                        <th><?php echo _MAINJS_XFERPTSREP_DATE ?></th>
                        <th><?php echo _MAINJS_XFERPTSREP_TIME ?></th>
                        <th><?php echo _MAINJS_XFERPTSREP_SENT_AMOUNT ?></th>
                        <th><?php echo _MAINJS_XFERPTSREP_STATUS ?></th>
                    </tr>
                </thead>
                <tbody>

                <?php

                    for($cnt = 0;$cnt < count($adminList) ;$cnt++)
                    {?>
                        <tr class="link-to-details">
                            <td><?php echo ($cnt+1)?></td>
                              <td><?php echo $adminList[$cnt]->getSendName();?></td>
                            <td><?php echo $adminList[$cnt]->getReceiveName();?></td>
                            <td>
                                <?php $dateCreated = date("Y-m-d",strtotime($adminList[$cnt]->getDateCreated()));echo $dateCreated;?>
                            </td>
                            <td>
                                <?php $timeCreated = date("h:m:s",strtotime($adminList[$cnt]->getDateCreated()));echo $timeCreated;?>
                            </td>
                            <td><?php echo $adminList[$cnt]->getSendAmount();?></td>
                            <td><?php echo $adminList[$cnt]->getStatus();?></td>



                        </tr>
                        <?php
                    }
                }else { ?>

            <?php    }
                ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="clear"></div>


    <div class="width100 shipping-div2">
    	<div class="overflow-scroll-div">
        <?php    if($adminList2)
          { ?>
            <!-- <h3 class="h1-title h1-before-border shipping-h1">Send :</h3> -->
            <h3 class="h1-title h1-before-border shipping-h1"><?php echo _MAINJS_XFERPTSREP_SEND ?> :</h3>

            <table class="shipping-table">
                <thead>
                    <tr>
                        <!-- <th>NO.</th>
                        <th>SENDING NAME</th>
                        <th>ACCEPT NAME</th>
                        <th>DATE</th>
                        <th>TIME</th>
                        <th>SENDING AMOUNT (Pts)</th>
                        <th>STATUS</th> -->

                        <th><?php echo _MAINJS_XFERPTSREP_NO ?></th>
                        <th><?php echo _MAINJS_XFERPTSREP_SENT_NAME ?></th>
                        <th><?php echo _MAINJS_XFERPTSREP_ACCPET_NAME ?></th>
                        <th><?php echo _MAINJS_XFERPTSREP_DATE ?></th>
                        <th><?php echo _MAINJS_XFERPTSREP_TIME ?></th>
                        <th><?php echo _MAINJS_XFERPTSREP_SENT_AMOUNT ?></th>
                        <th><?php echo _MAINJS_XFERPTSREP_STATUS ?></th>

                    </tr>
                </thead>
                <tbody>

                <?php

                    for($cnt = 0;$cnt < count($adminList2) ;$cnt++)
                    {?>
                        <tr class="link-to-details">
                            <td><?php echo ($cnt+1)?></td>
                              <td><?php echo $adminList2[$cnt]->getSendName();?></td>
                            <td><?php echo $adminList2[$cnt]->getReceiveName();?></td>
                            <td>
                                <?php $dateCreated = date("Y-m-d",strtotime($adminList2[$cnt]->getDateCreated()));echo $dateCreated;?>
                            </td>
                            <td>
                                <?php $timeCreated = date("h:m:s",strtotime($adminList2[$cnt]->getDateCreated()));echo $timeCreated;?>
                            </td>
                            <td><?php echo $adminList2[$cnt]->getSendAmount();?></td>
                            <td><?php echo $adminList2[$cnt]->getStatus();?></td>



                        </tr>
                        <?php
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>


    <div class="width100 shipping-div2">
    	<div class="overflow-scroll-div">
        <?php    if( !$adminList && !$adminList2)
          { ?>
            <!-- <h3 class="h1-title h1-before-border shipping-h1">Send :</h3> -->

            <table class="shipping-table">
                <thead>
                    <tr>
                        <!-- <th>NO.</th>
                        <th>SENDING NAME</th>
                        <th>ACCEPT NAME</th>
                        <th>DATE</th>
                        <th>TIME</th>
                        <th>SENDING AMOUNT (Pts)</th>
                        <th>STATUS</th> -->

                        <th><?php echo _MAINJS_XFERPTSREP_NO ?></th>
                        <th><?php echo _MAINJS_XFERPTSREP_SENT_NAME ?></th>
                        <th><?php echo _MAINJS_XFERPTSREP_ACCPET_NAME ?></th>
                        <th><?php echo _MAINJS_XFERPTSREP_DATE ?></th>
                        <th><?php echo _MAINJS_XFERPTSREP_TIME ?></th>
                        <th><?php echo _MAINJS_XFERPTSREP_SENT_AMOUNT ?></th>
                        <th><?php echo _MAINJS_XFERPTSREP_STATUS ?></th>

                    </tr>
                </thead>
                <tbody>

                <?php

                  ?>

                        <?php


                ?>
                </tbody>
            </table><br><center>  <div class= "width100 oveflow">
                <div class="width20">
                    <div class="white50div">
                <!-- <?php //echo "*There is No Transfer Point report For Now." ?> -->
                <?php echo _MAINJS_XFERPTSREP_NO_REPORT ?>
              </div>
          </div>
        </div></center><?php } ?>
        </div>
    </div>

    <div class="clear"></div>

    <!-- <div class="bottom-big-container">
    	<div class="left-btm-page">
        	Page <select class="clean transparent-select"><option>1</option></select> of 1
        </div>
        <div class="middle-btm-page">
        	<a class="round-black-page">1</a>
            <a class="round-white-page">2</a>
        </div>
        <div class="right-btm-page">
        	Total: 2
        </div>
    </div>  -->

</div>


<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

        if($_GET['type'] == 1 )
        {
            $messageType = "Successfully Add New Admin";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "Error";
        }
        echo '

        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>

        ';
        $_SESSION['messageType'] = 0;
}
?>

</body>
</html>
