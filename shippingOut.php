<?php
// if (session_id() == ""){
//     session_start();
// }
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

// $id = $_SESSION['order_id'];

$conn = connDB();

$productsOrders =  getProductOrders($conn);

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    if(isset($_POST["shipping_method"])){
        $shipping_method = rewrite($_POST["shipping_method"]);
        $tracking_number = rewrite($_POST["tracking_number"]);
        $shipping_date = rewrite($_POST["shipping_date"]);
        $shipping_status = rewrite($_POST["shipping_status"]);
        $order_id = rewrite($_POST["order_id"]);
    }else{
        $shipping_method = "";
        $tracking_number = "";
        $shipping_date = "";
        $shipping_status = "";
        $order_id = "";
    }
}

$conn->close();
function promptError($msg)
{
    echo '<script>  alert("'.$msg.'");  </script>';
}

function promptSuccess($msg)
{
    echo '<script>  alert("'.$msg.'");   </script>';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://bossinternational.asia/shippingOut.php" />
    <meta property="og:title" content="Shipping Out | Boss" />
    <title>Shipping Out | Boss</title>
    <meta property="og:description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="keywords" content="Impotence, Boss, man, men sex, sexual, cure, product, unable to ejaculate, Penile Erectile Dysfunction, Sexual Desire Disorder, Sexual Intercourse Disorder, low sexual desire,阳痿,性冷淡,性功能障碍,不举,  etc">
    <link rel="canonical" href="https://bossinternational.asia/shippingOut.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php include 'header-sherry.php'; ?>



<div class="yellow-body padding-from-menu same-padding">
<form method="POST" id="idOfForm" onsubmit="doPreview(this.submited); return false;">
<!-- <form method="POST"  id="idOfForm"> -->
<!-- <form method="post" action="send.php" id="idOfForm"> -->
    <h1 class="details-h1" onclick="goBack()">
        <a class="black-white-link2 hover1">
            <img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">
            <img src="img/back2.png" class="back-btn2 hover1b" alt="back" title="back">
            Order Number : #<?php echo $_POST['order_id'];?>
        </a>
    </h1>


    <div class="width100 shipping-div2">
        <table class="details-table">
            <tbody>
            <?php
            if(isset($_POST['order_id']))
            {
                $conn = connDB();
                //Order
                $orderArray = getOrders($conn,"WHERE id = ? ", array("id") ,array($_POST['order_id']),"i");
                //OrderProduct
                $orderProductArray = getProductOrders($conn,"WHERE order_id = ? ", array("order_id") ,array($_POST['order_id']),"i");
                //$orderDetails = $orderArray[0];

                if($orderArray != null)
                {
                    ?>
                    <tr>
                        <td>Name</td>
                        <td>:</td>
                        <td><?php echo $orderArray[0]->getName()?></td>
                        <!-- <td><?php //echo $orderArray[0]->getPrpduvtID->getName()?></td> -->
                    </tr>
                    <tr>
                        <td>Contact</td>
                        <td>:</td>
                        <td><?php echo $orderArray[0]->getContactNo()?></td>
                    </tr>
                    <!-- <tr>
                        <td>Order ID</td>
                        <td>:</td>
                        <td>#<?php //echo $orderArray[0]->getId()?></td>
                    </tr> -->
                    <tr>
                        <td>Ship To</td>
                        <td>:</td>
                        <td>
                            <?php echo $orderArray[0]->getAddressLine1()?>
                            <?php echo $orderArray[0]->getAddressLine2()?>
                            <?php echo $orderArray[0]->getCity()?>
                            <?php echo $orderArray[0]->getZipcode()?>
                            <?php echo $orderArray[0]->getState()?>
                            <?php echo $orderArray[0]->getCountry()?>
                        </td>
                    </tr>
                    <tr>
                        <td>Total Price</td>
                        <td>:</td>
                        <td>RM <?php echo $orderArray[0]->getSubtotal()?></td>
                    </tr>
                    <?php

                }
            }
            else
            {}
            $conn->close();
            ?>
            </tbody>
        </table>
    </div>

    <div class="width100 shipping-div2">
        <table class="shipping-table">
            <thead>
                <tr>
                    <th>NO.</th>
                    <th>PRODUCT NAME</th>
                    <th>QUANTITY</th>
                </tr>
            </thead>
            <tbody>
            <?php
            if(isset($_POST['order_id']))
            {
                $conn = connDB();
                //Order
                $orderArray = getOrders($conn,"WHERE id = ? ", array("id") ,array($_POST['order_id']),"i");
                //OrderProduct
                $orderProductArray = getProductOrders($conn,"WHERE order_id = ? ", array("order_id") ,array($_POST['order_id']),"i");
                //$orderDetails = $orderArray[0];

                //Product Details
                //$abc = getProduct($conn,"WHERE product_id = ? ", array("product_id") ,array($_POST['order_id']),"i");

                if($orderArray != null)
                {
                    for($cntAA = 0;$cntAA < count($orderProductArray) ;$cntAA++)
                    {
                        
                    $product = getProduct($conn ,"WHERE id = ? ", array("name"),array($orderProductArray[$cntAA]->getProductId()),"i");    
                        ?>
                    <tr>
                        <!-- <input type="hidden" name="order_id" value="<?php //echo $_POST['order_id'];?>">  -->
                        <td><?php echo ($cntAA+1)?></td>
                        <td><?php echo $product[0] -> getName(); ?></td>
                        <td><?php echo $orderProductArray[$cntAA]->getQuantity();?></td>
                    </tr>
                    <?php
                    }
                }
            }
            else
            {}
            $conn->close();
            ?>
            </tbody>
        </table>
    </div>

    <div class="search-container0">
            <div class="shipping-input clean smaller-text2 three-input">
                <p>Shipping Method</p>
                <select class="shipping-input2 clean normal-input same-height-with-date" type="text" id="shipping_method" name="shipping_method">
                    <option value="POSLAJU" name="POSLAJU">POSLAJU</option>
                    <option value="SKYNET" name="SKYNET">SKYNET</option>
                </select>
            </div>

            <div class="shipping-input clean smaller-text2 middle-shipping-div second-shipping three-input">
                <p>Tracking Number</p>
                <input class="shipping-input2 clean normal-input same-height-with-date" type="text" id="tracking_number" name="tracking_number" placeholder="Tracking Number">
            </div>

            <div class="shipping-input clean smaller-text2 three-input">
                <!-- <p>Default Date</p> -->
                <p>Shipping Date</p>
                <input class="shipping-input2 clean normal-input same-height-with-date" type="date" id="shipping_date" name="shipping_date">
            </div>
            <div class="clear"></div>

            <input class="shipping-input2 clean normal-input same-height-with-date" type="hidden" id="order_id" name="order_id" value="<?php echo $orderArray[0]->getId()?>">
            <input class="shipping-input2 clean normal-input same-height-with-date" type="hidden" id="shipping_status" name="shipping_status" value="SHIPPED">
    </div>

    <div class="clear"></div>

    <!-- <div class="search-container0">
            <div class="shipping-input clean smaller-text2 three-input">
                <input class="shipping-input2 clean normal-input same-height-with-date" type="hidden" id="order_id" name="order_id" value="<?php //echo $orderArray[0]->getId()?>">
            </div>
            <div class="shipping-input clean smaller-text2 middle-shipping-div second-shipping three-input">
                <input class="shipping-input2 clean normal-input same-height-with-date" type="hidden" id="shipping_status" name="shipping_status" value="shipped">
            </div>
    </div>  -->

    <div class="clear"></div>

    <div class="three-btn-container">
        <!-- <a href="reject.php" class="shipout-btn-a black-button three-btn-a"><b>REJECT</b></a> -->
        <!-- <button input type="submit" name="submitabc" value="ShipOut" class="shipout-btn-a black-button three-btn-a" onclick="reject();">Reject</button> -->
        <!-- <a href="refund.php" class="refund-btn-a white-button three-btn-a"><b>REFUND</b></a> -->
        <!-- <input onclick="this.form.submited=this.value;"  type="submit" name="REJECT" value="REJECT" class="reject-btn-a white-button three-btn-a"> -->
        <input onclick="this.form.submited=this.value;"  type="submit" name="REJECT AND REFUND" value="REJECT AND REFUND" class="refund-btn-a white-button three-btn-a">
        <input onclick="this.form.submited=this.value;"  type="submit" name="SHIPOUT" value="SHIP OUT" class="shipout-btn-a black-button three-btn-a">
    </div>
</form>
</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<script type="text/javascript">
    function doPreview(buttonType)
    {
        switch(buttonType){
            // case 'REJECT':
            //     form=document.getElementById('idOfForm');
            //     form.action='shippingReject.php';
            //     form.submit();
            // break;
            case 'REJECT AND REFUND':
                form=document.getElementById('idOfForm');
                form.action='shippingRefund.php';
                form.submit();
            break;
            case 'SHIP OUT':
                form=document.getElementById('idOfForm');
                form.action='utilities/updateShippingFunction.php';
                form.submit();
            break;
        }
        
    }
</script>

<script>
function goBack() {
  window.history.back();
}
</script>

</body>
</html>
