<div class="footer-div width100 same-padding overflow">
	<p class="yellow-text footer-p"><?php echo _MAINJS_JS_FOOTER ?></p>
</div>

<!-- Modal Box HTML -->
<!-- Login Modal -->
<div id="login-modal" class="modal-css">
<?php
  $oilxag_uname = null;
  $oilxag_password = null;

	$uid = $_SESSION['uid'];

if(isset($_COOKIE['remember-oilxag']) && isset($_COOKIE['email-oilxag']) && isset($_COOKIE['password-oilxag']))
{
  echo'saddasassadsaddsasadsa';

    echo $oilxag_uname = $_COOKIE['email-oilxag'];
    echo $oilxag_password = $_COOKIE['password-oilxag'];
}
else
{
  echo $oilxag_uname = '';
  echo $oilxag_password = '';
}


?>

  <!-- Modal content -->
  <div class="modal-content-css login-modal-content">
    <span class="close-css close-login">&times;</span>
    <h1 class="white-h1"><?php echo _MAINJS_JS_LOGIN ?></h1>
    <form class="login-form" method="POST" action="utilities/loginFunction.php">
    	<div class="input-grey-div">
            <span class="input-span"><img src="img/user.png" class="login-input-icon" alt="<?php echo _MAINJS_JS_USERNAME ?>" title="<?php echo _MAINJS_JS_USERNAME ?>"></span>
        	<input class="login-input name-input clean" type="text" placeholder="<?php echo _MAINJS_JS_USERNAME ?>" name="email" value="<?php echo $oilxag_uname;?>">
        </div>
    	<div class="input-grey-div">
            <span class="input-span"><img src="img/lock.png" class="login-input-icon" alt="<?php echo _MAINJS_JS_PASSWORD ?>" title="<?php echo _MAINJS_JS_PASSWORD ?>"></span>
        	<input class="login-input password-input clean" type="password" placeholder="<?php echo _MAINJS_JS_PASSWORD ?>" name="password" id="login_password" value="<?php echo $oilxag_password;?>">
            <span class="visible-span"><img src="img/visible.png" class="login-input-icon" alt="<?php echo _MAINJS_JS_VIEW_PASSWORD ?>" title="<?php echo _MAINJS_JS_VIEW_PASSWORD ?>" id="login_view_password"></span>
        </div>
        <?php
        if(isset($_COOKIE['remember-oilxag']) && $_COOKIE['remember-oilxag'] == 1)
        {
          ?>
            <div class="left-check"><input checked type="checkbox" class="checkbox-remember-me" name="remember-me"> <p class="remember-me-p"> <?php echo _MAINJS_JS_REMEMBER_ME ?></p></div>
          <?php
        }
        else
        {
          ?>
            <div class="left-check"><input type="checkbox" class="checkbox-remember-me" name="remember-me"> <p class="remember-me-p"> <?php echo _MAINJS_JS_REMEMBER_ME ?></p></div>
          <?php
        }
        ?>
        <div class="right-forgot"><a class="open-forgot forgot-password-a"><?php echo _MAINJS_JS_FORGOT_PASSWORD ?></a></div>
        <div class="clear"></div>
        <button class="yellow-button clean" name="loginButton" type="submit"><?php echo _MAINJS_JS_LOGIN ?></button>
    </form>
  </div>

</div>

<!-- Notice Modal -->
<div id="notice-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css notice-modal-content">
    <span class="close-css close-notice" id="close-notice">&times;</span>
    <h1 class="menu-h1" id="noticeTitle">Title Here</h1>
	<div class="menu-link-container" id="noticeMessage">Message Here</div>
  </div>

</div>
<!--- End part 1--->

<!-- Menu Modal -->
<div id="menu-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css menu-modal-content">
    <span class="close-css close-menu">&times;</span>
    <h1 class="menu-h1">MENU</h1>
	<div class="menu-link-container"><a href="#about" class="mobile-menu close-menu">About</a></div>
    <div class="menu-link-container"><a href="#products" class="mobile-menu close-menu">Products</a></div>
    <div class="menu-link-container"><a href="#contact" class="mobile-menu close-menu">Contact Us</a></div>
    <div class="menu-link-container"><a href="faq.php" class="mobile-menu">FAQ</a></div>
    <div class="menu-link-container"><a class="mobile-menu open-login">Login</a></div>
    <!--<div class="menu-link-container"><a class="mobile-menu open-register">Sign Up</a></div>    -->
  </div>

</div>



<!-- Modal Box HTML -->
<!-- Forgot Password Modal -->
<div id="forgot-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css forgot-modal-content login-modal-content">
    <span class="close-css close-forgot">&times;</span>
    <h1 class="white-h1"><?php echo _MAINJS_JS_FORGOT_PASSWORD_TITLE ?></h1>
    <form class="login-form" method="POST" action="utilities/forgotEmailFunction.php">
    	<div class="input-grey-div">
            <span class="input-span"><img src="img/email2.png" class="login-input-icon" alt="<?php echo _MAINJS_INDEX_EMAIL ?>" title="<?php echo _MAINJS_INDEX_EMAIL ?>"></span>
        	<input class="login-input name-input clean" type="email" placeholder="<?php echo _MAINJS_INDEX_EMAIL ?>" required name="forgotPassword_email">
        </div>
        <div class="clear"></div>
        <button class="yellow-button clean"><?php echo _MAINJS_JS_SUBMIT ?></button>
        <div class="text-center width100 link-container"><a class="open-login yellow-link create-a"><?php echo _MAINJS_JS_LOGIN_AGAIN ?></a></div>
    </form>
  </div>

</div>

<!-- Menu Login Modal -->
<div id="menu-login-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css menu-modal-content">
    <span class="close-css close-menu-login">&times;</span>
    <h1 class="menu-h1">MENU</h1>
    <!-- <div class="menu-link-container"><a href="#about" class="mobile-menu close-menu">About</a></div>
    <div class="menu-link-container"><a href="#products" class="mobile-menu close-menu">Products</a></div>
    <div class="menu-link-container"><a href="#contact" class="mobile-menu close-menu">Contact Us</a></div>
    <div class="menu-link-container"><a href="faq.php" class="mobile-menu">FAQ</a></div> -->
    <div class="menu-link-container"><a href="announcement.php" class="mobile-menu">Announcement</a></div>
    <div class="menu-link-container"><a href="profile.php" class="mobile-menu">My Profile</a></div>
    <div class="menu-link-container"><a href="editPassword.php" class="mobile-menu">Edit Password</a></div>
    <div class="menu-link-container"><a class="mobile-menu open-epin">Edit E-Pin</a></div>
    <div class="menu-link-container"><a href="referee.php" class="mobile-menu">My Referee</a></div>
    <div class="menu-link-container"><a href="addReferee.php" class="mobile-menu">Add Referee</a></div>
    <!-- <div class="menu-link-container"><a href="product.php" class="mobile-menu">Purchase</a></div>
    <div class="menu-link-container"><a href="purchaseHistory.php" class="mobile-menu">Purchase History</a></div> -->
    <div class="menu-link-container"><a href="wallet.php" class="mobile-menu">My Wallet</a></div>
    <div class="menu-link-container"><a href="logout.php" class="mobile-menu">Logout</a></div>
  </div>

</div>

<!-- Withdraw Points Modal -->
<div id="withdraw-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css login-modal-content withdraw-modal-content">
    <span class="close-css close-withdraw">&times;</span>
    <h1 class="white-h1">WITHDRAWAL</h1>
    <form method="POST"  action="utilities/withdrawAmountFunction.php">
    <!-- Wen Jie, help them fill in their bank details-->
    	<div class="input-grey-div">
            <span class="input-span"><img src="img/bank.png" class="login-input-icon" alt="Bank" title="Bank"></span>
        	<input oninput="this.value = this.value.toUpperCase()" class="login-input name-input clean" type="text" id="bank_name" name="bank_name"  placeholder="Bank" required>
        </div>
    	<div class="input-grey-div">
            <span class="input-span"><img src="img/bank-account-number.png" class="login-input-icon" alt="Account No." title="Account No."></span>
        	<input class="login-input name-input clean" type="text" id="acc_number" name="acc_number"  placeholder="Account No." required>
        </div>
    	<div class="input-grey-div">
            <span class="input-span"><img src="img/user.png" class="login-input-icon" alt="Account No." title="Account No."></span>
        	<input oninput="this.value = this.value.toUpperCase()" class="login-input name-input clean" type="text" id="usernameaccount" name="usernameaccount"  placeholder="Bank Account Holder Name" required>
        </div>
        <div class="input-grey-div">
        	<div class="left-points-div">
                <span class="input-span"><img src="img/coin.png" class="login-input-icon" alt="Points" title="Points"></span>
                <input class="login-input name-input clean" id="withdraw_amount" name="withdraw_amount" type="number" placeholder="Withdraw Amount" value="" >
            </div>

        </div>
		<div class="input-grey-div">
			<span class="input-span"><img src="img/lock.png" class="login-input-icon" alt="E-Pin" title="E-Pin"></span>
			<input class="login-input password-input clean" id="withdraw_epin" type="password" placeholder="E-Pin" name="withdraw_epin"><span class="visible-span"><img src="img/visible.png" class="login-input-icon" alt="View Password" title="View Password" required></span>
		</div>
		<p class="white-text explanation-p">
        	Withdrawal charge: RM 10 per transaction<br>
			Minimum withdrawal amount: RM 100 per transaction
        </p>
        <div class="clear"></div>
        <button class="yellow-button clean" type="submitwithdraw" value="<?php if($_SERVER['REQUEST_METHOD'] == 'POST'){
				    createOrder($conn,$uid);} ?>" >Withdraw</button>

    </form>




  </div>

</div>


<!-- Add E-Pin Modal, if the user first time access anything need e-pin -->
<div id="addepin-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css login-modal-content withdraw-modal-content">
    <span class="close-css close-addepin">&times;</span>
    <h1 class="white-h1">Add E-Pin</h1>
    <form >
    	<div class="input-grey-div">
            <span class="input-span"><img src="img/lock.png" class="login-input-icon" alt="E-Pin" title="E-Pin"></span>
        	<input class="login-input password-input clean" type="password" placeholder="New E-Pin"><span class="visible-span"><img src="img/visible.png" class="login-input-icon" alt="View Password" title="View Password" ></span>
        </div>
    	<div class="input-grey-div">
            <span class="input-span"><img src="img/lock.png" class="login-input-icon" alt="E-Pin" title="E-Pin"></span>
        	<input class="login-input password-input clean" type="password" placeholder="Retype New E-Pin"><span class="visible-span"><img src="img/visible.png" class="login-input-icon" alt="View Password" title="View Password" ></span>
        </div>
        <div class="clear"></div>
        <button class="yellow-button clean" type="submit">Confirm</button>

    </form>
  </div>

</div>



<!-- Change E-Pin Modal -->
<div id="epin-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css login-modal-content withdraw-modal-content">
    <span class="close-css close-epin">&times;</span>
    <h1 class="white-h1">Edit E-Pin</h1>
    <form >
    	<div class="input-grey-div">
            <span class="input-span"><img src="img/lock.png" class="login-input-icon" alt="E-Pin" title="E-Pin"></span>
        	<input class="login-input password-input clean" type="password" placeholder="Current E-Pin"><span class="visible-span"><img src="img/visible.png" class="login-input-icon" alt="View Password" title="View Password" ></span>
        </div>
    	<div class="input-grey-div">
            <span class="input-span"><img src="img/lock.png" class="login-input-icon" alt="E-Pin" title="E-Pin"></span>
        	<input class="login-input password-input clean" type="password" placeholder="New E-Pin"><span class="visible-span"><img src="img/visible.png" class="login-input-icon" alt="View Password" title="View Password" ></span>
        </div>
    	<div class="input-grey-div">
            <span class="input-span"><img src="img/lock.png" class="login-input-icon" alt="E-Pin" title="E-Pin"></span>
        	<input class="login-input password-input clean" type="password" placeholder="Retype New E-Pin"><span class="visible-span"><img src="img/visible.png" class="login-input-icon" alt="View Password" title="View Password" ></span>
        </div>
        <div class="clear"></div>
        <button class="yellow-button clean" type="submit">Confirm</button>

    </form>
  </div>

</div>



<script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/jquery.fancybox.js" type="text/javascript"></script>
<script src="js/headroom.js" type="text/javascript"></script>

<script type="text/javascript" src="js/main.js?version=1.0.1"></script>

<script>
    (function() {
        var header = new Headroom(document.querySelector("#header"), {
            tolerance: 5,
            offset : 205,
            classes: {
              initial: "animated",
              pinned: "slideDown",
              unpinned: "slideUp"
            }
        });
        header.init();

    }());
</script>

	<script>
    // Cache selectors
    var lastId,
        topMenu = $("#top-menu"),
        topMenuHeight = topMenu.outerHeight(),
        // All list items
        menuItems = topMenu.find("a"),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function(){
          var item = $($(this).attr("href"));
          if (item.length) { return item; }
        });

    // Bind click handler to menu items
    // so we can get a fancy scroll animation
    menuItems.click(function(e){
      var href = $(this).attr("href"),
          offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
      $('html, body').stop().animate({
          scrollTop: offsetTop
      }, 500);
      e.preventDefault();
    });

    // Bind to scroll
    $(window).scroll(function(){
       // Get container scroll position
       var fromTop = $(this).scrollTop()+topMenuHeight;

       // Get id of current scroll item
       var cur = scrollItems.map(function(){
         if ($(this).offset().top < fromTop)
           return this;
       });
       // Get the id of the current element
       cur = cur[cur.length-1];
       var id = cur && cur.length ? cur[0].id : "";

    });
    </script>

<script>
$(document).ready(function(){
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});
</script>


<!--- Modal Box --->
<script>
var loginmodal = document.getElementById("login-modal");
var menumodal = document.getElementById("menu-modal");

var forgotmodal = document.getElementById("forgot-modal");
var sendverification = document.getElementById("send-verification");
var menuloginmodal = document.getElementById("menu-login-modal");
var productmodal = document.getElementById("product-modal");
var withdrawmodal = document.getElementById("withdraw-modal");
var convertmodal = document.getElementById("convert-modal");
var transfermodal = document.getElementById("transfer-modal");
var addepinmodal = document.getElementById("addepin-modal");
var epinmodal = document.getElementById("epin-modal");
var cynomodal = document.getElementById("cyno-modal");

var openlogin = document.getElementsByClassName("open-login")[0];
var openlogin1 = document.getElementsByClassName("open-login")[1];
var openlogin2 = document.getElementsByClassName("open-login")[2];
var openmenu = document.getElementsByClassName("open-menu")[0];

var openforgot = document.getElementsByClassName("open-forgot")[0];
var openmenulogin = document.getElementsByClassName("open-menu-login")[0];
var openproduct = document.getElementsByClassName("open-product")[0];
var openwithdraw = document.getElementsByClassName("open-withdraw")[0];
var openconvert = document.getElementsByClassName("open-convert")[0];
var opentransfer = document.getElementsByClassName("open-transfer")[0];
var openepin = document.getElementsByClassName("open-epin")[0];
var openepin1 = document.getElementsByClassName("open-epin")[1];
var opencyno = document.getElementsByClassName("open-cyno")[0];

var closelogin = document.getElementsByClassName("close-login")[0];
var closemenu = document.getElementsByClassName("close-menu")[0];
var closemenu1 = document.getElementsByClassName("close-menu")[1];
var closemenu2 = document.getElementsByClassName("close-menu")[2];
var closemenu3 = document.getElementsByClassName("close-menu")[3];
var closeforgot = document.getElementsByClassName("close-forgot")[0];
var closemenulogin = document.getElementsByClassName("close-menu-login")[0];
var closproduct = document.getElementsByClassName("close-product")[0];
var closewithdraw = document.getElementsByClassName("close-withdraw")[0];
var closeconvert = document.getElementsByClassName("close-convert")[0];
var closetransfer = document.getElementsByClassName("close-transfer")[0];
var closeaddepin = document.getElementsByClassName("close-addepin")[0];
var closeepin = document.getElementsByClassName("close-epin")[0];
var closecyno = document.getElementsByClassName("close-cyno")[0];

if(openlogin){
openlogin.onclick = function() {
  loginmodal.style.display = "block";
}
}
if(openlogin1){
openlogin1.onclick = function() {
  loginmodal.style.display = "block";
  menumodal.style.display = "none";
}
}

if(openlogin2){
openlogin2.onclick = function() {
  loginmodal.style.display = "block";
  forgotmodal.style.display = "none";
}
}

if(openmenu){
    openmenu.onclick = function() {
        menumodal.style.display = "block";
    }
}

if(openforgot){
openforgot.onclick = function() {
  forgotmodal.style.display = "block";
  loginmodal.style.display = "none";
}
}
if(openmenulogin){
openmenulogin.onclick = function() {
  menuloginmodal.style.display = "block";
}
}
if(openproduct){
openproduct.onclick = function() {
  productmodal.style.display = "block";
}
}
if(openwithdraw){
openwithdraw.onclick = function() {
  withdrawmodal.style.display = "block";
}
}
if(openconvert){
openconvert.onclick = function() {
  convertmodal.style.display = "block";
}
}
if(opentransfer){
opentransfer.onclick = function() {
  transfermodal.style.display = "block";
}
}

if(openepin){
openepin.onclick = function() {
  epinmodal.style.display = "block";
}
}
if(openepin1){
openepin1.onclick = function() {
  epinmodal.style.display = "block";
  menuloginmodal.style.display = "none";
}
}
if(opencyno){
opencyno.onclick = function() {
  cynomodal.style.display = "block";
}
}
//if(isset($_GET['referrerUID']))
//{
  //echo" registermodal.style.display = 'block';";
//}

if(closelogin){
closelogin.onclick = function() {
  loginmodal.style.display = "none";
}
}
if(closemenu){
closemenu.onclick = function() {
  menumodal.style.display = "none";
}
}
if(closemenu1){
closemenu1.onclick = function() {
  menumodal.style.display = "none";
}
}
if(closemenu2){
closemenu2.onclick = function() {
  menumodal.style.display = "none";
}
}
if(closemenu3){
closemenu3.onclick = function() {
  menumodal.style.display = "none";
}
}
if(closeforgot){
closeforgot.onclick = function() {
  forgotmodal.style.display = "none";
}
}
if(closemenulogin){
closemenulogin.onclick = function() {
  menuloginmodal.style.display = "none";
}
}

if(closewithdraw){
closewithdraw.onclick = function() {
  withdrawmodal.style.display = "none";
}
}
if(closeconvert){
closeconvert.onclick = function() {
  convertmodal.style.display = "none";
}
}
if(closetransfer){
closetransfer.onclick = function() {
  transfermodal.style.display = "none";
}
}
if(closeaddepin){
closeaddepin.onclick = function() {
  addepinmodal.style.display = "none";
}
}
if(closeepin){
closeepin.onclick = function() {
  epinmodal.style.display = "none";
}
}
if(closecyno){
closecyno.onclick = function() {
  cynomodal.style.display = "none";
}
}
window.onclick = function(event) {
  if (event.target == loginmodal) {
    loginmodal.style.display = "none";
  }
  if (event.target == menumodal) {
    menumodal.style.display = "none";
  }
  if (event.target == forgotmodal) {
    forgotmodal.style.display = "none";
  }
  if (event.target == menuloginmodal) {
    menuloginmodal.style.display = "none";
  }
  if (event.target == productmodal) {
    productmodal.style.display = "none";
  }
  if (event.target == withdrawmodal) {
    withdrawmodal.style.display = "none";
  }
  if (event.target == convertmodal) {
    convertmodal.style.display = "none";
  }
  if (event.target == transfermodal) {
    transfermodal.style.display = "none";
  }
  if (event.target == addepinmodal) {
    addepinmodal.style.display = "none";
  }
  if (event.target == epinmodal) {
    epinmodal.style.display = "none";
  }
  if (event.target == cynomodal) {
    cynomodal.style.display = "none";
  }  
}
</script>


<!-- JS Create Account -->
<script>
  function checkIfVariableIsNullOrEmptyString(field,isValidate)
  {
    if(field == null || field == "" || field.length == 0)
    {
      isValidate += 1;
      return isValidate;
    }
    else
    {
      return isValidate;
    }
  }
  function createAccountValidation()
  {
    // First Level Validation
    let isValidatedAndCanProceedToNextLevel = 0;

    let register_username = $('#register_username').val();
    let register_email_user = $('#register_email_user').val();
    let register_password = $('#register_password').val();
    let register_retype_password = $('#register_retype_password').val();
    let register_email_referrer = $('#register_email_referrer').val();


    // console.log('Initial Counter = '+isValidatedAndCanProceedToNextLevel);

    isValidatedAndCanProceedToNextLevel = checkIfVariableIsNullOrEmptyString(register_username,isValidatedAndCanProceedToNextLevel);
    // console.log('counter = '+isValidatedAndCanProceedToNextLevel);

    isValidatedAndCanProceedToNextLevel = checkIfVariableIsNullOrEmptyString(register_email_user,isValidatedAndCanProceedToNextLevel);
    // console.log('counter = '+isValidatedAndCanProceedToNextLevel);

    isValidatedAndCanProceedToNextLevel = checkIfVariableIsNullOrEmptyString(register_password,isValidatedAndCanProceedToNextLevel);
    // console.log('counter = '+isValidatedAndCanProceedToNextLevel);

    isValidatedAndCanProceedToNextLevel = checkIfVariableIsNullOrEmptyString(register_retype_password,isValidatedAndCanProceedToNextLevel);
    // console.log('counter = '+isValidatedAndCanProceedToNextLevel);

    // Second Level Validation
    if(isValidatedAndCanProceedToNextLevel == 0)
    {
      // alert('can continue');
      if(register_password.length >= 6)
      {
        if(register_password == register_retype_password)
        {
          if(register_email_referrer == null || register_email_referrer == "" || register_email_referrer.length == 0)
          {

          }
          else
          {
            if(emailIsValid (register_email_referrer))
            {

            }
            else
            {
              alert('Referrer`s email is not valid ! Please try again ! ');
              event.preventDefault();
            }
          }
        }
        else
        {
          alert('Password does not match ! Please try again ! ');
          event.preventDefault();
        }
      }
      else
      {
        alert('Password must be more than 5 ! Please try again ! ');
        event.preventDefault();
      }
    }
    else
    {
      alert('Please enter all fields required ! ');
      event.preventDefault();
    }
  }
  function emailIsValid (email)
  {
    return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)
  }
</script>
<script>
  viewPassword( document.getElementById('login_view_password'), document.getElementById('login_password'));
  viewPassword( document.getElementById('register_password_img'), document.getElementById('register_password'));
  viewPassword( document.getElementById('register_retype_password_img'), document.getElementById('register_retype_password'));
</script>

<script src="js/jquery.min.js"></script>

<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.dlmenu.js"></script>
		<script>
			$(function() {
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
				});
			});
		</script>