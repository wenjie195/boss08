<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Images.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Withdrawal.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';


$conn = connDB();


$adminList = getWithdrawReq($conn," WHERE withdrawal_status = 'PENDING' ",array("withdrawal_status"),"i");

if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
if(isset($_POST["withdrawal_details"])){
        $withdrawalStatus = rewrite($_POST["withdrawal_status"]);
        $withdrawalMethod = rewrite($_POST["withdrawal_method"]);
        $withdrawalAmount = rewrite($_POST["withdrawal_accepted"]);
        $withdrawalNote = rewrite($_POST["withdrawal_note"]);

        $withdrawalNumber = rewrite($_POST["withdrawal_number"]);
      }else {
        $withdrawalStatus = "";
        $withdrawalMethod = "";
        $withdrawalAmount = "";
        $withdrawalNote = "";

        $withdrawalNumber = "";
      }
    }


// $products = getProduct($conn);
//
// $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
// $userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://bossinternational.asia/withdrawalReq.php" />
    <meta property="og:title" content="Withdrawal Requester  Details| Boss" />
    <title>Withdrawal Requester  Details | Boss</title>
    <meta property="og:description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="keywords" content="Impotence, Boss, man, men sex, sexual, cure, product, unable to ejaculate, Penile Erectile Dysfunction, Sexual Desire Disorder, Sexual Intercourse Disorder, low sexual desire,阳痿,性冷淡,性功能障碍,不举,  etc">
    <link rel="canonical" href="https://bossinternational.asia/withdrawalReq.php" />
    <?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header-sherry.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">
    <form method="POST" action="utilities/withdrawRequestFunction.php" enctype="multipart/form-data">

	<h1 class="details-h1" onclick="goBack()">
    	<a class="black-white-link2 hover1 gold-text">
    		<img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">
            <img src="img/back2.png" class="back-btn2 hover1b" alt="back" title="back">
        	<?php echo _MAINJS_ADMWD_WITHDRAWAL_NO ?> : #<?php echo $_POST['withdrawal_number'];?>
        </a>
    </h1>
        <table class="details-table">
            <tbody>
                <?php
                if(isset($_POST['withdrawal_number']))
                {
                    $conn = connDB();
                    //Order
                    $withdrawArray = getWithdrawReq($conn,"WHERE withdrawal_number = ? ", array("withdrawal_number") ,array($_POST['withdrawal_number']),"i");
                    $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($withdrawArray[0]->getUid()),"s");
                    $userDetails = $userRows[0];

                    if($withdrawArray != null && $userDetails !=null)
                    {?>
                        <tr>
                            <td><?php echo _MAINJS_ADMWD_USERNAME ?></td>
                            <td>:</td>
                            <td><?php echo $withdrawArray[0]->getOwnerUsername() ?></td>
                        </tr>
                        <tr>
                            <td><?php echo _MAINJS_ADMWD_BANK ?></td>
                            <td>:</td>
                            <td><?php echo $withdrawArray[0]->getBankName() ?></td>
                        </tr>
                        <tr>
                            <td><?php echo _MAINJS_PROFILE_ACCOUNT_HOLDER_NAME ?></td>
                            <td>:</td>
                            <td><?php echo $withdrawArray[0]->getUsername() ?></td>
                        </tr>
                        <tr>
                            <td><?php echo _MAINJS_PROFILE_ACCOUNT_NUMBER ?></td>
                            <td>:</td>
                            <td><?php echo $withdrawArray[0]->getAccNumber() ?></td>
                        </tr>
                        <tr>
                            <td><?php echo _MAINJS_ADMWD_AMOUNT_2 ?></td>
                            <td>:</td>
                            <td><?php echo $withdrawArray[0]->getWithdrawRequestAmount() ?></td>
                        </tr>
                        <tr>
                            <td><?php echo _MAINJS_ADMWD_CONTACT_2 ?></td>
                            <td>:</td>
                            <td><?php echo $withdrawArray[0]->getContact() ?></td>
                        </tr>
                        <tr>
                            <td><?php echo _MAINJS_ADMWD_REQUEST_DATE ?></td>
                            <td>:</td>
                            <td><?php echo $withdrawArray[0]->getDateCreated() ?></td>
                        </tr>
                    <?php
                    }
                }
                else
                {}
                $conn->close();
                ?>
            </tbody>
        </table>

        <div class="search-container0">

        <div class="shipping-input clean smaller-text2 fifty-input ow-mbtm">
            <p><?php echo _MAINJS_ADMWD_METHOD ?></p>
            <select class="shipping-input2 clean normal-input same-height-with-date" type = "text" id="withdrawal_method" name="withdrawal_method">
                <option>Online Banking</option>
            </select>
        </div>

        <div class="shipping-input clean smaller-text2 fifty-input ow-mbtm">
            <p><?php echo _MAINJS_ADMWD_AMOUNT_3 ?></p>
            <input class="shipping-input2 clean normal-input same-height-with-date" type="number" readonly placeholder="Key in Amount" id="withdrawal_accepted" name="withdrawal_accepted" value="<?php echo $withdrawArray[0]->getWithdrawRequestAmount() ?>">
        </div>

        <div class="shipping-input clean smaller-text2 fifty-input ow-mbtm">
            <p><?php echo _MAINJS_ADMWD_REFERENCES ?></p>
            <input oninput="this.value = this.value.toUpperCase()" class="shipping-input2 clean normal-input same-height-with-date" type="text" placeholder="Bank In Reference" id="withdrawal_note" name="withdrawal_note" required>
        </div>

        <div class="clear"></div>

        <div class="upload-btn-wrapper">
            <button class="upload-btn"><?php echo _MAINJS_ADMWD_UPLOAD_RECEIPT ?></button>
            <input class="hidden-input" type="file" name="file" />
        </div>

        <input class="shipping-input2 clean normal-input same-height-with-date" type="hidden" id="withdrawal_number" name="withdrawal_number" value="<?php echo $withdrawArray[0]->getWithdrawalNumber()?>">

        <div class="clear"></div>

        <div class="three-btn-container">
            <button a href="rejectWith.php" class="shipout-btn-a black-button three-btn-a" type="submit" id = "withdrawal_status1" name = "withdrawal_status1" value = "REJECTED" ><b><?php echo _MAINJS_ADMWD_REJECT ?></b></a></button>
            <button class="shipout-btn-a black-button three-btn-a" type="submit" id = "withdrawal_status" name = "withdrawal_status" value = "ACCEPTED" ><b><?php echo _MAINJS_ADMWD_CONFIRM ?></b></a></button>
        </div>

        </div>

    </form>
</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>
<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Withdraw Request Success!";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "The Withdraw Money Should Not Pass the Amount On The Wallet!!";
        }
        // if($_GET['type'] == 2)
        // {
        //     $messageType = "The Amount Should Atleast RM10 Should Left On The Wallet!!";
        // }
        if($_GET['type'] == 3)
        {
            $messageType = "Wrong E-Pin!";
        }
        if($_GET['type'] == 4)
        {
            $messageType = "You Cant Proceed To Convert the Cash To Point Until Previous Withdraw Request Has Completed!!";
        }
        if($_GET['type'] == 5)
        {
            $messageType = "Transfer Cash To Point Success";
        }
        if($_GET['type'] == 6)
        {
            $messageType = "You Cant Proceed To Withdraw the Cash Until Previous Withdraw Request Has Completed!!";
        }
        if($_GET['type'] == 7)
        {
            $messageType = "No Existing IC Number!!";
        }
        if($_GET['type'] == 8)
        {
            $messageType = "The Point Succesfully Transfer!!";
        }
        if($_GET['type'] == 9)
        {
            $messageType = "Atleast 10 Points Should Left On The Wallet!!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Complete Your Profile To Withdraw The Cash!!";
        }
        if($_GET['type'] == 12)
        {
            $messageType = "Withdraw Error!";
        }
        if($_GET['type'] == 12)
        {
            $messageType = "Picture upload failed, please try again.";
        }
        if($_GET['type'] == 13)
        {
            $messageType = "Please select a picture.";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }


}
?>

<script>
function goBack() {
  window.history.back();
}
</script>
</body>
</html>
