<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://bossinternational.asia/editPin.php" />
    <meta property="og:title" content="Edit Pin | Boss" />
    <title>Edit Pin | Boss</title>
    <meta property="og:description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="keywords" content="Impotence, Boss, man, men sex, sexual, cure, product, unable to ejaculate, Penile Erectile Dysfunction, Sexual Desire Disorder, Sexual Intercourse Disorder, low sexual desire,阳痿,性冷淡,性功能障碍,不举,  etc">
    <link rel="canonical" href="https://bossinternational.asia/editPin.php" />
    <?php include 'css.php'; ?>    
</head>
<body class="body">
<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">

    <form class="edit-profile-div2" action="utilities/changeEPinFunction.php" method="POST">

    <div class="left-div">
        <p class="continue-shopping pointer continue2"><a href="profile.php" class="black-white-link"><img src="img/back.png" class="back-btn" alt="back" title="back" > <?php echo _MAINJS_EPIN_BACK ?></a></p>
    </div>

        <!-- <h2 class="profile-title">CHANGE E-PIN</h2> -->
        <h2 class="profile-title"><?php echo _MAINJS_EPIN_CHANGE ?></h2>
        <table class="edit-profile-table password-table">
        	<tr class="profile-tr">
                <!-- <td class="profile-td1">Current E-Pin</td> -->
                <td class="profile-td1"><?php echo _MAINJS_EPIN_CURRENT_EPIN ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3">
                    <input required name="editEPin_current" id="editEPin_current" class="clean edit-profile-input" type="password">
                    <!-- <input required name="editPassword_current" id="editPassword_current" class="clean edit-profile-input" type="password"> -->
                    <span class="visible-span2">
                        <img src="img/visible.png" class="login-input-icon" alt="View Password" title="View Password" id="editPassword_current_img">
                    </span>
                </td>
            </tr>
        	<tr class="profile-tr">
                <!-- <td class="profile-td1">New E-Pin</td> -->
                <td class="profile-td1"><?php echo _MAINJS_EPIN_NEW_EPIN ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3">
                    <input required name="editEPin_new" id="editEPin_new" class="clean edit-profile-input" type="password">
                    <span class="visible-span2">
                        <img src="img/visible.png" class="login-input-icon" alt="View Password" title="View Password" id="editPassword_new_img">
                    </span>
                </td>
            </tr>            
        	<tr class="profile-tr">
                <!-- <td class="profile-td1">Retype New E-Pin</td> -->
                <td class="profile-td1"><?php echo _MAINJS_EPIN_RETYPE_NEW_EPIN ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3">
                    <input required name="editEPin_reenter" id="editEPin_reenter" class="clean edit-profile-input"type="password">
                    <span class="visible-span2">
                        <img src="img/visible.png" class="login-input-icon" alt="View Password" title="View Password" id="editPassword_reenter_img">
                    </span>
                </td>
            </tr>          
        </table>
		<button class="confirm-btn text-center white-text clean black-button"><?php echo _MAINJS_EDITPASS_CONFIRM ?></button>
    </form>
</div>
<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>

<?php 
if(isset($_GET['type']))
{
    $messageType = null;


    if($_SESSION['messageType'] == 2)
    {
        // if($_GET['type'] == 1)
        // {
        //     $messageType = "E-Pin Renew Successfully";
        // }
        if($_GET['type'] == 2)
        {
            $messageType = "Server Problem !";
        }
        if($_GET['type'] == 3)
        {
            $messageType = "New E-Pin Does Not Match !";
        }
        if($_GET['type'] == 4)
        {
            $messageType = "E-Pin Length Must Be More Than 5";
        }
        if($_GET['type'] == 5)
        {
            $messageType = "Current E-Pin Is Not The Same As Previous !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

<script>
  viewPassword( document.getElementById('editPassword_current_img'), document.getElementById('editEPin_current'));
  viewPassword( document.getElementById('editPassword_new_img'), document.getElementById('editEPin_new'));
  viewPassword( document.getElementById('editPassword_reenter_img'), document.getElementById('editEPin_reenter'));
</script>

</body>
</html>