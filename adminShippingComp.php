<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$conn = connDB();

$productsOrders =  getProductOrders($conn);

$conn = connDB();
$post = array();
$start_date = date("Y-m-d");
$end_date = date("Y-m-d");
$username = "";
$phoneNo="";
$trackingNumber="";

$record_per_page = 8;
$page = '';
if(isset($_GET["page"]))
{
 $page = $_GET["page"];
}
else
{
 $page = 1;
}

$start_from = ($page-1)*$record_per_page;

if (isset($_GET["start_date"]) && isset($_GET["end_date"]) && isset($_GET["username"]))
{
	$start_date = $_GET["start_date"];
	$end_date = $_GET["end_date"];
	$username = $_GET["username"];
  $id = $_GET["id"];
  $phoneNo = $_GET["contactNo"];
  $trackingNumber = $_GET["tracking_number"];
  //$phoneNo = $_GET["contactNo"];
	$post = $_GET;
}

$products = getProduct($conn);
$list = GetList($post, $conn,$start_from,$record_per_page);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function GetList($post, $conn,$start_from,$record_per_page)
{
	$sql = "SELECT username,date_updated,shipping_method,shipping_status, ";
	$sql .= "id,contactNo,total,tracking_number ";
	$sql .= "FROM orders ";
	$sql .= "WHERE payment_status = 'ACCEPTED' AND shipping_status != 'PENDING' ";

  if (isset($post['reset'])) {
    if (isset($post["start_date"]) && strlen($post["start_date"]) < 0)
  	{
  		$sql .= "AND DATE(date_updated) >= '" . $post["start_date"] . "' ";
  	}

  	if (isset($post["end_date"]) && strlen($post["end_date"]) < 0)
  	{
  		$sql .= "AND DATE(date_updated) <= '" . $post["end_date"] . "' ";
  	}

  	if (isset($post["username"]) && strlen($post["username"]) < 0)
  	{
      $sql .= "AND username LIKE '%" . $post["username"] . "%' ";
  	}
    if (isset($post["id"]) && strlen($post["id"]) < 0)
  	{
      $sql .= "AND id LIKE '%" . $post["id"] . "%' ";
  	}
    if (isset($post["contactNo"]) && strlen($post["contactNo"]) < 0)
  	{
      $sql .= "AND contactNo LIKE '%" . $post["contactNo"] . "%' ";
  	}
    if (isset($post["tracking_number"]) && strlen($post["tracking_number"]) < 0)
  	{
      $sql .= "AND tracking_number LIKE '%" . $post["tracking_number"] . "%' ";
  	}
  }else {
    if (isset($post["start_date"]))
  	{
  		$sql .= "AND DATE(date_updated) >= '" . $post["start_date"] . "' ";
  	}

  	if (isset($post["end_date"]))
  	{
  		$sql .= "AND DATE(date_updated) <= '" . $post["end_date"] . "' ";
  	}

  	if (isset($post["username"]) && strlen($post["username"]) > 0)
  	{
      $sql .= "AND username LIKE '%" . $post["username"] . "%' ";
  	}
    if (isset($post["id"]) && strlen($post["id"]) > 0)
  	{
      $sql .= "AND id LIKE '%" . $post["id"] . "%' ";
  	}
    if (isset($post["contactNo"]) && strlen($post["contactNo"]) > 0)
  	{
      $sql .= "AND contactNo LIKE '%" . $post["contactNo"] . "%' ";
  	}
    if (isset($post["tracking_number"]) && strlen($post["tracking_number"]) > 0)
  	{
      $sql .= "AND tracking_number LIKE '%" . $post["tracking_number"] . "%' ";
  	}
  }

	$sql .= "ORDER BY date_updated ASC LIMIT $start_from, $record_per_page ";
	//echo $sql;exit;

	$result = $conn->query($sql);
	$output = array();

	if ($result->num_rows > 0)
	{
		// output data of each row
		while($row = $result->fetch_assoc())
		{
			$output[] = $row;
		}
	}

	return $output;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://bossinternational.asia/adminShippingComp.php" />
    <meta property="og:title" content="Shipping Completed | Boss" />
    <title>Shipping Completed | Boss</title>
    <meta property="og:description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="keywords" content="Impotence, Boss, man, men sex, sexual, cure, product, unable to ejaculate, Penile Erectile Dysfunction, Sexual Desire Disorder, Sexual Intercourse Disorder, low sexual desire,阳痿,性冷淡,性功能障碍,不举,  etc">
    <link rel="canonical" href="https://bossinternational.asia/adminShippingComp.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">
	<h1 class="h1-title h1-before-border shipping-h1">Completed | <a href="adminShipping.php" class="white-text title-tab-a">Waiting for Shipping Out</a></h1>
    <!-- This is a filter for the table result -->

    <!-- <select class="filter-select clean">
    	<option class="filter-option">Latest Shipping</option>
        <option class="filter-option">Oldest Shipping</option>
    </select> -->

    <!-- End of Filter -->

    <div class="clear"></div>



	<div class="search-container0 payout-search">
    <form action="adminShippingCompComp.php" type="post">
            <div class="shipping-input clean smaller-text2">
                <p class="white-text">Username</p>
                <input class="shipping-input2 clean normal-input same-height-with-date" type="text" name="username" placeholder="Username"  value="<?php echo $username; ?>">
            </div>
            <div class="shipping-input clean smaller-text2">
                <p class="white-text">Order Number</p>
                <input class="shipping-input2 clean normal-input same-height-with-date" type="number" name="id" placeholder="Order Number"  value="<?php echo $id; ?>">
            </div>
            <div class="shipping-input clean smaller-text2">
                <p class="white-text">Tracking Number</p>
                <input class="shipping-input2 clean normal-input same-height-with-date" type="text" name="tracking_number" placeholder="Tracking Number"  value="<?php echo $trackingNumber; ?>">
            </div>
            <div class="shipping-input clean smaller-text2">
                <p class="white-text">Contact</p>
                <input class="shipping-input2 clean normal-input same-height-with-date" type="number" name="contactNo" placeholder="Contact"  value="<?php echo $phoneNo; ?>">
            </div>
            <div class="shipping-input clean smaller-text2">
                <p class="white-text">Start Date</p>
                <input class="shipping-input2 clean normal-input" name="start_date" type="date" value="<?php echo $start_date; ?>">
            </div>
            <div class="shipping-input clean smaller-text2">
                <p class="white-text">End Date</p>
                <input class="shipping-input2 clean normal-input" name="end_date" type="date" value="<?php echo $end_date; ?>">
            </div>
            <button type="submit" class="clean black-button shipping-search-btn second-shipping same-height-with-date2 mobile-w">Search</button>
            <button type="submit" name="reset" class="clean black-button shipping-search-btn second-shipping same-height-with-date2 mobile-mh mobile-w">Reset</button>
      </form>
  </div>
    <div class="clear"></div>
		<?php

     if (!$list) {
      ?>
      <center>  <div class= "width100 oveflow extra-margin-top">
          <div class="width20">
              <div class="white50div">
          <?php echo "*There is No Shipping Completed For Now." ?>
        </div>
    </div>
    </div></center><?php
  }else {



    $conn = connDB();?>

    <div class="width100 shipping-div2 scroll-div">
        <?php $conn = connDB();?>
            <!-- <table class="shipping-table">     -->
            <table class="shipping-table scroll-table">
                <thead>
                    <tr>
                        <th>NO.</th>
                        <th>ORDER NUMBER</th>
                        <th>USERNAME</th>
                        <th>CONTACT</th>
                        <th>ISSUE DATE</th>
                        <th>METHOD</th>
                        <th>STATUS</th>
                        <th>TRACKING NO.</th>
                        <th>DETAILS</th>
                        <!-- <th>REJECT</th>
                        <th>REFUND</th> -->
                    </tr>
                </thead>
                <tbody>
								<?php if ($list):
                  $ind=0;?>
									<?php foreach ($list AS $ls):
                    $ind++; ?>
										<tr>
											<td><?php echo $ind; ?></td>
											<td>#<?php echo $ls["id"]; ?></td>
                      <td><?php echo $ls["username"]; ?></td>
                      <td><?php echo $ls["contactNo"]; ?></td>
                      <td><?php echo $ls["date_updated"]; ?></td>
                      <td><?php echo $ls["shipping_method"]; ?></td>
                      <td><?php echo $ls["shipping_status"]; ?></td>
                      <td><?php echo $ls["tracking_number"]; ?></td>
                      <td>
                        <?php if ($ls["shipping_status"]=='SHIPPED') {
                          ?><form action="reviewShipping.php" method="POST">
                              <button class="clean edit-anc-btn hover1" type="submit" name="order_id" value="<?php echo $ls["id"];?>">
                                  <!-- <img src="img/shipping1.png" class="edit-announcement-img hover1a" alt="Delivery" title="Delivery">
                                  <img src="img/shipping2.png" class="edit-announcement-img hover1b" alt="Delivery" title="Delivery"> -->
                                  <img src="img/shipping1.png" class="edit-announcement-img hover1a" alt="Shipping Out" title="Review Shipping">
                                  <img src="img/shipping2.png" class="edit-announcement-img hover1b" alt="Shipping Out" title="Review Shipping">
                              </button>
                          </form><?php
                        }else {
                          ?><form action="reviewShippingRefund.php" method="POST">
                              <button class="clean edit-anc-btn hover1" type="submit" name="order_id" value="<?php echo $ls["id"];?>">
                                  <!-- <img src="img/shipping1.png" class="edit-announcement-img hover1a" alt="Delivery" title="Delivery">
                                  <img src="img/shipping2.png" class="edit-announcement-img hover1b" alt="Delivery" title="Delivery"> -->
                                  <img src="img/shipping1.png" class="edit-announcement-img hover1a" alt="Shipping Out" title="Review Shipping">
                                  <img src="img/shipping2.png" class="edit-announcement-img hover1b" alt="Shipping Out" title="Review Shipping">
                              </button>
                          </form><?php
                        } ?>

                      </td>
										</tr>
									<?php endforeach; ?>
								<?php else: ?>
									<tr>
										<!-- <td colspan="7">No result</td> -->
								</tr>
								<?php endif; ?>
                  </tbody>
            </table><?php
            ?>
        <?php //$conn->close();?>
    </div>

  <div class="clear"></div><br>
					<div class="pagination">
						<div class="width100 text-center">
										<ul>
					<?php
					$page_query = "SELECT * FROM orders WHERE payment_status = 'ACCEPTED' AND shipping_status = 'SHIPPED' ORDER BY date_created ASC";
					$page_result = mysqli_query($conn, $page_query);
					$total_records = mysqli_num_rows($page_result);
					$total_pages = ceil($total_records/$record_per_page);
					$start_loop = $page;
					$difference = $total_pages - $page;
					if($difference <= 5)
					{
					 $start_loop = $total_pages - 5;
					}
					$end_loop = $start_loop + 4;
					if($page > 1)
					{
					 echo "<a href='adminShippingComp.php?page=1'><li>First</li></a>";
					 echo "<a href='adminShippingComp.php?page=".($page - 1)."'><li><<</li></a>";
					}
					for($i=1; $i<=$total_pages; $i++)
					{
							echo "<a href='adminShippingComp.php?page=".$i."'><li>".$i."</li></a>";
					}
					if($page <= $end_loop)
					{
					 echo "<a href='adminShippingComp.php?page=".($page + 1)."'><li>>></li></a>";
					 echo "<a href='adminShippingComp.php?page=".$total_pages."'><li>Last</li></a>";
					}


					?>
								</ul>
					</div>
					</div>



        <?php }//$conn->close();?>
    </div>



</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<style>
.pagination {
  	display: inline-block;
	text-align: center;
	width:100%;
}

.pagination li {
  color: white;
  padding: 8px 16px;
  text-decoration: none;
	text-align: center;
  transition:ease-in-out 0.15s;;
  border: 1px solid #000000;
  margin: 0 4px;
	background-color: #000000;
	display:inline-block;
}
.pagination a {
	display:inline-block;
	margin-bottom:10px;
}
.pagination a.active {
  background-color: white;
  color: black;
  border: 1px solid white;
}

.pagination a:hover:not(.active) {
	background-color: white;
  	color: black;
  	border: 1px solid white;}
</style>

</body>
</html>
