<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BossBonusReport.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();
$uid = $_SESSION['uid'];

$adminList = getBossBonusReport($conn,"WHERE receiver_uid = ? AND amount != 200 AND amount != 192 AND amount != 128",array("uid"),array($uid),"s");
$userDetails = getUser($conn,"WHERE uid = ?",array("uid"),array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://bossinternational.asia/bonusReport.php" />
    <meta property="og:title" content="Bonus Report | Boss" />
    <title>Bonus Report | Boss</title>
    <meta property="og:description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="keywords" content="Impotence, Boss, man, men sex, sexual, cure, product, unable to ejaculate, Penile Erectile Dysfunction, Sexual Desire Disorder, Sexual Intercourse Disorder, low sexual desire,阳痿,性冷淡,性功能障碍,不举,  etc">
    <link rel="canonical" href="https://bossinternational.asia/bonusReport.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php include 'header-sherry.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">

    <!-- <h1 class="h1-title h1-before-border shipping-h1">Overriding , Same Level Bonus</h1> -->
    <h1 class="h1-title h1-before-border shipping-h1"><?php echo _MAINJS_BONUSREP_OVER_BONUS_REP ?></h1>
    <!-- <h1 class="right-cell shipping-h1 right-h1"><?php //echo _MAINJS_BONUSREP_TOT_BONUS ?> : RM<?php //echo $userDetails[0]->getBonus() ?></h1> -->

    <div class="clear"></div>

    <div class="width100 shipping-div2">
    	<div class="overflow-scroll-div">
            <table class="shipping-table">
                <thead>
                    <tr>
                        <th><?php echo _MAINJS_BONUSREP_NO ?></th>
                        <th><?php echo _MAINJS_BONUSREP_USERNAME ?></th>
                        <!-- <th>New User</th> -->
                        <!-- <th>Receiver Username</th> -->
                        <th><?php echo _MAINJS_BONUSREP_BONUS ?> (RM)</th>
                        <th><?php echo _MAINJS_BONUSREP_JOIN_DATE ?></th>
                        <!-- <th><th> -->
                    </tr>
                </thead>
                <tbody>

                <?php
                if($adminList)
                {
                    for($cnt = 0;$cnt < count($adminList) ;$cnt++)
                    {?>
                        <tr class="link-to-details">
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $adminList[$cnt]->getNewUsername();?></td>
                            <td> <?php echo $adminList[$cnt]->getAmount();?></td>
                            <td>
                                <?php $dateCreated = date("Y-m-d",strtotime($adminList[$cnt]->getDateCreated()));echo $dateCreated;?>
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>

                </tbody>
            </table>
        </div>
    </div>
    <!-- <div class="clear"></div> -->
</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>

</body>
</html>