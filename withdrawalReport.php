<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Withdrawal.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Images.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/TransactionHistory.php';

$conn = connDB();
$uid = $_SESSION['uid'];

$adminList = getWithdrawReq($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
//$userDetails = getUser($conn,"WHERE uid = ?",array("uid"),array($uid),"s");
//for memberlist
// $adminList = getUser($conn," WHERE user_type = ? ",array("user_type"),array(1),"i");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://bossinternational.asia/withdrawalReport.php" />
    <meta property="og:title" content="Withdrawal Report | Boss" />
    <title>Withdrawal Report | Boss</title>
    <meta property="og:description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="keywords" content="Impotence, Boss, man, men sex, sexual, cure, product, unable to ejaculate, Penile Erectile Dysfunction, Sexual Desire Disorder, Sexual Intercourse Disorder, low sexual desire,阳痿,性冷淡,性功能障碍,不举,  etc">
    <link rel="canonical" href="https://bossinternational.asia/withdrawalReport.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php include 'header-sherry.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">
    <!-- <h1 class="h1-title h1-before-border shipping-h1">Withdrawal Report</h1> -->
    <h1 class="h1-title h1-before-border shipping-h1"><?php echo _MAINJS_WITHDRAWREP_WITHDRAW_REP ?></h1>
    <!-- <h1 class="right-cell">Pending : <?php //totalPending() ?></h1> -->


    <!-- This is a filter for the table result -->
    <!-- <select class="filter-select clean">
    	<option class="filter-option">Latest</option>
    	<option class="filter-option">Oldest</option>
    </select> -->



    <div class="clear"></div>

    <div class="width100 shipping-div2">
    	<div class="overflow-scroll-div">
            <table class="shipping-table">
                <thead>
                    <tr>
                        <!-- <th>NO.</th>
                        <th>WITHDRAW NO.</th>
                        <th>NAME</th>
                        <th>DATE</th>
                        <th>REQUEST AMOUNT (RM)</th>
                        <th>GIVEN AMOUNT (RM)</th>
                        <th>NOTE</th>
                        <th>STATUS</th>
                        <th>DETAILS</th> -->

                        <th><?php echo _MAINJS_WITHDRAWREP_NO ?></th>
                        <th><?php echo _MAINJS_WITHDRAWREP_WITHDRAWAL_NO ?></th>
                        <th><?php echo _MAINJS_WITHDRAWREP_NAME ?></th>
                        <th><?php echo _MAINJS_WITHDRAWREP_DATE ?></th>
                        <th><?php echo _MAINJS_WITHDRAWREP_REQUEST_AMT ?></th>
                        <th><?php echo _MAINJS_WITHDRAWREP_APPROVE_AMT ?></th>
                        <th><?php echo _MAINJS_WITHDRAWREP_NOTE ?></th>
                        <th><?php echo _MAINJS_WITHDRAWREP_STATUS ?></th>
                        <th><?php echo _MAINJS_WITHDRAWREP_DETAILS ?></th>

                    </tr>
                </thead>
                <tbody>

                <?php
                if($adminList)
                {
                    for($cnt = 0;$cnt < count($adminList) ;$cnt++)
                    {?>
                        <tr class="link-to-details">
                            <td><?php echo ($cnt+1)?></td>
                            <td>#<?php echo $adminList[$cnt]->getWithdrawalNumber();?></td>
                            <td><?php echo $adminList[$cnt]->getUsername();?></td>
                            <td>
                                <?php $dateCreated = date("Y-m-d",strtotime($adminList[$cnt]->getDateCreated()));echo $dateCreated;?>
                            </td>

                            <td> <?php echo $adminList[$cnt]->getWithdrawRequestAmount();?></td>
                            <td> <?php echo $adminList[$cnt]->getWithdrawalAmount();?></td>
                            <td> <?php echo $adminList[$cnt]->getWithdrawalNote();?></td>
                            <td> <?php echo $adminList[$cnt]->getWithdrawalRequestStatus();?></td>
                            <td>
                              <?php

                              $statusWithdraw = $adminList[$cnt]->getWithdrawalRequestStatus();

                              if ($statusWithdraw == 'ACCEPTED') {?>
                                  <form action="withdrawalDetails.php" method="POST">
                                      <button class="clean edit-anc-btn hover1" type="submit" name="withdrawal_number" value="<?php echo $adminList[$cnt]->getWithdrawalNumber();?>">
                                        <img src="img/verify-payment.png" class="edit-announcement-img hover1a" alt="UpdateShipping" title="View Details">
                                        <img src="img/verify-payment2.png" class="edit-announcement-img hover1b" alt="UpdateShipping" title="View Details">
                                      </button>
                                  </form>
                              </td>

                            <?php } ?>


                        </tr>
                        <?php
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="clear"></div>

    <!-- <div class="bottom-big-container">
    	<div class="left-btm-page">
        	Page <select class="clean transparent-select"><option>1</option></select> of 1
        </div>
        <div class="middle-btm-page">
        	<a class="round-black-page">1</a>
            <a class="round-white-page">2</a>
        </div>
        <div class="right-btm-page">
        	Total: 2
        </div>
    </div>  -->

</div>


<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

        if($_GET['type'] == 1 )
        {
            $messageType = "Successfully Add New Admin";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "Error";
        }
        echo '

        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>

        ';
        $_SESSION['messageType'] = 0;
}
?>
<?php


function totalPending(){
  $conn = connDB();
  $uid = $_SESSION['uid'];
  $result1 = mysqli_query($conn,"SELECT count(withdrawal_status) FROM withdrawal WHERE withdrawal_status = 'PENDING' AND uid = '$uid';");

  if (mysqli_num_rows($result1) > 0) {
  ?>
              <?php
              $i=0;
              while($row = mysqli_fetch_array($result1)) {
              ?>
              <?php echo $row["count(withdrawal_status)"]; ?>

              <?php
              $i++;
              }

              ?>
         <?php
        }
        else{
            echo "0";
        }

}

 ?>

</body>
</html>
