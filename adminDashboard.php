<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$products = getProduct($conn);

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://bossinternational.asia/adminDashboard.php" />
    <meta property="og:title" content="Admin Dashboard | Boss" />
    <title>Admin Dashboard | Boss</title>
    <meta property="og:description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="keywords" content="Impotence, Boss, man, men sex, sexual, cure, product, unable to ejaculate, Penile Erectile Dysfunction, Sexual Desire Disorder, Sexual Intercourse Disorder, low sexual desire,阳痿,性冷淡,性功能障碍,不举,  etc">
    <link rel="canonical" href="https://bossinternational.asia/adminDashboard.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<!--<form method="POST">-->
    <?php
//    if(isset($_POST['product-list-quantity-input'])){
//        createProductList($products,$_POST['product-list-quantity-input']);
//    }else{
//        createProductList($products);
//    }

    ?>
<!--    <button type="submit" name="addToCartButton" id="addToCartButton" >Add to cart</button>-->
<!--</form>-->
<div class="yellow-body padding-from-menu same-padding">

    <!-- <h1 class="h1-title h1-before-border shipping-h1 right-info"><a href="adminDashboard.php?lang=en" class="white-text title-tab-a">EN</a> | <a href="adminDashboard.php?lang=ch" class="white-text title-tab-a">中文</a></h1> -->
    <!-- <h1 class="h1-title h1-before-border">Dashboard</h1> -->
    <h1 class="h1-title h1-before-border"><?php echo _MAINJS_ADMDASH_DASHBOARD ?></h1>
    <div class="border-top100 four-div-container">
    	<a href="adminProduct.php" class="black-text">
            <div class="four-white-div hover1 white-div-yellow">
                <img src="img/product1.png" class="four-img hover1a" alt="Products" title="Products">
                <img src="img/product2.png" class="four-img hover1b" alt="Products" title="Products">
                <!-- <p class="four-div-p"><b><?php //echo totalProductDashboard(); ?> Total Product</b></p> -->
                <p class="four-div-p"><b><?php echo totalProductDashboard(); ?> <?php echo _MAINJS_ADMDASH_TOTAL_PRODUCT ?> </b></p>
            </div>
        </a>
        <!-- <a href="adminShpping.php" class="black-text"> -->
        <a href="adminShipping.php" class="black-text">
            <div class="four-white-div hover1 four-middle-div1 white-div-yellow">
                <img src="img/shipping1.png" class="four-img hover1a" alt="Shipping" title="Shipping">
                <img src="img/shipping2.png" class="four-img hover1b" alt="Shipping" title="Shipping">
                <!-- <p class="four-div-p"><b><?php //echo shippingRequest() ?> Shipping Requests</b></p> -->
                <p class="four-div-p"><b><?php echo shippingRequest() ?> <?php echo _MAINJS_ADMDASH_SHIPPING_REQUEST ?> </b></p>
            </div>
        </a>
        <a href="adminWithdrawal.php" class="black-text">
            <div class="four-white-div hover1 four-middle-div2 white-div-yellow">
                <img src="img/withdraw1.png" class="four-img hover1a" alt="Withdrawal" title="Withdrawal">
                <img src="img/withdraw2.png" class="four-img hover1b" alt="Withdrawal" title="Withdrawal">
                <!-- <p class="four-div-p"><b><?php //echo withdrawalRequest() ?> Withdrawal Requests</b></p> -->
                <p class="four-div-p"><b><?php echo withdrawalRequest() ?> <?php echo _MAINJS_ADMDASH_WITHDRAWAL_REQUEST ?> </b></p>
            </div>
        </a>
        <a href="adminMember.php" class="black-text">
            <div class="four-white-div hover1 white-div-yellow">
                <img src="img/member1.png" class="four-img hover1a" alt="New Joined Members" title="New Joined Members">
                <img src="img/member2.png" class="four-img hover1b" alt="New Joined Members" title="New Joined Members">
                <!-- <p class="four-div-p"><b><?php //echo totalMemberJoined()?> Total Members</b></p> -->
                <p class="four-div-p"><b><?php echo totalMemberJoined()?> <?php echo _MAINJS_ADMDASH_TOTAL_MEMBER ?> </b></p>
            </div>
        </a>
    </div>
    <div class="clear"></div>

    
</div>

<?php


function totalPrice(){
  $conn = connDB();
  $result1 = mysqli_query($conn,"SELECT sum(total_price) AS productno1 FROM `product`");

  if (mysqli_num_rows($result1) > 0) {
  ?>  <table>


  <div class="clear"></div>

  <div class="with100">
   <table class="sales-table">
       <thead>

              <?php
              $i=0;
              while($row = mysqli_fetch_array($result1)) {

              ?>

          </thead>
          <?php

           ?>



              <?php echo $row["productno1"]; ?>




              <?php
              $i++;
              }

              ?>

        </table>



         <?php
        }
        else{
            echo "No result found";
        }

}

 ?>

 <?php


 function totalQuantity(){
   $conn = connDB();
   $result1 = mysqli_query($conn,"SELECT sum(buy_stock) AS productno1 FROM `product`");

   if (mysqli_num_rows($result1) > 0) {
   ?>  <table>


   <div class="clear"></div>

   <div class="with100">
    <table class="sales-table">
        <thead>

               <?php
               $i=0;
               while($row = mysqli_fetch_array($result1)) {

               ?>

           </thead>
           <?php

            ?>



               <?php echo $row["productno1"]; ?>




               <?php
               $i++;
               }

               ?>

         </table>



          <?php
         }
         else{
             echo "No result found";
         }

 }

  ?>

 <?php


 function totalMemberJoined(){
   $conn = connDB();
   $result1 = mysqli_query($conn,"SELECT count(uid) AS totalMemberJoined FROM user WHERE user_type = '1' ");

   if (mysqli_num_rows($result1) > 0) {
   ?>  <table>


   <div class="clear"></div>

   <div class="with100">
    <table class="sales-table">
        <thead>

               <?php
               $i=0;
               while($row = mysqli_fetch_array($result1)) {

               ?>

           </thead>
           <?php

            ?>



               <?php echo $row["totalMemberJoined"]; ?>




               <?php
               $i++;
               }

               ?>

         </table>



          <?php
         }
         else{
             echo "No result found";
         }

 }

  ?>

  <?php


  function shippingRequest(){
    $conn = connDB();
    $result1 = mysqli_query($conn,"SELECT count(uid) AS shippingRequest FROM `orders` WHERE shipping_status = 'PENDING'");

    if (mysqli_num_rows($result1) > 0) {
    ?>  <table>


    <div class="clear"></div>

    <div class="with100">
     <table class="sales-table">
         <thead>

                <?php
                $i=0;
                while($row = mysqli_fetch_array($result1)) {

                ?>

            </thead>
            <?php

             ?>



                <?php echo $row["shippingRequest"]; ?>




                <?php
                $i++;
                }

                ?>

          </table>



           <?php
          }
          else{
              echo "No result found";
          }

  }

   ?>

   <?php


   function withdrawalRequest(){
     $conn = connDB();
     $result1 = mysqli_query($conn,"SELECT count(uid) AS withdrawalRequest FROM `withdrawal` WHERE withdrawal_status = 'PENDING'");

     if (mysqli_num_rows($result1) > 0) {
     ?>  <table>


     <div class="clear"></div>

     <div class="with100">
      <table class="sales-table">
          <thead>

                 <?php
                 $i=0;
                 while($row = mysqli_fetch_array($result1)) {

                 ?>

             </thead>
             <?php

              ?>



                 <?php echo $row["withdrawalRequest"]; ?>




                 <?php
                 $i++;
                 }

                 ?>

           </table>



            <?php
           }
           else{
               echo "0";
           }

   }

    ?>

   <?php


   function totalProductDashboard(){
     $conn = connDB();
     $result1 = mysqli_query($conn,"SELECT count(display) AS totalProductDashboard FROM product  WHERE display = 1");

     if (mysqli_num_rows($result1) > 0) {
     ?>  <table>


     <div class="clear"></div>

     <div class="with100">
      <table class="sales-table">
          <thead>

                 <?php
                 $i=0;
                 while($row = mysqli_fetch_array($result1)) {

                 ?>

             </thead>
             <?php

              ?>



                 <?php echo $row["totalProductDashboard"]; ?>




                 <?php
                 $i++;
                 }

                 ?>

           </table>



            <?php
           }
           else{
               echo "No result found";
           }

   }

    ?>



<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

</body>
</html>
