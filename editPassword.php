<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://bossinternational.asia/editPassword.php" />
    <meta property="og:title" content="Edit Password | Boss" />
    <title>Edit Password | Boss</title>
    <meta property="og:description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="keywords" content="Impotence, Boss, man, men sex, sexual, cure, product, unable to ejaculate, Penile Erectile Dysfunction, Sexual Desire Disorder, Sexual Intercourse Disorder, low sexual desire,阳痿,性冷淡,性功能障碍,不举,  etc">
    <link rel="canonical" href="https://bossinternational.asia/editPassword.php" />
    <?php include 'css.php'; ?>    
</head>
<body class="body">
<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<!--<form method="POST">-->
    <?php
//    if(isset($_POST['product-list-quantity-input'])){
//        createProductList($products,$_POST['product-list-quantity-input']);
//    }else{
//        createProductList($products);
//    }

    ?>
<!--    <button type="submit" name="addToCartButton" id="addToCartButton" >Add to cart</button>-->
<!--</form>-->
<div class="yellow-body padding-from-menu same-padding">
    <form class="edit-profile-div2" action="utilities/changePasswordFunction.php" method="POST">
        <h1 class="username"><?php echo $userDetails->getUsername();?></h1>
        <!-- <h2 class="profile-title">CHANGE PASSWORD</h2> -->
        <h2 class="profile-title"><?php echo _MAINJS_EDITPASS_CHANGE_PASS ?></h2>
        <table class="edit-profile-table password-table white-text">
        	<tr class="profile-tr">
                <!-- <td class="profile-td1">Current Password</td> -->
                <td class="profile-td1"><?php echo _MAINJS_EDITPASS_CURRENT_PASS ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3">
                    <input required name="editPassword_current" id="editPassword_current" class="clean edit-profile-input" type="password">
                    <span class="visible-span2">
                        <img src="img/visible.png" class="login-input-icon" alt="View Password" title="View Password" id="editPassword_current_img">
                    </span>
                </td>
            </tr>
        	<tr class="profile-tr">
                <!-- <td class="profile-td1">New Password</td> -->
                <td class="profile-td1"><?php echo _MAINJS_EDITPASS_NEW_PASS ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3">
                    <input required name="editPassword_new" id="editPassword_new" class="clean edit-profile-input" type="password">
                    <span class="visible-span2">
                        <img src="img/visible.png" class="login-input-icon" alt="View Password" title="View Password" id="editPassword_new_img">
                    </span>
                </td>
            </tr>            
        	<tr class="profile-tr">
                <!-- <td class="profile-td1">Retype New Password</td> -->
                <td class="profile-td1"><?php echo _MAINJS_EDITPASS_RETYPE_NEW_PASS ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3">
                    <input required name="editPassword_reenter" id="editPassword_reenter" class="clean edit-profile-input"type="password">
                    <span class="visible-span2">
                        <img src="img/visible.png" class="login-input-icon" alt="View Password" title="View Password" id="editPassword_reenter_img">
                    </span>
                </td>
            </tr>          
        </table>
        <button class="confirm-btn text-center white-text clean black-button"><?php echo _MAINJS_EDITPASS_CONFIRM ?></button>
		<!-- <button class="confirm-btn text-center white-text clean black-button">Confirm</button> -->
    </form>
</div>
<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>
<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Your current password does not match! <br>Please try again.";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "Your new password must be more than 5 characters";
        }
        if($_GET['type'] == 3)
        {
            $messageType = "Password does not match with retype password. <br>Please try again.";
        }
        if($_GET['type'] == 4)
        {
            $messageType = "Server problem. <br>Please try again later.";
        }
        if($_GET['type'] == 5)
        {
            $messageType = "Password successfully updated!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>
<script>
  viewPassword( document.getElementById('editPassword_current_img'), document.getElementById('editPassword_current'));
  viewPassword( document.getElementById('editPassword_new_img'), document.getElementById('editPassword_new'));
  viewPassword( document.getElementById('editPassword_reenter_img'), document.getElementById('editPassword_reenter'));
</script>
</body>
</html>