<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://bossinternational.asia/adminMember.php" />
    <meta property="og:title" content="Member | Boss" />
    <title>Member | Boss</title>
    <meta property="og:description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="keywords" content="Impotence, Boss, man, men sex, sexual, cure, product, unable to ejaculate, Penile Erectile Dysfunction, Sexual Desire Disorder, Sexual Intercourse Disorder, low sexual desire,阳痿,性冷淡,性功能障碍,不举,  etc">
    <link rel="canonical" href="https://bossinternational.asia/adminMember.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">

  <h1 class="h1-title h1-before-border shipping-h1"><?php echo _MAINJS_ADMMEM_MEMBER ?></h1>
    <div class="clear"></div>

      <div class="overflow-scroll-div">
          <table class="shipping-table">
              <thead>
                  <tr>
                      <th>NO.</th>
                      <th>USERNAME</th>
                      <th>FULLNAME</th>
                      <th>JOINED DATE</th>
                  </tr>
              </thead>
              <tbody>

              <?php
              $conn = connDB();
              $memberList = getUser($conn," WHERE user_type = ? ",array("user_type"),array(1),"i");
              if($memberList)
              {
                  for($cnt = 0;$cnt < count($memberList) ;$cnt++)
                  {?>
                      <tr>
                          <td><?php echo ($cnt+1)?></td>
                          <td><?php echo $memberList[$cnt]->getUsername();?></td>
                          <td><?php echo $memberList[$cnt]->getFullname();?></td>
                          <td>
                              <?php $dateCreated = date("Y-m-d",strtotime($memberList[$cnt]->getDateCreated()));echo $dateCreated;?>
                          </td>
                      </tr>
                      <?php
                  }
              }
              $conn->close();
              ?>
              </tbody>           
          </table>
      </div>

</div>


<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

</body>
</html>