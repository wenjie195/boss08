<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Cart.php';

require_once dirname(__FILE__) . '/classes/GroupCommission.php';
require_once dirname(__FILE__) . '/classes/TransactionHistory.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    addToCart();
    createOrder($conn,$uid);
    header('Location: ./checkout.php');

}


$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://bossinternational.asia/viewCart.php" />
<meta property="og:title" content="Cart | Boss" />
<title>Cart | Boss</title>
<meta property="og:description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
<meta name="description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
<meta name="keywords" content="Impotence, Boss, man, men sex, sexual, cure, product, unable to ejaculate, Penile Erectile Dysfunction, Sexual Desire Disorder, Sexual Intercourse Disorder, low sexual desire,阳痿,性冷淡,性功能障碍,不举,  etc">
<link rel="canonical" href="https://bossinternational.asia/viewCart.php" />
<?php include 'css.php'; ?>
<?php require_once dirname(__FILE__) . '/header.php'; ?>
</head>

<body class="body">

<!-- Start Menu -->

<div class="yellow-body padding-from-menu same-padding">
	<?php include 'header-sherry.php'; ?>

    <h1 class="cart-h1">Your Cart</h1>

    <form method="POST">


        <table class="cart-table">
            <th>Product</th>
            <th></th>
            <th class="quantity-td">QUANTITY</th>
            <th>PRICE</th>
            <th>TOTAL</th>
        </table>

        <?php
            $conn = connDB();
            if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart'])
            {
                $productListHtml = getShoppingCart($conn,2);
                echo $productListHtml;
            }
            else
            {
                echo " <h3> YOUR CART IS EMPTY </h3>";
            }


            if(array_key_exists('xclearCart', $_POST))
            {
                xclearCart();
            }
            else
            {
            // code...
                unset($productListHtml);
            }

            $conn->close();
        ?>

    <div class="cart-bottom-div">
        <div class="left-cart-bottom-div">
            <p class="continue-shopping pointer continue2">
                <a href="product.php" class="black-white-link"><img src="img/back.png" class="back-btn" alt="back" title="back"> Continue Shopping</a>
            </p>
        </div>
    </div>

    </form>

</div>

<?php include 'js.php'; ?>

</body>
</html>