<?php
class User {
    /* Member variables */
    var $uid,$username,$email,$password,$salt,$phoneNo,$icNo,$countryId,$fullName,$epin,$saltEpin,$emailVerificationCode,$isEmailVerified,$loginType,
            $userType,$canSendNewsletter,$isReferred,$dateCreated,$dateUpdated,
                $address,$birthday,$gender,$bankName,$bankAccountHolder,$bankAccountNo,$picture,$total_downline_no,$current_status,
                    $downline_no_lvlone,$status_lvlone,$downline_no_lvltwo,$status_lvltwo,$downline_no_lvlthree,$status_lvlthree,$bonus,$points,$cash;

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @param mixed $salt
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    /**
     * @return mixed
     */
    public function getPhoneNo()
    {
        return $this->phoneNo;
    }

    /**
     * @param mixed $phoneNo
     */
    public function setPhoneNo($phoneNo)
    {
        $this->phoneNo = $phoneNo;
    }

    /**
     * @return mixed
     */
    public function getIcNo()
    {
        return $this->icNo;
    }

    /**
     * @param mixed $icNo
     */
    public function setIcNo($icNo)
    {
        $this->icNo = $icNo;
    }

    /**
     * @return mixed
     */
    public function getCountryId()
    {
        return $this->countryId;
    }

    /**
     * @param mixed $countryId
     */
    public function setCountryId($countryId)
    {
        $this->countryId = $countryId;
    }

    /**
     * @return mixed
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @param mixed $fullName
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;
    }

    /**
     * @return mixed
     */
    public function getEpin()
    {
        return $this->epin;
    }

    /**
     * @param mixed $epin
     */
    public function setEpin($epin)
    {
        $this->epin = $epin;
    }

    /**
     * @return mixed
     */
    public function getSaltEpin()
    {
        return $this->saltEpin;
    }

    /**
     * @param mixed $saltEpin
     */
    public function setSaltEpin($saltEpin)
    {
        $this->saltEpin = $saltEpin;
    }

    /**
     * @return mixed
     */
    public function getEmailVerificationCode()
    {
        return $this->emailVerificationCode;
    }

    /**
     * @param mixed $emailVerificationCode
     */
    public function setEmailVerificationCode($emailVerificationCode)
    {
        $this->emailVerificationCode = $emailVerificationCode;
    }

    /**
     * @return mixed
     */
    public function getisEmailVerified()
    {
        return $this->isEmailVerified;
    }

    /**
     * @param mixed $isEmailVerified
     */
    public function setIsEmailVerified($isEmailVerified)
    {
        $this->isEmailVerified = $isEmailVerified;
    }

    // /**
    //  * @return mixed
    //  */
    // public function getisPhoneVerified()
    // {
    //     return $this->isPhoneVerified;
    // }

    // /**
    //  * @param mixed $isPhoneVerified
    //  */
    // public function setIsPhoneVerified($isPhoneVerified)
    // {
    //     $this->isPhoneVerified = $isPhoneVerified;
    // }

    /**
     * @return mixed
     */
    public function getLoginType()
    {
        return $this->loginType;
    }

    /**
     * @param mixed $loginType
     */
    public function setLoginType($loginType)
    {
        $this->loginType = $loginType;
    }

    /**
     * @return mixed
     */
    public function getUserType()
    {
        return $this->userType;
    }

    /**
     * @param mixed $userType
     */
    public function setUserType($userType)
    {
        $this->userType = $userType;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

    /**
     * @return mixed
     */
    public function getisReferred()
    {
        return $this->isReferred;
    }

    /**
     * @param mixed $isReferred
     */
    public function setIsReferred($isReferred)
    {
        $this->isReferred = $isReferred;
    }

    /**
     * @return mixed
     */
    public function getCanSendNewsletter()
    {
        return $this->canSendNewsletter;
    }

    /**
     * @param mixed $canSendNewsletter
     */
    public function setCanSendNewsletter($canSendNewsletter)
    {
        $this->canSendNewsletter = $canSendNewsletter;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @param mixed $birthday
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return mixed
     */
    public function getBankName()
    {
        return $this->bankName;
    }

    /**
     * @param mixed $bankName
     */
    public function setBankName($bankName)
    {
        $this->bankName = $bankName;
    }

/**
     * @return mixed
     */
    public function getBankAccountHolder()
    {
        return $this->bankAccountHolder;
    }

    /**
     * @param mixed $bankAccountHolder
     */
    public function setBankAccountHolder($bankAccountHolder)
    {
        $this->bankAccountHolder = $bankAccountHolder;
    }

    /**
     * @return mixed
     */
    public function getBankAccountNo()
    {
        return $this->bankAccountNo;
    }

    /**
     * @param mixed $bankAccountNo
     */
    public function setBankAccountNo($bankAccountNo)
    {
        $this->bankAccountNo = $bankAccountNo;
    }

    /**
     * @return mixed
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param mixed $picture
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }

    /**
     * @return mixed
     */
    public function getTotalDownlineNo()
    {
        return $this->total_downline_no;
    }

    /**
     * @param mixed $total_downline_no
     */
    public function setTotalDownlineNo($total_downline_no)
    {
        $this->total_downline_no = $total_downline_no;
    }

    /**
     * @return mixed
     */
    public function getCurrentStatus()
    {
        return $this->current_status;
    }

    /**
     * @param mixed $current_status
     */
    public function setCurrentStatus($current_status)
    {
        $this->current_status = $current_status;
    }

    /**
     * @return mixed
     */
    public function getDownlineLvlOne()
    {
        return $this->downline_no_lvlone;
    }

    /**
     * @param mixed $downline_no_lvlone
     */
    public function setDownlineLvlOne($downline_no_lvlone)
    {
        $this->downline_no_lvlone = $downline_no_lvlone;
    }

    /**
     * @return mixed
     */
    public function getStatusLvlOne()
    {
        return $this->status_lvlone;
    }

    /**
     * @param mixed $status_lvlone
     */
    public function setStatusLvlOne($status_lvlone)
    {
        $this->status_lvlone = $status_lvlone;
    }

    /**
     * @return mixed
     */
    public function getDownlineLvlTwo()
    {
        return $this->downline_no_lvltwo;
    }

    /**
     * @param mixed $downline_no_lvltwo
     */
    public function setDownlineLvlTwo($downline_no_lvltwo)
    {
        $this->downline_no_lvltwo = $downline_no_lvltwo;
    }

    /**
     * @return mixed
     */
    public function getStatusLvlTwo()
    {
        return $this->status_lvltwo;
    }

    /**
     * @param mixed $status_lvlone
     */
    public function setStatusLvlTwo($status_lvltwo)
    {
        $this->status_lvltwo = $status_lvltwo;
    }

        /**
     * @return mixed
     */
    public function getDownlineLvlThree()
    {
        return $this->downline_no_lvlthree;
    }

    /**
     * @param mixed $downline_no_lvlthree
     */
    public function setDownlineLvlThree($downline_no_lvlthree)
    {
        $this->downline_no_lvlthree = $downline_no_lvlthree;
    }

    /**
     * @return mixed
     */
    public function getStatusLvlThree()
    {
        return $this->status_lvlthree;
    }

    /**
     * @param mixed $status_lvlthree
     */
    public function setStatusLvlThree($status_lvlthree)
    {
        $this->status_lvlthree = $status_lvlthree;
    }

    /**
     * @return mixed
     */
    public function getBonus()
    {
        return $this->bonus;
    }

    /**
     * @param mixed $bonus
     */
    public function setBonus($bonus)
    {
        $this->bonus = $bonus;
    }

    /**
     * @return mixed
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * @param mixed $points
     */
    public function setPoints($points)
    {
        $this->points = $points;
    }

    /**
     * @return mixed
     */
    public function getCash()
    {
        return $this->cash;
    }

    /**
     * @param mixed $cash
     */
    public function setCash($cash)
    {
        $this->cash = $cash;
    }

}

function getUser($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("uid","username","email","password","salt","phone_no","ic_no","country_id","full_name","epin","salt_epin","email_verification_code","is_email_verified",
        "login_type","user_type","can_send_newsletter","is_referred","date_created","date_updated",
            "address","birthday","gender","bank_name","bank_account_holder","bank_account_no","picture_id","total_downline_no","current_status",
                "downline_no_lvlone","status_lvlone","downline_no_lvltwo","status_lvltwo","downline_no_lvlthree","status_lvlthree","bonus","points","cash");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"user");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($uid, $username, $email, $password, $salt, $phoneNo, $icNo, $countryId, $fullName, $epin ,$saltEpin, $emailVerificationCode, $isEmailVerified,
            $loginType, $userType, $canSendNewsletter, $isReferred, $dateCreated, $dateUpdated,
                $address, $birthday, $gender, $bankName, $bankAccountHolder, $bankAccountNo, $picture, $total_downline_no, $current_status,
                    $downline_no_lvlone,$status_lvlone,$downline_no_lvltwo,$status_lvltwo,$downline_no_lvlthree,$status_lvlthree,$bonus,$points,$cash);

        $resultRows = array();
        while ($stmt->fetch()) {
            $user = new User;
            $user->setUid($uid);
            $user->setUsername($username);
            $user->setEmail($email);
            $user->setPassword($password);
            $user->setSalt($salt);
            $user->setPhoneNo($phoneNo);
            $user->setIcNo($icNo);
            $user->setCountryId($countryId);
            $user->setFullName($fullName);
            $user->setEpin($epin);
            $user->setSaltEpin($saltEpin);
            $user->setEmailVerificationCode($emailVerificationCode);
            $user->setIsEmailVerified($isEmailVerified);
            $user->setLoginType($loginType);
            $user->setUserType($userType);
            $user->setCanSendNewsletter($canSendNewsletter);
            $user->setIsReferred($isReferred);
            $user->setDateCreated($dateCreated);
            $user->setDateUpdated($dateUpdated);
            $user->setAddress($address);
            $user->setBirthday($birthday);
            $user->setGender($gender);
            $user->setBankName($bankName);
            $user->setBankAccountHolder($bankAccountHolder);
            $user->setBankAccountNo($bankAccountNo);
            $user->setPicture($picture);
            $user->setTotalDownlineNo($total_downline_no);
            $user->setCurrentStatus($current_status);

            $user->setDownlineLvlOne($downline_no_lvlone);
            $user->setStatusLvlOne($status_lvlone);
            $user->setDownlineLvlTwo($downline_no_lvltwo);
            $user->setStatusLvlTwo($status_lvltwo);
            $user->setDownlineLvlThree($downline_no_lvlthree);
            $user->setStatusLvlThree($status_lvlthree);

            $user->setBonus($bonus);
            $user->setPoints($points);
            $user->setCash($cash);
        
            array_push($resultRows,$user);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}

function createTotalWithdrawAmount(){

  $totaedlWithdrawAmount = 0;

      if($withdrawAmount > 0){

          $totaledWithdrawAmount = $withdrawAmount + $userDetails->getWithdrawAmount();

      }else{

      }


}
