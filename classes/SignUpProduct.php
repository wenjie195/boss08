<?php
class SignUpProduct {
    /* Member variables */
    // var  $referrerUid,$referrerName,$referralUid,$referralName,$product,$dateCreated,$dateUpdated;
    // var  $id,$referrerUid,$referrerName,$referralUid,$referralName,$product,$price,$quantity,$total,$dateCreated,$dateUpdated;
    var  $id,$referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$product,$price,$quantity,$total,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
    /**
     * @return mixed
     */
    public function getReferrerUid()
    {
        return $this->referrerUid;
    }

    /**
     * @param mixed $referrerUid
     */
    public function setReferrerUid($referrerUid)
    {
        $this->referrerUid = $referrerUid;
    }

    /**
     * @return mixed
     */
    public function getReferrerName()
    {
        return $this->referrerName;
    }

    /**
     * @param mixed $referrerName
     */
    public function setReferrerName($referrerName)
    {
        $this->referrerName = $referrerName;
    }

    /**
     * @return mixed
     */
    public function getReferralUid()
    {
        return $this->referralUid;
    }

    /**
     * @param mixed $referralUid
     */
    public function setReferralUid($referralUid)
    {
        $this->referralUid = $referralUid;
    }

    /**
     * @return mixed
     */
    public function getReferralName()
    {
        return $this->referralName;
    }

    /**
     * @param mixed $referralName
     */
    public function setReferralName($referralName)
    {
        $this->referralName = $referralName;
    }

    /**
     * @return mixed
     */
    public function getReferralFullname()
    {
        return $this->referralFullname;
    }

    /**
     * @param mixed $referralFullname
     */
    public function setReferralFullname($referralFullname)
    {
        $this->referralFullname = $referralFullname;
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param mixed $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return mixed
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param mixed $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }
    
    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getSignUpProduct($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    // $dbColumnNames = array("referrer_id","referrer_name","referral_id","referral_name","product","date_created","date_updated");
    // $dbColumnNames = array("id","referrer_id","referrer_name","referral_id","referral_name","product","price","quantity","total","date_created","date_updated");
    $dbColumnNames = array("id","referrer_id","referrer_name","referral_id","referral_name","referral_fullname","product","price","quantity","total","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"signup_product");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        // $stmt->bind_result($referrerUid,$referrerName,$referralUid,$referralName,$product,$dateCreated,$dateUpdated);
        // $stmt->bind_result($id,$referrerUid,$referrerName,$referralUid,$referralName,$product,$price,$quantity,$total,$dateCreated,$dateUpdated);
        $stmt->bind_result($id,$referrerUid,$referrerName,$referralUid,$referralName,$referralFullname,$product,$price,$quantity,$total,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new SignUpProduct();

            $class->setId($id);
            $class->setReferrerUid($referrerUid);
            $class->setReferrerName($referrerName);
            $class->setReferralUid($referralUid);
            $class->setReferralName($referralName);

            $class->setReferralFullname($referralFullname);

            $class->setProduct($product);
            $class->setPrice($price);
            $class->setQuantity($quantity);
            $class->setTotal($total);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }

}
