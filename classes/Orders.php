<?php
class Orders {
    /* Member variables */
    //var $id,$uid,$address_line_1,$address_line_2,$address_line_3,$city,$zipcode,$state,$country,$dateCreated,$dateUpdated;
    var $id,$uid,$username,$bankName,$bankAccountHolder,$bankAccountNo,$name,$contactNo,$email,$address_line_1,$address_line_2,$address_line_3
            ,$city,$zipcode,$state,$country,$dateCreated,$dateUpdated,$subtotal,$total,$payment_method,$payment_amount,$payment_bankreference,$payment_date,$payment_time,$payment_status
            ,$shipping_status,$shipping_method,$shipping_date,$tracking_number,$reject_reason,$refund_method,$refund_amount,$refund_note,$refund_reason,$receipt;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $uid
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

        /**
     * @return mixed
     */
    public function getBankName()
    {
        return $this->bankName;
    }

    /**
     * @param mixed $bankName
     */
    public function setBankName($bankName)
    {
        $this->bankName = $bankName;
    }

/**
     * @return mixed
     */
    public function getBankAccountHolder()
    {
        return $this->bankAccountHolder;
    }

    /**
     * @param mixed $bankAccountHolder
     */
    public function setBankAccountHolder($bankAccountHolder)
    {
        $this->bankAccountHolder = $bankAccountHolder;
    }

    /**
     * @return mixed
     */
    public function getBankAccountNo()
    {
        return $this->bankAccountNo;
    }

    /**
     * @param mixed $bankAccountNo
     */
    public function setBankAccountNo($bankAccountNo)
    {
        $this->bankAccountNo = $bankAccountNo;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $id
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getContactNo()
    {
        return $this->contactNo;
    }

    /**
     * @param mixed $id
     */
    public function setContactNo($contactNo)
    {
        $this->contactNo = $contactNo;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $id
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getAddressLine1()
    {
        return $this->address_line_1;
    }

    /**
     * @param mixed $address_line_1
     */
    public function setAddressLine1($address_line_1)
    {
        $this->address_line_1 = $address_line_1;
    }

    /**
     * @return mixed
     */
    public function getAddressLine2()
    {
        return $this->address_line_2;
    }

    /**
     * @param mixed $address_line_2
     */
    public function setAddressLine2($address_line_2)
    {
        $this->address_line_2 = $address_line_2;
    }

    /**
     * @return mixed
     */
    public function getAddressLine3()
    {
        return $this->address_line_3;
    }

    /**
     * @param mixed $address_line_3
     */
    public function setAddressLine3($address_line_3)
    {
        $this->address_line_3 = $address_line_3;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * @param mixed $zipcode
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

    /**
     * @return mixed
     */
    public function getSubtotal()
    {
        return $this->subtotal;
    }

    /**
     * @param mixed $subtotal
     */
    public function setSubtotal($subtotal)
    {
        $this->subtotal = $subtotal;
    }

    /**
     * @return mixed
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param mixed $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return mixed
     */
    public function getPaymentMethod()
    {
        return $this->payment_method;
    }

    /**
     * @param mixed $payment_method
     */
    public function setPaymentMethod($payment_method)
    {
        $this->payment_method = $payment_method;
    }

    /**
     * @return mixed
     */
    public function getPaymentAmount()
    {
        return $this->payment_amount;
    }

    /**
     * @param mixed $payment_amount
     */
    public function setPaymentAmount($payment_amount)
    {
        $this->payment_amount = $payment_amount;
    }

    /**
     * @return mixed
     */
    public function getPaymentBankReference()
    {
        return $this->payment_bankreference;
    }

    /**
     * @param mixed $payment_bankreference
     */
    public function setPaymentBankReference($payment_bankreference)
    {
        $this->payment_bankreference = $payment_bankreference;
    }

    /**
     * @return mixed
     */
    public function getPaymentDate()
    {
        return $this->payment_date;
    }

    /**
     * @param mixed $payment_date
     */
    public function setPaymentDate($payment_date)
    {
        $this->payment_date = $payment_date;
    }

    /**
     * @return mixed
     */
    public function getPaymentTime()
    {
        return $this->payment_time;
    }

    /**
     * @param mixed $payment_time
     */
    public function setPaymentTime($payment_time)
    {
        $this->payment_time = $payment_time;
    }

    /**
     * @return mixed
     */
    public function getPaymentStatus()
    {
        return $this->payment_status;
    }

    /**
     * @param mixed $payment_status
     */
    public function setPaymentStatus($payment_status)
    {
        $this->payment_status = $payment_status;
    }

    /**
     * @return mixed
     */
    public function getShippingStatus()
    {
        return $this->shipping_status;
    }

    /**
     * @param mixed $shipping_status
     */
    public function setShippingStatus($shipping_status)
    {
        $this->shipping_status = $shipping_status;
    }

    /**
     * @return mixed
     */
    public function getShippingMethod()
    {
        return $this->shipping_method;
    }

    /**
     * @param mixed $shipping_method
     */
    public function setShippingMethod($shipping_method)
    {
        $this->shipping_method = $shipping_method;
    }

    /**
     * @return mixed
     */
    public function getShippingDate()
    {
        return $this->shipping_date;
    }

    /**
     * @param mixed $shipping_date

     */
    public function setShippingDate($shipping_date)
    {
        $this->shipping_date = $shipping_date;
    }

    /**
     * @return mixed
     */
    public function getTrackingNumber()
    {
        return $this->tracking_number;
    }

    /**
     * @param mixed $tracking_number
     */
    public function setTrackingNumber($tracking_number)
    {
        $this->tracking_number = $tracking_number;
    }

    /**
     * @return mixed
     */
    public function getRejectReason()
    {
        return $this->reject_reason;
    }

    /**
     * @param mixed $reject_reason
     */
    public function setRejectReason($reject_reason)
    {
        $this->reject_reason = $reject_reason;
    }

    /**
     * @return mixed
     */
    public function getRefundMethod()
    {
        return $this->refund_method;
    }

    /**
     * @param mixed $refund_method
     */
    public function setRefundMethod($refund_method)
    {
        $this->refund_method = $refund_method;
    }

    /**
     * @return mixed
     */
    public function getRefundAmount()
    {
        return $this->refund_amount;
    }

    /**
     * @param mixed $refund_amount
     */
    public function setRefundAmount($refund_amount)
    {
        $this->refund_amount = $refund_amount;
    }

    /**
     * @return mixed
     */
    public function getRefundNote()
    {
        return $this->refund_note;
    }

    /**
     * @param mixed $refund_note
     */
    public function setRefundNote($refund_note)
    {
        $this->refund_note = $refund_note;
    }

    /**
     * @return mixed
     */
    public function getRefundReason()
    {
        return $this->refund_reason;
    }

    /**
     * @param mixed $refund_reason
     */
    public function setRefundReason($refund_reason)
    {
        $this->refund_reason = $refund_reason;
    }

    /**
     * @return mixed
     */
    public function getReceipt()
    {
        return $this->receipt;
    }

    /**
     * @param mixed $refund_reason
     */
    public function setReceipt($receipt)
    {
        $this->receipt = $receipt;
    }

}

function getOrders($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","username","bank_name","bank_account_holder","bank_account_no","name","contactNo","email","address_line_1","address_line_2","address_line_3",
        "city","zipcode","state","country","date_created","date_updated","subtotal","total","payment_method","payment_amount","payment_bankreference","payment_date","payment_time","payment_status","shipping_status",
            "shipping_method","shipping_date","tracking_number","reject_reason","refund_method","refund_amount","refund_note","refund_reason","receipt");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"orders");
    if($whereClause){
        $sql .= $whereClause;
    }

    // echo $whereClause;

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$uid,$username, $bankName, $bankAccountHolder, $bankAccountNo,$name,$contactNo,$email,$address_line_1,$address_line_2,$address_line_3,
            $city,$zipcode,$state,$country,$dateCreated,$dateUpdated,$subtotal,$total,$payment_method,$payment_amount,$payment_bankreference,$payment_date,$payment_time,$payment_status,$shipping_status,
                $shipping_method,$shipping_date,$tracking_number,$reject_reason,$refund_method,$refund_amount,$refund_note,$refund_reason,$receipt);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Orders();
            $class->setId($id);
            $class->setUid($uid);
            $class->setUsername($username);
            $class->setBankName($bankName);
            $class->setBankAccountHolder($bankAccountHolder);
            $class->setBankAccountNo($bankAccountNo);
            $class->setName($name);
            $class->setContactNo($contactNo);
            $class->setEmail($email);
            $class->setAddressLine1($address_line_1);
            $class->setAddressLine2($address_line_2);
            $class->setAddressLine3($address_line_3);
            $class->setCity($city);
            $class->setZipcode($zipcode);
            $class->setState($state);
            $class->setCountry($country);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);
            $class->setSubtotal($subtotal);
            $class->setTotal($total);

            $class->setPaymentMethod($payment_method);
            $class->setPaymentAmount($payment_amount);
            $class->setPaymentBankReference($payment_bankreference);
            $class->setPaymentDate($payment_date);
            $class->setPaymentTime($payment_time);
            $class->setPaymentStatus($payment_status);

            $class->setShippingStatus($shipping_status);
            $class->setShippingMethod($shipping_method);
            $class->setShippingDate($shipping_date);
            $class->setTrackingNumber($tracking_number);
            $class->setRejectReason($reject_reason);

            $class->setRefundMethod($refund_method);
            $class->setRefundAmount($refund_amount);
            $class->setRefundNote($refund_note);
            $class->setRefundReason($refund_reason);
            $class->setReceipt($receipt);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
