<?php
class TransferPointReport {
    /* Member variables */
    var $sendUid,$sendName,$sendAmount,$receiveName,$receiveUid,$dateCreated,$status;

    /**
     * @return mixed
     */
    public function getSendUid()
    {
        return $this->send_uid;
    }

    /**
     * @param mixed $id
     */
    public function setSendUid($sendUid)
    {
        $this->send_uid = $sendUid;
    }

    /**
     * @return mixed
     */
    public function getSendName()
    {
        return $this->send_name;
    }

    /**
     * @param mixed $id
     */
    public function setSendName($sendName)
    {
        $this->send_name = $sendName;
    }

    /**
     * @return mixed
     */
    public function getSendAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $id
     */
    public function setSendAmount($sendAmount)
    {
        $this->amount = $sendAmount;
    }

    /**
     * @return mixed
     */
    public function getReceiveName()
    {
        return $this->receive_name;
    }

    /**
     * @param mixed $id
     */
    public function setReceiveName($receiveName)
    {
        $this->receive_name = $receiveName;
    }

    /**
     * @return mixed
     */
    public function getReceiveUid()
    {
        return $this->receive_uid;
    }

    /**
     * @param mixed $id
     */
    public function setReceiveUid($receiveUid)
    {
        $this->receive_uid = $receiveUid;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * @param mixed $id
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $id
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }


}

function getTransferPointReport($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("send_uid","send_name","amount","receive_name","receive_uid","create_date","status");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"transfer_point");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($sendUid,$sendName,$sendAmount,$receiveName,$receiveUid,$dateCreated,$status);
                    // array("id","withdrawal_number", "withdrawal_status", "final_amount","date_create");

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new TransferPointReport();
            $class->setSendUid($sendUid);
            $class->setSendName($sendName);
            $class->setSendAmount($sendAmount);
            $class->setReceiveName($receiveName);
            $class->setReceiveUid($receiveUid);
            $class->setDateCreated($dateCreated);
            $class->setStatus($status);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }

}

            //todo this 2 code is AFTER payment successfully done then only execute
//            insertIntoTransactionHistory($conn,$totalPrice,0,$uid,null,null,null,2,null,$orderId,3,null,null);
//
