<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';

$uid = $_SESSION['uid'];

function registerNewUser($conn,$uid,$username,$fullname,$email,$finalPassword,$salt,$icNo,$referrerUid = null,$topReferrerUid = null,$referralName = null,$currentLevel = null)
{
$isReferred = 0;
if($referrerUid && $topReferrerUid)
{
$isReferred = 1;
}

if(insertDynamicData($conn,"user",array("uid","username","full_name","email","password","salt","ic_no","is_referred"),
     array($uid,$username,$fullname,$email,$finalPassword,$salt,$icNo,$isReferred),"sssssssi") === null)
{
     header('Location: ../addReferee.php?promptError=1');
     //     promptError("error registering new account.The account already exist");
     //     return false;
}
else
{
     if($isReferred === 1)
     {
     if(insertDynamicData($conn,"referral_history",array("referrer_id","referral_id","referral_name","current_level","top_referrer_id"),
          array($referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid),"sssis") === null)
          {
               header('Location: ../addReferee.php?promptError=2');
               //   promptError("error assigning referral relationship");
               //   return false;
          }
     }
}
return true;
}


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $register_uid = md5(uniqid());

     $register_username = rewrite($_POST['register_username']);
     $register_fullname = rewrite($_POST['register_fullname']);
     $register_ic_no = rewrite($_POST['register_ic_no']);
     $register_email_user = $_POST['register_email_user'];

     $register_username_referrer = $_POST['register_username_referrer'];

     $register_password = $_POST['register_password'];
     $register_password_validation = strlen($register_password);
     $register_retype_password = $_POST['register_retype_password'];
     $password = hash('sha256',$register_password);
     $salt = substr(sha1(mt_rand()), 0, 100);
     $finalPassword = hash('sha256', $salt.$password);

     // $downline_number = $_POST['downline_number'];
     // $bonus = $_POST['bonus'];

     //   FOR DEBUGGING 
     // echo "<br>";
     // echo $register_uid."<br>";

          if($register_password == $register_retype_password)
          {
               if($register_password_validation >= 6)
               {
                    if($register_username_referrer)
                    {
                         $referrerUserRows = getUser($conn," WHERE username = ? ",array("username"),array($register_username_referrer),"s");

                         if($referrerUserRows)
                         {
                              $referrerUid = $referrerUserRows[0]->getUid();
                              $topReferrerUid = $referrerUid;//assign top referrer id to this guy 1st, if he is not the top, will be overwritten
                              $referralName = $register_username;
                              $currentLevel = 1;
                  
                              $referralHistoryRows = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($referrerUid),"s");

                              if($referralHistoryRows)
                              {
                                  $topReferrerUid = $referralHistoryRows[0]->getTopReferrerId();
                                  $currentLevel = $referralHistoryRows[0]->getCurrentLevel() + 1;
                              }

                              $usernameRows = getUser($conn," WHERE username = ? ",array("username"),array($_POST['register_username']),"s");
                              $usernameDetails = $usernameRows[0];

                              $fullnameRows = getUser($conn," WHERE full_name = ? ",array("full_name"),array($_POST['register_fullname']),"s");
                              $fullnameDetails = $fullnameRows[0];

                              $icNoRows = getUser($conn," WHERE ic_no = ? ",array("ic_no"),array($_POST['register_ic_no']),"i");
                              $icNoDetails = $icNoRows[0];

                              $userEmailRows = getUser($conn," WHERE email = ? ",array("email"),array($_POST['register_email_user']),"s");
                              $userEmailDetails = $userEmailRows[0];


                              if (!$usernameDetails && !$fullnameDetails && !$icNoDetails && !$userEmailDetails)
                              {

                                   if(registerNewUser($conn,$register_uid,$register_username,$register_fullname,$register_email_user,$finalPassword,$salt,$register_ic_no,$referrerUid,$topReferrerUid,$referralName,$currentLevel))
                                   {

                                        $uplineRows = getUser($conn," WHERE username = ? ",array("username"),array($register_username_referrer),"s");
                                        $uplineDetails = $uplineRows[0];

                                        $registerBonus = $uplineDetails->getBonus();
                                        $registeredDownlineNumber = $uplineDetails->getTotalDownlineNo();
                                        $registeredDownlineNumberLevelOne = $uplineDetails->getDownlineLvlOne();
                                        if($registeredDownlineNumber <= 2)
                                        {
                                             // echo $registeredDownlineNumber."<br>";
                                             // echo $registerBonus."<br>";
                                             $uplineRegisterPoints = $registerBonus + 200;
                                             $uplineRegisteredDonlineNo = $registeredDownlineNumber + 1;

                                             $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
                                             if(!$user)
                                             {
                                                  $tableName = array();
                                                  $tableValue =  array();
                                                  $stringType =  "";
                                                  //echo "save to database";
                                                  if($uplineRegisteredDonlineNo)
                                                  {
                                                       array_push($tableName,"total_downline_no");
                                                       array_push($tableValue,$uplineRegisteredDonlineNo);
                                                       $stringType .=  "s";
                                                  }
                                                  if($uplineRegisterPoints)
                                                  {
                                                       array_push($tableName,"bonus");
                                                       array_push($tableValue,$uplineRegisterPoints);
                                                       $stringType .=  "s";
                                                  }

                                                  array_push($tableValue,$register_username_referrer);
                                                  $stringType .=  "s";
                                                  $registerPointUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
                                                  if($registerPointUpdated)
                                                  { }
                                                  else
                                                  { }
                                             }
                                        }

                                        elseif($registeredDownlineNumber >= 3 && $registeredDownlineNumber < 4)
                                        {
                                             $uplineRegisterPoints = $registerBonus + 200;
                                             $uplineRegisteredDonlineNo = $registeredDownlineNumber + 1;
                                             $uplineStatus = 'Platinium';

                                             $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
                                             if(!$user)
                                             {
                                                  $tableName = array();
                                                  $tableValue =  array();
                                                  $stringType =  "";
                                                  //echo "save to database";
                                                  if($uplineRegisteredDonlineNo)
                                                  {
                                                       array_push($tableName,"total_downline_no");
                                                       array_push($tableValue,$uplineRegisteredDonlineNo);
                                                       $stringType .=  "s";
                                                  }
                                                  if($uplineRegisterPoints)
                                                  {
                                                       array_push($tableName,"bonus");
                                                       array_push($tableValue,$uplineRegisterPoints);
                                                       $stringType .=  "s";
                                                  }
                                                  if($uplineStatus)
                                                  {
                                                       array_push($tableName,"current_status");
                                                       array_push($tableValue,$uplineStatus);
                                                       $stringType .=  "s";
                                                  }
                                                  array_push($tableValue,$register_username_referrer);
                                                  $stringType .=  "s";
                                                  $registerPointUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
                                                  if($registerPointUpdated)
                                                  { }
                                                  else
                                                  { }
                                             }
                                        }

                                        elseif($registeredDownlineNumber >= 4 && $registeredDownlineNumberLevelOne <= 4)
                                        {
                                             $uplineRegisterPoints = $registerBonus + 128;
                                             $uplineRegisteredDonlineNo = $registeredDownlineNumber + 1;
                                             $uplineRegisteredDonlineNoLvlOne = $registeredDownlineNumberLevelOne + 1;
                                             $uplineStatus = 'Platinium';

                                             $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
                                             if(!$user)
                                             {
                                                  $tableName = array();
                                                  $tableValue =  array();
                                                  $stringType =  "";
                                                  //echo "save to database";
                                                  if($uplineRegisteredDonlineNo)
                                                  {
                                                       array_push($tableName,"total_downline_no");
                                                       array_push($tableValue,$uplineRegisteredDonlineNo);
                                                       $stringType .=  "s";
                                                  }
                                                  if($uplineRegisterPoints)
                                                  {
                                                       array_push($tableName,"bonus");
                                                       array_push($tableValue,$uplineRegisterPoints);
                                                       $stringType .=  "s";
                                                  }
                                                  if($uplineStatus)
                                                  {
                                                       array_push($tableName,"current_status");
                                                       array_push($tableValue,$uplineStatus);
                                                       $stringType .=  "s";
                                                  }
                                                  if($uplineRegisteredDonlineNoLvlOne)
                                                  {
                                                       array_push($tableName,"downline_no_lvlone");
                                                       array_push($tableValue,$uplineRegisteredDonlineNoLvlOne);
                                                       $stringType .=  "s";
                                                  }

                                                  array_push($tableValue,$register_username_referrer);
                                                  $stringType .=  "s";
                                                  $registerPointUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
                                                  if($registerPointUpdated)
                                                  { }
                                                  else
                                                  { }
                                             }
                                        }
                                        elseif($registeredDownlineNumber >= 4 && $registeredDownlineNumberLevelOne <= 5)
                                        { 
                                             $uplineRegisterPoints = $registerBonus + 128;
                                             $uplineRegisteredDonlineNo = $registeredDownlineNumber + 1;
                                             $uplineRegisteredDonlineNoLvlOne = $registeredDownlineNumberLevelOne + 1;
                                             $uplineStatus = 'Titanium';

                                             $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");
                                             if(!$user)
                                             {
                                                  $tableName = array();
                                                  $tableValue =  array();
                                                  $stringType =  "";
                                                  //echo "save to database";
                                                  if($uplineRegisteredDonlineNo)
                                                  {
                                                       array_push($tableName,"total_downline_no");
                                                       array_push($tableValue,$uplineRegisteredDonlineNo);
                                                       $stringType .=  "s";
                                                  }
                                                  if($uplineRegisterPoints)
                                                  {
                                                       array_push($tableName,"bonus");
                                                       array_push($tableValue,$uplineRegisterPoints);
                                                       $stringType .=  "s";
                                                  }
                                                  if($uplineStatus)
                                                  {
                                                       array_push($tableName,"current_status");
                                                       array_push($tableValue,$uplineStatus);
                                                       $stringType .=  "s";
                                                  }
                                                  if($uplineRegisteredDonlineNoLvlOne)
                                                  {
                                                       array_push($tableName,"downline_no_lvlone");
                                                       array_push($tableValue,$uplineRegisteredDonlineNoLvlOne);
                                                       $stringType .=  "s";
                                                  }

                                                  array_push($tableValue,$register_username_referrer);
                                                  $stringType .=  "s";
                                                  $registerPointUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
                                                  if($registerPointUpdated)
                                                  { }
                                                  else
                                                  { }
                                             }
                                        }

                                        // sendEmailForVerification($register_uid);
                                        $_SESSION['messageType'] = 1;
                                        header('Location: ../addReferee.php?type=1');

                                        // echo $uplineRegisterPoints."<br>";
                                        // echo $uplineRegisteredDonlineNo."<br>";
                                   }
                              }
                              else
                              {
                                   // header('Location: ../addReferee.php?promptError=1');
                              }
                         }
                         else
                         {
                              // $_SESSION['messageType'] = 1;
                              // header('Location: ../addReferee.php?type=5');
                         }
                    }
                    else 
                    { }
               }
               else 
               {
                    // $_SESSION['messageType'] = 1;
                    // header('Location: ../addReferee.php?type=5');
               }
          }
          else 
          {
               // $_SESSION['messageType'] = 1;
               // header('Location: ../addReferee.php?type=5');
          }   
    
}
else 
{
     header('Location: ../addReferee.php');
}
?>