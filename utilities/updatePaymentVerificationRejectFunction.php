<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Product.php';
require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/ProductOrders.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';
require_once dirname(__FILE__) . '/allNoticeModals.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        $conn = connDB();

        // $payment_status = rewrite($_POST["payment_status"]);
        // $shipping_status = rewrite($_POST["shipping_status"]);
        $payment_status = "REJECT";
        $shipping_status = "REJECT";
        $shipping_date = rewrite($_POST["shipping_date"]);
        $order_id = rewrite($_POST["order_id"]);

        //for debugging
        // echo "<br>";
        // echo $_POST['order_id']."<br>";
        // echo $payment_status."<br>";
        // echo $shipping_status."<br>";
        // echo $shipping_date."<br>";

        if(isset($_POST['order_id']))
        {   
            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";
            if($payment_status)
            {
                array_push($tableName,"payment_status");
                array_push($tableValue,$payment_status);
                $stringType .=  "s";
            }     
            if($shipping_status)
            {
                array_push($tableName,"shipping_status");
                array_push($tableValue,$shipping_status);
                $stringType .=  "s";
            }     
            if($shipping_date)
            {
                array_push($tableName,"shipping_date");
                array_push($tableValue,$shipping_date);
                $stringType .=  "s";
            } 

            array_push($tableValue,$order_id);
            $stringType .=  "s";
            $orderUpdated = updateDynamicData($conn,"orders"," WHERE id = ? ",$tableName,$tableValue,$stringType);
            
            if($orderUpdated)
            {
                // echo "<br>";
                // echo $_POST['order_id']."<br>";
                // echo $payment_status."<br>";
                // echo $shipping_status."<br>";
                // echo $shipping_date."<br>";
                // echo $shipping_status."<br>";
                // echo "success";
                $_SESSION['messageType'] = 1;
                header('Location: ../adminShipping.php?type=11');
            }
            else
            {
                //echo "fail";
                $_SESSION['messageType'] = 1;
                header('Location: ../adminPayment.php?type=2');
            }
        }
        else
        {
            //echo "dunno";
            $_SESSION['messageType'] = 1;
            header('Location: ../adminPayment.php?type=3');
        }

    }
else 
{
    header('Location: ../adminPayment.php');
}

?>