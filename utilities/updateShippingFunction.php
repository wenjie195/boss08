<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Product.php';
require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/ProductOrders.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';
require_once dirname(__FILE__) . '/allNoticeModals.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        $conn = connDB();

        $shipping_method = rewrite($_POST["shipping_method"]);
        $tracking_number = rewrite($_POST["tracking_number"]);
        $shipping_date = rewrite($_POST["shipping_date"]);
        $shipping_status = rewrite($_POST["shipping_status"]);
        $order_id = rewrite($_POST["order_id"]);

        // echo $order_id."<br>";
        // echo $shipping_method."<br>";

        if(isset($_POST['order_id']))
        {
            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";
            if($shipping_method)
            {
                array_push($tableName,"shipping_method");
                array_push($tableValue,$shipping_method);
                $stringType .=  "s";
            }
            if($tracking_number)
            {
                array_push($tableName,"tracking_number");
                array_push($tableValue,$tracking_number);
                $stringType .=  "s";
            }
            if($shipping_date)
            {
                array_push($tableName,"shipping_date");
                array_push($tableValue,$shipping_date);
                $stringType .=  "s";
            }
            if($shipping_status)
            {
                array_push($tableName,"shipping_status");
                array_push($tableValue,$shipping_status);
                $stringType .=  "s";
            }

            array_push($tableValue,$order_id);
            $stringType .=  "s";
            $orderUpdated = updateDynamicData($conn,"orders"," WHERE id = ? ",$tableName,$tableValue,$stringType);

            if($orderUpdated)
            {

                $_SESSION['messageType'] = 1;
                header('Location: ../adminShipping.php?type=1');
            }
            else
            {
                // echo "fail";
                $_SESSION['messageType'] = 1;
                header('Location: ../adminShipping.php?type=2');
            }
        }
        else
        {
            // echo "dunno";
            $_SESSION['messageType'] = 1;
            header('Location: ../adminShipping.php?type=3');
        }

        if(isset($_POST['order_id']))
        {
            $conn = connDB();
            //Order
            $orderArray = getOrders($conn,"WHERE id = ? ", array("id") ,array($_POST['order_id']),"i");
            //OrderProduct
            $orderProductArray = getProductOrders($conn,"WHERE order_id = ? ", array("order_id") ,array($_POST['order_id']),"i");
            //$orderDetails = $orderArray[0];

            //Product Details
            //$abc = getProduct($conn,"WHERE product_id = ? ", array("product_id") ,array($_POST['order_id']),"i");

            if($orderArray != null)
            {
                for($cntAA = 0;$cntAA < count($orderProductArray) ;$cntAA++)
                {

                $product = getProduct($conn ,"WHERE id = ? ", array("name"),array($orderProductArray[$cntAA]->getProductId()),"i");
                    ?>

                <?php

                $buyQuantity = $orderProductArray[$cntAA]->getQuantity();
                $productName = $product[0] -> getName();
                $productPrice = $product[0] -> getPrice();
                $currentQuantity = $product[0] -> getBuyStock();
                $currentStock = $product[0] -> getStock();
                $currentCroductPrice = $product[0] -> getTotalPrice();

                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
                // //echo "save to database";
                if($buyQuantity)
                {

                  $totalQuantity = $currentStock - $buyQuantity;

                    array_push($tableName,"stock");
                    array_push($tableValue,$totalQuantity);
                    $stringType .=  "i";
                }
                if($buyQuantity)
                {

                  $totalBuyQuantity = $currentQuantity + $buyQuantity;

                    array_push($tableName,"buy_stock");
                    array_push($tableValue,$totalBuyQuantity);
                    $stringType .=  "i";
                }
                if($productPrice)
                {

                  $totalPriceBuy = $productPrice * $buyQuantity;
                  $currentCroductPriceFinal = $currentCroductPrice + $totalPriceBuy;

                    array_push($tableName,"total_price");
                    array_push($tableValue,$currentCroductPriceFinal);
                    $stringType .=  "i";
                }

                array_push($tableValue,$productName);
                $stringType .=  "s";
                $withdrawUpdated = updateDynamicData($conn,"product"," WHERE name = ? ",$tableName,$tableValue,$stringType);

                if($withdrawUpdated)
                {
                    // $_SESSION['messageType'] = 1;
                    // header('Location: ../shippingOut.php?type=1');
                }
                else
                {
                    echo "fail";

                }
                }
            }
        }

    }
else
{
    header('Location: ../adminShipping.php');
}

?>
