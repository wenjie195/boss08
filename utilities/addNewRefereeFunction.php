<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';

$uid = $_SESSION['uid'];

// $getAllUpline = getTop10ReferrerOfUser($conn,$uid);

function registerNewUser($conn,$uid,$username,$fullname,$email,$finalPassword,$salt,$icNo,$referrerUid = null,$topReferrerUid = null,$referralName = null,$currentLevel = null)
{
$isReferred = 0;
if($referrerUid && $topReferrerUid)
{
$isReferred = 1;
}

if(insertDynamicData($conn,"user",array("uid","username","full_name","email","password","salt","ic_no","is_referred"),
     array($uid,$username,$fullname,$email,$finalPassword,$salt,$icNo,$isReferred),"sssssssi") === null)
{
     header('Location: ../addReferee.php?promptError=1');
     //     promptError("error registering new account.The account already exist");
     //     return false;
}
else
{
     if($isReferred === 1)
     {
     if(insertDynamicData($conn,"referral_history",array("referrer_id","referral_id","referral_name","current_level","top_referrer_id"),
          array($referrerUid,$uid,$referralName,$currentLevel,$topReferrerUid),"sssis") === null)
          {
               header('Location: ../addReferee.php?promptError=2');
               //   promptError("error assigning referral relationship");
               //   return false;
          }
     }
}
return true;
}


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $register_uid = md5(uniqid());

     $register_username = rewrite($_POST['register_username']);
     $register_fullname = rewrite($_POST['register_fullname']);
     $register_ic_no = rewrite($_POST['register_ic_no']);
     $register_email_user = $_POST['register_email_user'];

     $register_username_referrer = $_POST['register_username_referrer'];

     $register_password = $_POST['register_password'];
     $register_password_validation = strlen($register_password);
     $register_retype_password = $_POST['register_retype_password'];
     $password = hash('sha256',$register_password);
     $salt = substr(sha1(mt_rand()), 0, 100);
     $finalPassword = hash('sha256', $salt.$password);

     // $downline_number = $_POST['downline_number'];
     // $bonus = $_POST['bonus'];

     //   FOR DEBUGGING
     // echo "<br>";
     // echo $register_uid."<br>";

          if($register_password == $register_retype_password)
          {
               if($register_password_validation >= 6)
               {
                    if($register_username_referrer)
                    {
                         $referrerUserRows = getUser($conn," WHERE username = ? ",array("username"),array($register_username_referrer),"s");

                         if($referrerUserRows)
                         {
                              $referrerUid = $referrerUserRows[0]->getUid();
                              $referrerDownlineNo = $referrerUserRows[0]->getTotalDownlineNo();
                              $topReferrerUid = $referrerUid;//assign top referrer id to this guy 1st, if he is not the top, will be overwritten
                              $referralName = $register_username;
                              $currentLevel = 1;

                              $referralHistoryRows = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($referrerUid),"s");

                              if($referralHistoryRows)
                              {
                                  $topReferrerUid = $referralHistoryRows[0]->getTopReferrerId();
                                  $currentLevel = $referralHistoryRows[0]->getCurrentLevel() + 1;
                              }

                              $usernameRows = getUser($conn," WHERE username = ? ",array("username"),array($_POST['register_username']),"s");
                              $usernameDetails = $usernameRows[0];

                              $fullnameRows = getUser($conn," WHERE full_name = ? ",array("full_name"),array($_POST['register_fullname']),"s");
                              $fullnameDetails = $fullnameRows[0];

                              $icNoRows = getUser($conn," WHERE ic_no = ? ",array("ic_no"),array($_POST['register_ic_no']),"i");
                              $icNoDetails = $icNoRows[0];

                              $userEmailRows = getUser($conn," WHERE email = ? ",array("email"),array($_POST['register_email_user']),"s");
                              $userEmailDetails = $userEmailRows[0];


                              if (!$usernameDetails && !$fullnameDetails && !$icNoDetails && !$userEmailDetails)
                              {
                                  $totalDownline = $referrerDownlineNo + 1;

                                   if(registerNewUser($conn,$register_uid,$register_username,$register_fullname,$register_email_user,$finalPassword,$salt,$register_ic_no,$referrerUid,$topReferrerUid,$referralName,$currentLevel))
                                   {
                                        $getAllUpline = getTop10ReferrerOfUser($conn,$uid);

                                        $directBonus = 200;
                                        $uplineBonus = 64;
                                        $uplineBonusTitanium = 64;
                                        if ($totalDownline >= 4) {
                                          $status = 'Platinium';
                                        }else {
                                          $status = 'Member';
                                        }


                                        $userUid = getUser($conn, "WHERE uid = ? ", array("uid"), array($uid), "s");
                                        $userPreviousBonus = $userUid[0] -> getBonus();
                                        $userPreviousCash = $userUid[0] -> getCash();
                                        // for boss bonus report
                                        $directUplineUid = $userUid[0] -> getUid();
                                        $directUplineUsername = $userUid[0] -> getUsername();

//===================================================================================================================
// platinium
                                        if ($userUid[0]->getCurrentStatus() == 'Platinium')
                                        {
                                             $finalBonus = $userPreviousBonus + $directBonus - 72;
                                             $finalCash = $userPreviousCash + $directBonus - 72;
                                             // $finalCash = $userPreviousCash + $directBonus - 72;
                                             if ($userUid) 
                                             {
                                                  $tableName = array();
                                                  $tableValue =  array();
                                                  $stringType =  "";
                                                  //echo "save to database";
                                                  if($finalBonus)
                                                  {
                                                       array_push($tableName,"bonus");
                                                       array_push($tableValue,$finalBonus);
                                                       $stringType .=  "s";
                                                  }
                                                  if($finalCash)
                                                  {
                                                       array_push($tableName,"cash");
                                                       array_push($tableValue,$finalCash);
                                                       $stringType .=  "s";
                                                  }
                                                  if($totalDownline)
                                                  {
                                                       array_push($tableName,"total_downline_no");
                                                       array_push($tableValue,$totalDownline);
                                                       $stringType .=  "s";
                                                  }
                                                  if($status)
                                                  {
                                                       array_push($tableName,"current_status");
                                                       array_push($tableValue,$status);
                                                       $stringType .=  "s";
                                                  }

                                                  array_push($tableValue,$uid);
                                                  // array_push($tableValue,$getAllUpline);
                                                  $stringType .=  "s";
                                                  $registerPointUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                  if($registerPointUpdated)
                                                  { 
                                                       //echo "success Member";

                                                       $newUsername = $referralName;
                                                       $receivedUid = $directUplineUid;
                                                       $receivedUsername = $directUplineUsername;
                                                       $platinumBonus = $directBonus - 72;
                                                       $amount = $platinumBonus;

                                                       // if (BonusFlow($conn,$newUsername,$receivedUsername,$receivedUid,$amount))
                                                       // {}// echo "success";
                                                       // else
                                                       // {}// echo "fail";

                                                       $bonusType = 'Direct Sponosr';
                                                       if (BonusFlow($conn,$newUsername,$receivedUsername,$receivedUid,$amount,$bonusType))
                                                       {}// echo "success";
                                                       else
                                                       {}// echo "fail";

                                                       for ($cnt=0; $cnt <count($getAllUpline) ; $cnt++)
                                                       {
                                                            $yourUpline = getUser($conn, "WHERE uid = ? ", array("uid"), array($getAllUpline[$cnt]), "s");
                                                            $yourUplineUid = $yourUpline[0]->getUid();

                                                            $getDownline = getReferralHistory($conn, "WHERE referrer_id = ? ", array("referrer_id"), array($yourUplineUid), "s");
                                                            $getDownlineUid = $getDownline[0]->getReferralId();

                                                            $downlineStatus = getUser($conn, "WHERE uid = ? ", array("uid"), array($getDownlineUid), "s");

                                                            if($yourUpline && $yourUpline[0]->getCurrentStatus() == 'Titanium' )
                                                            {
                                                                 // $uplineBonusTitanium = $userPreviousBonus + $uplineBonusTitanium - 24;

                                                                 $uplineBonus = $yourUpline[0] -> getBonus();
                                                                 $newUplineBonus = $uplineBonus + $uplineBonusTitanium;

                                                                 $uplineCash = $yourUpline[0] -> getCash();
                                                                 $newUplineCash = $uplineCash + $uplineBonusTitanium;

                                                                 $uplineUid = $yourUpline[0] -> getUid();
                                                                 $uplineUsername = $yourUpline[0] -> getUsername();

                                                                 //to new table for record bonus
                                                                 $newUsername = $referralName;
                                                                 $receivedUid = $uplineUid;
                                                                 $receivedUsername = $uplineUsername;
                                                                 $amount = $uplineBonusTitanium;

                                                                 if ($uplineBonusTitanium >= 40)
                                                                 {
                                                                      $tableName = array();
                                                                      $tableValue =  array();
                                                                      $stringType =  "";
                                                                      //echo "save to database";
                                                                      // if($uplineBonusTitanium)
                                                                      if($newUplineBonus)
                                                                      {
                                                                           array_push($tableName,"bonus");
                                                                           // array_push($tableValue,$uplineBonusTitanium);
                                                                           array_push($tableValue,$newUplineBonus);
                                                                           $stringType .=  "s";
                                                                      }
                                                                      if($newUplineCash)
                                                                      {
                                                                           array_push($tableName,"cash");
                                                                           array_push($tableValue,$newUplineCash);
                                                                           $stringType .=  "s";
                                                                      }
                                                                      array_push($tableValue,$yourUplineUid);
                                                                      // array_push($tableValue,$getAllUpline);
                                                                      $stringType .=  "s";
                                                                      $registerPointUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                                      if($registerPointUpdated)
                                                                      {
                                                                           //echo "success Titanium";

                                                                           //add on Bonus Type
                                                                           // if ($uplineBonusTitanium == 64)
                                                                           if ($amount == 64)
                                                                           {
                                                                                $bonusType = 'Leadership';
                                                                                if (BonusFlow($conn,$newUsername,$receivedUsername,$receivedUid,$amount,$bonusType))
                                                                                {}// echo "success";
                                                                                else
                                                                                {}// echo "fail";
                                                                           }
                                                                           else
                                                                           {    $bonusType = 'Same Level';
                                                                                if (BonusFlow($conn,$newUsername,$receivedUsername,$receivedUid,$amount,$bonusType))
                                                                                {}// echo "success";
                                                                                else
                                                                                {}// echo "fail";
                                                                           }

                                                                           // if (BonusFlow($conn,$newUsername,$receivedUsername,$receivedUid,$amount))
                                                                           // {}// echo "success";
                                                                           // else
                                                                           // {}// echo "fail";

                                                                      }
                                                                      else
                                                                      {
                                                                           echo "fail again"; 
                                                                      }
                                                                      $uplineBonusTitanium = $uplineBonusTitanium - 24;
                                                                 }
                                                            }
                                                            if ($yourUpline && $yourUpline[0]->getCurrentStatus() == 'Platinium' && $downlineStatus[0]->getCurrentStatus() != 'Titanium' && $uplineBonus != 40)
                                                            {
                                                                 // $uplineBonus = $uplineBonus - 24;

                                                                 $upUplineBonus = $yourUpline[0] -> getBonus();
                                                                 // $upUplineBonus1 =  $upUplineBonus - 24;
                                                                 // $newUpUplineBonus = $upUplineBonus1 + $uplineBonus;

                                                                 //correction for bonus
                                                                 $upUplineBonus1 =  $uplineBonus - 24;
                                                                 $newUpUplineBonus = $upUplineBonus + $upUplineBonus1;
                                                                 
                                                                 //add for cash 
                                                                 $upUplineCash = $yourUpline[0] -> getCash();
                                                                 $upUplineCashPreCal =  $uplineBonus - 24;
                                                                 $newUpUplineCash = $upUplineCash + $upUplineCashPreCal;

                                                                 $uplineUid = $yourUpline[0] -> getUid();
                                                                 $uplineUsername = $yourUpline[0] -> getUsername();
                                                                 
                                                                 //to new table for record bonus
                                                                 $newUsername = $referralName;
                                                                 $receivedUsername = $uplineUsername;
                                                                 $receivedUid = $uplineUid;
                                                                 // $amount = $uplineBonus;
                                                                 $amount = $upUplineBonus1;

                                                                 if ($uplineBonus >= 40)
                                                                 {
                                                                      $tableName = array();
                                                                      $tableValue =  array();
                                                                      $stringType =  "";
                                                                      //echo "save to database";
                                                                      // if($uplineBonus)
                                                                      if($newUpUplineBonus)
                                                                      {
                                                                           array_push($tableName,"bonus");
                                                                           // array_push($tableValue,$uplineBonus);
                                                                           array_push($tableValue,$newUpUplineBonus);
                                                                           $stringType .=  "s";
                                                                      }
                                                                      if($newUpUplineCash)
                                                                      {
                                                                           array_push($tableName,"cash");
                                                                           array_push($tableValue,$newUpUplineCash);
                                                                           $stringType .=  "s";
                                                                      }
                                                                      array_push($tableValue,$yourUplineUid);
                                                                      // array_push($tableValue,$getAllUpline);
                                                                      $stringType .=  "s";
                                                                      $registerPointUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                                      if($registerPointUpdated)
                                                                      {
                                                                           //echo "success Platinium";

                                                                           //add on Bonus Type
                                                                           // if ($uplinamounteBonus == 64)
                                                                           if ($amount == 64)
                                                                           {
                                                                                $bonusType = 'Leadership';
                                                                                if (BonusFlow($conn,$newUsername,$receivedUsername,$receivedUid,$amount,$bonusType))
                                                                                {}// echo "success";
                                                                                else
                                                                                {}// echo "fail";
                                                                           }
                                                                           else
                                                                           {    $bonusType = 'Same Level';
                                                                                if (BonusFlow($conn,$newUsername,$receivedUsername,$receivedUid,$amount,$bonusType))
                                                                                {}// echo "success";
                                                                                else
                                                                                {}// echo "fail";
                                                                           }

                                                                           // if (BonusFlow($conn,$newUsername,$receivedUsername,$receivedUid,$amount))
                                                                           // {}// echo "success";
                                                                           // else
                                                                           // {}// echo "fail";
                                                                      }
                                                                      else
                                                                      {
                                                                           echo "fail again";
                                                                      }
                                                                 $uplineBonus = $uplineBonus - 24;
                                                                 }
                                                                 else
                                                                 {

                                                                 }
                                                            }
                                                       }
                                                  }
                                                  else
                                                  {
                                                       echo "fail";
                                                  }
                                             }
                                        }

//===================================================================================================================
// member
                                        if ($userUid[0]->getCurrentStatus() == 'Member')
                                        {
                                        $finalBonus = $userPreviousBonus + $directBonus;
                                        $finalCash = $userPreviousCash + $directBonus;
                                        if ($userUid)
                                             {
                                                  $tableName = array();
                                                  $tableValue =  array();
                                                  $stringType =  "";
                                                  //echo "save to database";
                                                  if($finalBonus)
                                                  {
                                                       array_push($tableName,"bonus");
                                                       array_push($tableValue,$finalBonus);
                                                       $stringType .=  "s";
                                                  }
                                                  if($finalCash)
                                                  {
                                                       array_push($tableName,"cash");
                                                       array_push($tableValue,$finalCash);
                                                       $stringType .=  "s";
                                                  }
                                                  if($totalDownline)
                                                  {
                                                       array_push($tableName,"total_downline_no");
                                                       array_push($tableValue,$totalDownline);
                                                       $stringType .=  "s";
                                                  }
                                                  if($status)
                                                  {
                                                       array_push($tableName,"current_status");
                                                       array_push($tableValue,$status);
                                                       $stringType .=  "s";
                                                  }
                                                  array_push($tableValue,$uid);
                                                  // array_push($tableValue,$getAllUpline);
                                                  $stringType .=  "s";
                                                  $registerPointUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                  if($registerPointUpdated)
                                                  {    

                                                       $newUsername = $referralName;
                                                       $receivedUid = $directUplineUid;
                                                       $receivedUsername = $directUplineUsername;
                                                       $amount = $directBonus;
                                                       
                                                       // if (BonusFlow($conn,$newUsername,$receivedUsername,$receivedUid,$amount))
                                                       // {}// echo "success";
                                                       // else
                                                       // {}// echo "fail";

                                                       $bonusType = 'Direct Sponosr';
                                                       if (BonusFlow($conn,$newUsername,$receivedUsername,$receivedUid,$amount,$bonusType))
                                                       {}// echo "success";
                                                       else
                                                       {}// echo "fail";
                                                       
                                                       //echo "success Member";
                                                       for ($cnt=0; $cnt <count($getAllUpline) ; $cnt++)
                                                       {
                                                            $yourUpline = getUser($conn, "WHERE uid = ? ", array("uid"), array($getAllUpline[$cnt]), "s");
                                                            $yourUplineUid = $yourUpline[0]->getUid();

                                                            $getDownline = getReferralHistory($conn, "WHERE referrer_id = ? ", array("referrer_id"), array($yourUplineUid), "s");
                                                            $getDownlineUid = $getDownline[0]->getReferralId();

                                                            $downlineStatus = getUser($conn, "WHERE uid = ? ", array("uid"), array($getDownlineUid), "s");

                                                            if($yourUpline && $yourUpline[0]->getCurrentStatus() == 'Titanium' )
                                                            {
                                                                 //$uplineBonusTitanium = $userPreviousBonus + $uplineBonusTitanium;
                                                                 $uplineBonus = $yourUpline[0] -> getBonus();
                                                                 $newUplineBonus = $uplineBonus + $uplineBonusTitanium;

                                                                 // add cash
                                                                 $uplineCash = $yourUpline[0] -> getCash();
                                                                 $newUplineCash = $uplineCash + $uplineBonusTitanium;

                                                                 $uplineUid = $yourUpline[0] -> getUid();
                                                                 $uplineUsername = $yourUpline[0] -> getUsername();
                                                                 
                                                                 //to new table for record bonus
                                                                 $newUsername = $referralName;
                                                                 $receivedUid = $uplineUid;
                                                                 $receivedUsername = $uplineUsername;
                                                                 $amount = $uplineBonusTitanium;

                                                                 if ($uplineBonusTitanium >= 40)
                                                                 {
                                                                      $tableName = array();
                                                                      $tableValue =  array();
                                                                      $stringType =  "";
                                                                      //echo "save to database";
                                                                      if($newUplineBonus)
                                                                      {
                                                                           array_push($tableName,"bonus");
                                                                           array_push($tableValue,$newUplineBonus);
                                                                           $stringType .=  "s";
                                                                      }
                                                                      if($newUplineCash)
                                                                      {
                                                                           array_push($tableName,"cash");
                                                                           array_push($tableValue,$newUplineCash);
                                                                           $stringType .=  "s";
                                                                      }

                                                                      array_push($tableValue,$yourUplineUid);
                                                                      // array_push($tableValue,$getAllUpline);
                                                                      $stringType .=  "s";
                                                                      $registerPointUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                                      if($registerPointUpdated)
                                                                      {
                                                                           //echo "success Titanium";

                                                                           //add on Bonus Type
                                                                           // if ($uplineBonusTitanium == 64)
                                                                           if ($amount == 64)
                                                                           {
                                                                                $bonusType = 'Leadership';
                                                                                if (BonusFlow($conn,$newUsername,$receivedUsername,$receivedUid,$amount,$bonusType))
                                                                                {}// echo "success";
                                                                                else
                                                                                {}// echo "fail";
                                                                           }
                                                                           else
                                                                           {    $bonusType = 'Same Level';
                                                                                if (BonusFlow($conn,$newUsername,$receivedUsername,$receivedUid,$amount,$bonusType))
                                                                                {}// echo "success";
                                                                                else
                                                                                {}// echo "fail";
                                                                           }

                                                                           // if (BonusFlow($conn,$newUsername,$receivedUsername,$receivedUid,$amount))
                                                                           // {}// echo "success";
                                                                           // else
                                                                           // {}// echo "fail";

                                                                      }
                                                                      else
                                                                      {
                                                                           echo "fail again"; 
                                                                      }

                                                                           // if (BonusFlow($conn,$newUsername,$receivedUsername,$receivedUid,$amount))
                                                                           // {}// echo "success";
                                                                           // else
                                                                           // {}// echo "fail";

                                                                      $uplineBonusTitanium = $uplineBonusTitanium - 24;
                                                                 }
                                                            }
                                                            if ($yourUpline && $yourUpline[0]->getCurrentStatus() == 'Platinium' && $uplineBonusTitanium != 40)
                                                            {
                                                                 //$uplineBonus = $userPreviousBonus + $uplineBonus;
                                                                 $upUplineBonus = $yourUpline[0] -> getBonus();
                                                                 $newUpUplineBonus = $upUplineBonus + $uplineBonus;

                                                                 //asdd cash
                                                                 $upUplineCash = $yourUpline[0] -> getCash();
                                                                 $newUpUplineCash = $upUplineCash + $uplineBonus;

                                                                 $uplineUid = $yourUpline[0] -> getUid();
                                                                 $uplineUsername = $yourUpline[0] -> getUsername();
                                                                 
                                                                 //to new table for record bonus
                                                                 $newUsername = $referralName;
                                                                 $receivedUid = $uplineUid;
                                                                 $receivedUsername = $uplineUsername;
                                                                 $amount = $uplineBonus;

                                                                 if ($uplineBonus >= 40)
                                                                 {
                                                                      $tableName = array();
                                                                      $tableValue =  array();
                                                                      $stringType =  "";
                                                                      //echo "save to database";
                                                                      if($newUpUplineBonus)
                                                                      {
                                                                           array_push($tableName,"bonus");
                                                                           array_push($tableValue,$newUpUplineBonus);
                                                                           $stringType .=  "s";
                                                                      }
                                                                      if($newUpUplineCash)
                                                                      {
                                                                           array_push($tableName,"cash");
                                                                           array_push($tableValue,$newUpUplineCash);
                                                                           $stringType .=  "s";
                                                                      }

                                                                      array_push($tableValue,$yourUplineUid);
                                                                      // array_push($tableValue,$getAllUpline);
                                                                      $stringType .=  "s";
                                                                      $registerPointUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                                      if($registerPointUpdated)
                                                                      {
                                                                           //echo "success Platinium";
                                                                      
                                                                           // if ($uplineBonus == 64)
                                                                           // $bonusType = 'Overriding';

                                                                           //add on Bonus Type
                                                                           // if ($uplineBonus == 64)
                                                                           if ($amount == 64)
                                                                           {
                                                                                $bonusType = 'Leadership';
                                                                                if (BonusFlow($conn,$newUsername,$receivedUsername,$receivedUid,$amount,$bonusType))
                                                                                {}// echo "success";
                                                                                else
                                                                                {}// echo "fail";
                                                                           }
                                                                           else
                                                                           {    $bonusType = 'Same Level';
                                                                                if (BonusFlow($conn,$newUsername,$receivedUsername,$receivedUid,$amount,$bonusType))
                                                                                {}// echo "success";
                                                                                else
                                                                                {}// echo "fail";
                                                                           }

                                                                           // if (BonusFlow($conn,$newUsername,$receivedUsername,$receivedUid,$amount))
                                                                           // {}// echo "success";
                                                                           // else
                                                                           // {}// echo "fail";
                                                                      }
                                                                      else
                                                                      {
                                                                           echo "fail again"; 
                                                                      }

                                                                           // if (BonusFlow($conn,$newUsername,$receivedUsername,$receivedUid,$amount))
                                                                           // {}// echo "success";
                                                                           // else
                                                                           // {}// echo "fail";

                                                                      $uplineBonus = $uplineBonus - 24;
                                                                 }
                                                            }
                                                       }
                                                  }
                                                  else
                                                  {
                                                       echo "fail"; 
                                                  }
                                             }
                                        }

//=============================================================================================================================
// titanium
                                        if ($userUid[0]->getCurrentStatus() == 'Titanium')
                                        {
                                             $finalBonus = $userPreviousBonus + $directBonus - 72 + 64;
                                             $finalCash = $userPreviousCash + $directBonus - 72 + 64;
                                             // $statusTitanium = 'Titanium';

                                             if ($userUid)
                                             {
                                                  $tableName = array();
                                                  $tableValue =  array();
                                                  $stringType =  "";
                                                  //echo "save to database";
                                                  if($finalBonus)
                                                  {
                                                       array_push($tableName,"bonus");
                                                       array_push($tableValue,$finalBonus);
                                                       $stringType .=  "s";
                                                  }
                                                  if($finalCash)
                                                  {
                                                       array_push($tableName,"cash");
                                                       array_push($tableValue,$finalCash);
                                                       $stringType .=  "s";
                                                  }
                                                  if($totalDownline)
                                                  {
                                                       array_push($tableName,"total_downline_no");
                                                       array_push($tableValue,$totalDownline);
                                                       $stringType .=  "s";
                                                  }
                                                  // if($statusTitanium)
                                                  // {
                                                  //      array_push($tableName,"current_status");
                                                  //      array_push($tableValue,$statusTitanium);
                                                  //      $stringType .=  "s";
                                                  // }

                                                  array_push($tableValue,$uid);
                                                  // array_push($tableValue,$getAllUpline);
                                                  $stringType .=  "s";
                                                  $registerPointUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                  if($registerPointUpdated)
                                                  {
                                                       //echo "success Member";

                                                       $newUsername = $referralName;
                                                       $receivedUid = $directUplineUid;
                                                       $receivedUsername = $directUplineUsername;
                                                       $titaniumBonus = $directBonus - 8;
                                                       $amount = $titaniumBonus;

                                                       // if (BonusFlow($conn,$newUsername,$receivedUsername,$receivedUid,$amount))
                                                       // {}// echo "success";
                                                       // else
                                                       // {}// echo "fail";

                                                       $bonusType = 'Direct Sponosr';
                                                       if (BonusFlow($conn,$newUsername,$receivedUsername,$receivedUid,$amount,$bonusType))
                                                       {}// echo "success";
                                                       else
                                                       {}// echo "fail";

                                                       for ($cnt=0; $cnt <count($getAllUpline) ; $cnt++)
                                                       {
                                                            $yourUpline = getUser($conn, "WHERE uid = ? ", array("uid"), array($getAllUpline[$cnt]), "s");
                                                            $yourUplineUid = $yourUpline[0]->getUid();

                                                            $getDownline = getReferralHistory($conn, "WHERE referrer_id = ? ", array("referrer_id"), array($yourUplineUid), "s");
                                                            $getDownlineUid = $getDownline[0]->getReferralId();

                                                            $downlineStatus = getUser($conn, "WHERE uid = ? ", array("uid"), array($getDownlineUid), "s");

                                                            // if($yourUpline && $yourUpline[0]->getCurrentStatus() == 'Titanium')
                                                            if($yourUpline && $yourUpline[0]->getCurrentStatus() == 'Titanium' && $uplineBonusTitanium != 40)
                                                            {
                                                                 $uplineBonus = $yourUpline[0] -> getBonus();
                                                                 $UplineBonusTita = $uplineBonus + $uplineBonusTitanium;
                                                                 $newUplineBonusTita = $UplineBonusTita - 24;
                                                                 // $bonus4TitaniumUpline = $uplineBonusTitanium - 24;
                                                                 
                                                                 //add cash
                                                                 $uplineCash = $yourUpline[0] -> getCash();
                                                                 $UplineCashTita = $uplineCash + $uplineBonusTitanium;
                                                                 $newUplineCashTita = $UplineCashTita - 24;

                                                                 $uplineUid = $yourUpline[0] -> getUid();
                                                                 $uplineUsername = $yourUpline[0] -> getUsername();

                                                                 $newUsername = $referralName;
                                                                 $receivedUid = $uplineUid;
                                                                 $receivedUsername = $uplineUsername;
                                                                 $titaniumBonusRecord = $uplineBonusTitanium - 24;
                                                                 $amount = $titaniumBonusRecord;

                                                                 if ($uplineBonusTitanium >= 40)
                                                                 {
                                                                      $tableName = array();
                                                                      $tableValue =  array();
                                                                      $stringType =  "";
                                                                      //echo "save to database";
                                                                      if($newUplineBonusTita)
                                                                      {
                                                                           array_push($tableName,"bonus");
                                                                           array_push($tableValue,$newUplineBonusTita);
                                                                           $stringType .=  "s";
                                                                      }
                                                                      if($newUplineCashTita)
                                                                      {
                                                                           array_push($tableName,"cash");
                                                                           array_push($tableValue,$newUplineCashTita);
                                                                           $stringType .=  "s";
                                                                      }
                                                                      array_push($tableValue,$yourUplineUid);
                                                                      // array_push($tableValue,$getAllUpline);
                                                                      $stringType .=  "s";
                                                                      $registerPointUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                                      if($registerPointUpdated)
                                                                      {
                                                                           //echo "success Titanium";

                                                                           // if (BonusFlow($conn,$newUsername,$receivedUsername,$receivedUid,$amount))
                                                                           // {}// echo "success";
                                                                           // else
                                                                           // {}// echo "fail";

                                                                           // if ($uplineBonusTitanium == 64)
                                                                           if ($amount == 64)
                                                                           {
                                                                                $bonusType = 'Leadership';
                                                                                if (BonusFlow($conn,$newUsername,$receivedUsername,$receivedUid,$amount,$bonusType))
                                                                                {}// echo "success";
                                                                                else
                                                                                {}// echo "fail";
                                                                           }
                                                                           else
                                                                           {    $bonusType = 'Same Level';
                                                                                if (BonusFlow($conn,$newUsername,$receivedUsername,$receivedUid,$amount,$bonusType))
                                                                                {}// echo "success";
                                                                                else
                                                                                {}// echo "fail";
                                                                           }

                                                                      }
                                                                      else
                                                                      {
                                                                           echo "fail again tt";
                                                                      }
                                                                      $uplineBonusTitanium = $uplineBonusTitanium - 24;
                                                                 }
                                                            }
                                                       }
                                                  }
                                                  else
                                                  {
                                                       echo "fail tt";
                                                  }
                                             }
                                        }

                                        // sendEmailForVerification($register_uid);
                                        $_SESSION['messageType'] = 1;
                                        header('Location: ../addReferee.php?type=1');
                                   }
                              }
                              else
                              {
                                   header('Location: ../addReferee.php?promptError=1');
                              }
                         }
                         else
                         {
                              $_SESSION['messageType'] = 1;
                              header('Location: ../addReferee.php?type=5');
                         }
                    }
                    else
                    { }
               }
               else
               {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../addReferee.php?type=5');
               }
          }
          else
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../addReferee.php?type=5');
          }

}
else
{
     header('Location: ../addReferee.php');
}

// function BonusFlow($conn,$newUsername,$receivedUsername,$receivedUid,$amount)
// {
//      if(insertDynamicData($conn,"boss_bonus",array("new_username","receiver_username","receiver_uid","amount"),
//      array($newUsername,$receivedUsername,$receivedUid,$amount),"ssss") === null)
//      {
//           return false;
//      }
//      else
//      {}
//      return true;
// }

function BonusFlow($conn,$newUsername,$receivedUsername,$receivedUid,$amount,$bonusType)
{
     if(insertDynamicData($conn,"boss_bonus",array("new_username","receiver_username","receiver_uid","amount","bonus_type"),
     array($newUsername,$receivedUsername,$receivedUid,$amount,$bonusType),"sssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

?>