<?php
if (session_id() == ""){
     session_start();
 }
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';
require_once dirname(__FILE__) . '/allNoticeModals.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid  = $_POST['checkThat'];
     $verify_Code  = $_POST['verify_Code'];
     $verify_Pass  = $_POST['verify_Pass'];
     $verify_Reenter  = $_POST['verify_Reenter'];

     $user = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");

     $dbPass =  $user[0]->getPassword();
     $dbSalt =  $user[0]->getSalt();

     $finalPassword1 = hash('sha256', $dbSalt.$verify_Code);

     if($finalPassword1 == $dbPass)
     {
          if(strlen($verify_Pass) >= 6 
          || strlen($verify_Reenter) >= 6 )
          {
               if($verify_Pass == $verify_Reenter)
               {
                    $password = hash('sha256',$verify_Pass);
                    $salt = substr(sha1(mt_rand()), 0, 100);
                    $finalPassword = hash('sha256', $salt.$password);

                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("password","salt"),array($finalPassword,$salt,$user[0]->getUid()),"sss");
                    if($passwordUpdated)
                    {
                         $_SESSION['messageType'] = 1;
                         header( "Location: ../index.php?type=9" );
                         //echo "//Successfullly changed password ";
                    }
                    else 
                    {
                         $_SESSION['messageType'] = 1;
                         header('Location: ../resetPassword.php?uid='.$user[0]->getUid().'&type=4');
                         //echo "//Server problem ";
                    }
               }
               else 
               {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../resetPassword.php?uid='.$user[0]->getUid().'&type=3');
                    //echo "//password does not match ";
               }
          }
          else 
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../resetPassword.php?uid='.$user[0]->getUid().'&type=2');
               //echo "//password must be more than 5 ";
          }
     }
     else 
     {
          $_SESSION['messageType'] = 1;
          header('Location: ../resetPassword.php?uid='.$user[0]->getUid().'&type=1');
          //echo "// wrong code verification ";
     }
}
else 
{
     header('Location: ../index.php');
}
?>