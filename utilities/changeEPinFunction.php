<?php
if (session_id() == "")
{
     session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';
require_once dirname(__FILE__) . '/allNoticeModals.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid  = $_SESSION['uid'];

    //  $editPassword_current  = $_POST['editPassword_current'];
    //  $editPassword_new  = $_POST['editPassword_new'];
    //  $editPassword_reenter  = $_POST['editPassword_reenter'];

     $editEPin_current  = $_POST['editEPin_current'];
     $editEPin_new  = $_POST['editEPin_new'];
     $editEPin_reenter  = $_POST['editEPin_reenter'];

     $user = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
     $userDetails = $user[0];
     // echo $userDetails->getUsername();

     $dbPass =  $userDetails->getEpin();
     $dbSalt =  $userDetails->getSaltEpin();

    //  $dbPass =  $userDetails->getPassword();
    //  $dbSalt =  $userDetails->getSalt();

     $editEPin_current_hashed = hash('sha256',$editEPin_current);
     $editEPin_current_hashed_salted = hash('sha256', $dbSalt . $editEPin_current_hashed);

     // echo "<br>";
     // echo $editEPin_current."<br>";
     // echo $editEPin_new."<br>";
     // echo $editEPin_reenter."<br>";
     // echo $dbPass."<br>";
     // echo $dbSalt."<br>";
     // echo $editEPin_current_hashed."<br>";
     // echo $editEPin_current_hashed_salted."<br>";

    //  $editPassword_current_hashed = hash('sha256',$editPassword_current);
    //  $editPassword_current_hashed_salted = hash('sha256', $dbSalt . $editPassword_current_hashed);

     // echo $dbPass."<br>";
     // echo $finalPassword."<br>";

     if($editEPin_current_hashed_salted == $dbPass)
     {
          if(strlen($editEPin_new) >= 6 && strlen($editEPin_reenter) >= 6 )
          {
               if($editEPin_new == $editEPin_reenter)
               {
                    $epin = hash('sha256',$editEPin_new);
                    $salt1 = substr(sha1(mt_rand()), 0, 100);
                    $finalePin = hash('sha256', $salt1.$epin);

                    $epinUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("epin","salt_epin"),array($finalePin,$salt1,$uid),"sss");
                    if($epinUpdated)
                    {
                         $_SESSION['messageType'] = 2;
                         header( "Location: ../profile.php?type=1" );
                         // echo "//Update Password success ";
                    }
                    else 
                    {
                         $_SESSION['messageType'] = 2;
                         header( "Location: ../editePin.php?type=2" );
                         // echo "//server problem ";
                    }
               }
               else 
               {
                    $_SESSION['messageType'] = 2;
                    header( "Location: ../editePin.php?type=3" );
                    // echo "//password must be same with reenter ";
               }
          }
          else 
          {
               $_SESSION['messageType'] = 2;
               header( "Location: ../editePin.php?type=4" );
               // echo "//password length must be more than 6 ";
          }
     }
     else 
     {
          $_SESSION['messageType'] = 2;
          header( "Location: ../editePin.php?type=5" );
          // echo "//Current Password is not the same as previous ";
     }    
}
?>