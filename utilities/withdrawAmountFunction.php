<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Rate.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Withdrawal.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();
    $uid = $_SESSION['uid'];

    $bankName = $_POST['bank_name'];
    $accNumber = $_POST['acc_number'];
    $usernameAccount = $_POST['usernameaccount'];

    // $withdrawRate = getRate($conn," WHERE id = ? ",array("id"),array(1),"i");
    // $rateDetails = $withdrawRate[0];

    $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
    $userDetails = $userRows[0];
    $contact = $userDetails->getPhoneNo();
    $username = $userDetails->getUsername();

    $withdrawalStatus = "PENDING";
    $withdraw = $_POST["withdraw_amount"]; //money user entered
    // $withdrawAmount = $userDetails -> getWithdrawAmount();
    $withdrawAmount = $userDetails -> getCash();

    //  withdrawal with charges
    $withdrawRate = getRate($conn," WHERE id = ? ",array("id"),array(1),"i");
    $rateDetails = $withdrawRate[0];
    $charges = $rateDetails -> getChargesWithdraw();
    $withdrawafterCharge = $withdraw - $charges;

    // withdrawal withont charges
    // $withdrawafterCharge = $withdraw;

    $currentEnterEpin = $_POST["withdraw_epin"];
    $dbEpin =  $userDetails->getEpin();
    $dbSaltEpin =  $userDetails->getSaltEpin();
    $newEpin_hashed = hash('sha256',$currentEnterEpin);
    $newEpin_hashed_salt = hash('sha256', $dbSaltEpin .   $newEpin_hashed);

    $currentEnterEpinConvert = $_POST["withdraw_epin_convert"];
    $newEpin_hashed_convert = hash('sha256',$currentEnterEpinConvert);
    $newEpin_hashed_salt_convert = hash('sha256', $dbSaltEpin .   $newEpin_hashed_convert);

    // $charges = $rateDetails -> getChargesWithdraw();

    if ($withdraw <= $withdrawAmount && $withdraw > 10)
    {
        if ($dbEpin == $newEpin_hashed_salt)
        {
            // $currentAmount = $userDetails->getWithdrawAmount();//getusermoney
            $currentAmount = $userDetails->getCash();//getusermoney
            $totalMoney = $currentAmount - $withdraw;

            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";

            if($totalMoney)
            {
                // array_push($tableName,"final_amount");
                // array_push($tableValue,$totalMoney);
                // $stringType .=  "i";
                array_push($tableName,"cash");
                array_push($tableValue,$totalMoney);
                $stringType .=  "s";
            }
            if(!$totalMoney)
            {   
                $money = 0;
                // array_push($tableName,"final_amount");
                // array_push($tableValue,$money);
                // $stringType .=  "i";
                array_push($tableName,"cash");
                array_push($tableValue,$money);
                $stringType .=  "s";
            }

            array_push($tableValue,$uid);
            $stringType .=  "s";
            $withdrawUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
            if($withdrawUpdated)
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../adminWithdrawal.php?type=1');
            }
            else
            {
                echo "fail";
            }

            if (NewWithdraw($conn,$uid,$withdrawalStatus,$withdrawafterCharge,$withdrawAmount,$usernameAccount,$bankName,$accNumber,$contact,$username))
            {
                $_SESSION['messageType'] = 4;
                header('Location: ../profile.php?type=1');
            }
            else
            {
                $_SESSION['messageType'] = 5;
                header('Location: ../profile.php?type=1');
            }
        }
        else
        {
            $_SESSION['messageType'] = 4;
            header('Location: ../profile.php?type=3');
        }
    } 
    else
    {
        // echo "fail";
        $_SESSION['messageType'] = 4;
        header('Location: ../profile.php?type=2');
    }
}
else
{
    header('Location: ../index.php');
}

function NewWithdraw($conn,$uid,$withdrawalStatus,$withdraw,$withdrawAmount,$usernameAccount,$bankName,$accNumber,$contact,$username){

     if(insertDynamicData($conn,"withdrawal",array("uid","withdrawal_status","amount","final_amount","username","bank_name","acc_number","contact","owner"),
        //  array($uid,$withdrawalStatus,$withdraw,$withdrawAmount,$usernameAccount,$bankName,$accNumber,$contact,$username),"sssissiis") === null){
            array($uid,$withdrawalStatus,$withdraw,$withdrawAmount,$usernameAccount,$bankName,$accNumber,$contact,$username),"sssssssss") === null){

          return false;
     }else{
     }

    return true;
 }

?>