<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php'; 
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$conn->close();

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://bossinternational.asia/adminEditPassword.php" />
<meta property="og:title" content="Edit Password | Boss" />
<title>Edit Password | Boss</title>
<meta property="og:description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
<meta name="description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
<meta name="keywords" content="Impotence, Boss, man, men sex, sexual, cure, product, unable to ejaculate, Penile Erectile Dysfunction, Sexual Desire Disorder, Sexual Intercourse Disorder, low sexual desire,阳痿,性冷淡,性功能障碍,不举,  etc">
<link rel="canonical" href="https://bossinternational.asia/adminEditPassword.php" />
<?php include 'css.php'; ?>
<?php require_once dirname(__FILE__) . '/header.php'; ?>
</head>

<body class="body">

<!-- Start Menu -->
<?php include 'header-sherry.php'; ?>
<div class="yellow-body padding-from-menu same-padding">
	<h1 class="success-h1 text-center">
    	Edit Password
    </h1>
    <div class="reset-password-div">
        <form class="login-form" method="POST" action="utilities/adminChangePasswordFunction.php">
            <div class="input-grey-div">
                <span class="input-span"><img src="img/lock.png" class="login-input-icon" alt="Current Password" title="Current Password"></span>
                <input name="adminEditPassword_current" id="adminEditPassword_current" required class="login-input password-input clean" type="password" placeholder="Current Password">
                    <span class="visible-span"><img src="img/visible.png" class="login-input-icon" alt="View Password" title="View Password" id="editPassword_current_img"></span>
            </div>
            
            <div class="input-grey-div">
                <span class="input-span"><img src="img/lock.png" class="login-input-icon" alt="New Password" title="New Password"></span>
                <input name="adminEditPassword_new" id="adminEditPassword_new" required class="login-input password-input clean" type="password" placeholder="New Password">
                 <span class="visible-span"><img src="img/visible.png" class="login-input-icon" alt="View Password" title="View Password" id="editPassword_new_img"></span>
            </div>        
            <div class="input-grey-div">
                <span class="input-span"><img src="img/lock.png" class="login-input-icon" alt="Retype New Password" title="Retype New Password"></span>
                <input name="adminEditPassword_reenter" id="adminEditPassword_reenter" required class="login-input password-input clean" type="password" placeholder="Retype New Password">
                <span class="visible-span"><img src="img/visible.png" class="login-input-icon" alt="View Password" title="View Password" id="editPassword_reenter_img"></span>
            </div>
            <div class="clear"></div>
                <div class="three-btn-container">
                    <!-- <button class="refund-btn-a white-button three-btn-a clean admin-edit-pw"><b>EDIT PASSWORD</b></button> -->
                    <button class="shipout-btn-a black-button three-btn-a clean"><b>CONFIRM</b></button>
                </div>   
        </form>

     </div>


</div>
<?php include 'js.php'; ?>

<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Your current password does not match! <br>Please try again.";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "Your new password must be more than 5 characters";
        }
        if($_GET['type'] == 3)
        {
            $messageType = "Password does not match with retype password. <br>Please try again.";
        }
        if($_GET['type'] == 4)
        {
            $messageType = "Server problem. <br>Please try again later.";
        }
        if($_GET['type'] == 5)
        {
            $messageType = "Password successfully updated!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

<script>
  viewPassword( document.getElementById('editPassword_current_img'), document.getElementById('adminEditPassword_current'));
  viewPassword( document.getElementById('editPassword_new_img'), document.getElementById('adminEditPassword_new'));
  viewPassword( document.getElementById('editPassword_reenter_img'), document.getElementById('adminEditPassword_reenter'));
</script>
<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Wrong Code Verification. <br>Please Try Again.";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Password must be more than 5. <br>Please Try Again";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Password Does Not Match. <br>Please Try Again";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "Server Failure ! <br>Please Try Again Later In A Few Minutes.";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>
</body>
</html>