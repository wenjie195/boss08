<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Images.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Withdrawal.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';


$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST'){




}


// $products = getProduct($conn);
//
// $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
// $userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://bossinternational.asia/editProduct.php" />
    <meta property="og:title" content="Product Edit| Boss" />
    <title>Product Edit | Boss</title>
    <meta property="og:description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="keywords" content="Impotence, Boss, man, men sex, sexual, cure, product, unable to ejaculate, Penile Erectile Dysfunction, Sexual Desire Disorder, Sexual Intercourse Disorder, low sexual desire,阳痿,性冷淡,性功能障碍,不举,  etc">
    <link rel="canonical" href="https://bossinternational.asia/restoreProduct.php" />
    <?php include 'css.php'; ?>   
</head>
<body class="body">

<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">
  <form method="POST" action="utilities/editProductFunction.php">

	<h1 class="details-h1" onclick="goBack()">
    	<a class="black-white-link2 hover1">
    		<img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">
            <img src="img/back2.png" class="back-btn2 hover1b" alt="back" title="back">
        	Product Name : <?php echo $_POST['order_id']; ?>
        </a>
    </h1>


    <table class="details-table">
        <?php
            if(isset($_POST['order_id']))
            {
                $conn = connDB();
                $productArray = getProduct($conn,"WHERE name = ? ", array("name") ,array($_POST['order_id']),"s");
            ?>

            <div class="shipping-input clean smaller-text2 fifty-input ow-mbtm">

                <input class="shipping-input2 clean normal-input same-height-with-date" type="hidden" placeholder="Product Id" id="id" name="id" value="<?php echo $productArray[0]->getId() ?>">
                </div>
                <div class="shipping-input clean smaller-text2 fifty-input ow-mbtm">
                <p>Name</p>
                <input class="shipping-input2 clean normal-input same-height-with-date" type="text" placeholder="Product Name" id="product_name" name="product_name" value="<?php echo $productArray[0]->getName() ?>">
                </div>
                <div class="shipping-input clean smaller-text2 fifty-input ow-mbtm">
                <p>Price</p>
                <input oninput="this.value = this.value.toUpperCase()" class="shipping-input2 clean normal-input same-height-with-date" type="number" placeholder="Product Price" id="product_price" name="product_price" value="<?php echo $productArray[0]->getPrice() ?>">
                </div>
                <div class="shipping-input clean smaller-text2 fifty-input ow-mbtm">
                <p>Stock</p>
                <input oninput="this.value = this.value.toUpperCase()" class="shipping-input2 clean normal-input same-height-with-date" type="number" placeholder="Product Stock" id="product_stock" name="product_stock" value="<?php echo $productArray[0]->getStock() ?>">
                </div>
                <div class="shipping-input clean smaller-text2 fifty-input ow-mbtm">
                <p>Product Description</p>
                <input oninput="this.value = this.value.toUpperCase()" class="shipping-input2 clean normal-input same-height-with-date" type="text" placeholder="Product Description" id="product_description" name="product_description" value="<?php echo $productArray[0]->getDescription() ?>">
                </div>

                <div class="shipping-input clean smaller-text2 fifty-input ow-mbtm">
                    <p>Product Image</p>
                        <td><?php

                        $id = $productArray[0]->getId();

                        // Get images from the database
                        $query = $conn->query("SELECT images FROM product WHERE id = '$id'");

                        if($query->num_rows > 0)
                        {
                        while($row = $query->fetch_assoc())
                            {
                            $imageURL = 'ProductImages/'.$row["images"];?>
                                <a class="img"><img src="<?php echo $imageURL; ?>" class="details-img receipt-img"></a>
                            <?php 
                            }
                        }
                        elseif($query->num_rows=null)
                        { ?>
                            <p>No image(s) found...</p>
                        <?php 
                        } ?>
                        </td>
                </div>

            </div>
            
            <?php
            }
        ?>
    </table>

    <div class="three-btn-container">
        <button class="shipout-btn-a black-button three-btn-a" type="submit" id = "restoreSubmit" name = "restoreSubmit" ><b>RESTORE</b></a></button>
    </div>

</form>
</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>
<script>
$(function () {
    $('.link-to-details').click(function () {
        window.location.href = $(this).data('url');
    });
})

</script>
<script>
function goBack() {
  window.history.back();
}
</script>
</body>
</html>
