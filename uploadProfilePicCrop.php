<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/classes/Images.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $picture = rewrite($_POST["update_picture"]);
}

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$userPic = getImages($conn," WHERE pid = ? ",array("pid"),array($userDetails->getPicture()),"s");
$userProPic = $userPic[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

$filename = $_GET['file'];

?>

<style>
.confirm-btntj 
{
    font-size: 15px;
    padding: 8px 30px;
    border-radius: 5px;
    margin-top: 30px;
}
.dim-body{
	background-color:grey;
	background-image:url(../img/dim-background.jpg);
	background-size:cover;
	width:100%;
	min-height:calc(100vh - 40px);
	overflow:hidden;
	padding-bottom: 50px;
}
.abc
{
    color:white;
}
</style>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://bossinternational.asia/uploadProfilePicCrop.php" />
    <meta property="og:title" content="Upload Profile Picture | Boss" />
    <title>Upload Profile Picture | Boss</title>
    <meta property="og:description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="keywords" content="Impotence, Boss, man, men sex, sexual, cure, product, unable to ejaculate, Penile Erectile Dysfunction, Sexual Desire Disorder, Sexual Intercourse Disorder, low sexual desire,阳痿,性冷淡,性功能障碍,不举,  etc">
    <link rel="canonical" href="https://bossinternational.asia/uploadProfilePicCrop.php" />
    
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.Jcrop.js"></script>
    <link rel="stylesheet" href="css/jquery.Jcrop.css" type="text/css" />

    <?php include 'css.php'; ?>    
</head>
<body class="body">
<?php include 'header-sherry.php'; ?>


<div class="dim-body padding-from-menu same-padding crop-div">
            <div class="right-profile-div paddingleft0">
                <h3 class="abc">Please Crop a Region of the Picture</h3>
                <h5 class="abc">"use <strong class="abc">+ </strong> to crop the picture"</h5>
                    <form method="POST"  action="upload.php?file=<?php echo $filename; ?>" enctype="multipart/form-data" onsubmit="return checkCoords();">
                    <input type = "hidden" name="uid" value="<?php echo $userDetails->getUid();?>" >
                    <input type = "hidden" name="username" value="<?php echo $userDetails->getUsername();?>" >
                    <img src="upload/<?php echo $filename; ?>" id="cropbox" class="uploaded-img">
                            <input type="hidden" id="x" name="x" />
                            <input type="hidden" id="y" name="y" />
                            <input type="hidden" id="w" name="w" />
                            <input type="hidden" id="h" name="h" />
                    <!-- </form>         -->
                    <input type="submit" name="submit" value="Upload" class="confirm-btntj text-center white-text clean black-button">        
                    </form>
                
            </div>
</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<div class="footer-div width100 same-padding overflow">
	<p class="yellow-text footer-p">© 2019 DCK Supreme Sdn. Bhd., All Rights Reserved.</p>
</div>

<script type="text/javascript">
  $(function(){
    $('#cropbox').Jcrop({
      aspectRatio: 1,
      onSelect: updateCoords
    });
  });

  function updateCoords(c){
    $('#x').val(c.x);
    $('#y').val(c.y);
    $('#w').val(c.w);
    $('#h').val(c.h);
  };

  function checkCoords(){
    if (parseInt($('#w').val())) return true;
    alert('Please select a crop region then press submit.');
    return false;
  };

</script>

</body>
</html>