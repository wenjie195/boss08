<div class="footer-div width100 same-padding overflow">
	<p class="yellow-text footer-p"><?php echo _MAINJS_JS_FOOTER ?></p>
</div>

<!-- Menu Admin Modal -->
<div id="menu-admin-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css menu-modal-content menu-admin-modal-content">
    <span class="close-css close-menu-admin">&times;</span>
    <h1 class="menu-h1">MENU</h1>
      <div class="menu-link-container"><a href="adminDashboard.php" class="mobile-menu close-menu">Dashboard</a></div>
      <div class="menu-link-container"><a href="adminPayment.php" class="mobile-menu close-menu">Payment</a></div>
      <div class="menu-link-container"><a href="adminShipping.php" class="mobile-menu close-menu">Shipping</a></div>
      <div class="menu-link-container"><a href="adminWithdrawal.php" class="mobile-menu close-menu">Withdrawal</a></div>
      <div class="menu-link-container"><a href="adminSales.php" class="mobile-menu">Sales</a></div>
      <div class="menu-link-container"><a href="adminProduct.php" class="mobile-menu close-menu">Product</a></div>
      <div class="menu-link-container"><a href="adminPayout.php" class="mobile-menu close-menu">Payout</a></div>
      <div class="menu-link-container"><a href="adminMember.php" class="mobile-menu close-menu">Member</a></div>
      <div class="menu-link-container"><a href="adminAdmin.php" class="mobile-menu close-menu">Admin</a></div>
      <div class="menu-link-container"><a href="adminProfile.php" class="mobile-menu">Edit Profile</a></div>  
      <div class="menu-link-container"><a href="adminRate.php" class="mobile-menu">Rate</a></div> 
      <div class="menu-link-container"><a href="announcement.php" class="mobile-menu">Announcement</a></div> 
      <div class="menu-link-container"><a href="adminReason.php" class="mobile-menu">Reason</a></div> 
      <div class="menu-link-container"><a href="logout.php" class="mobile-menu">Logout</a></div>
  </div>

</div>

<script src="js/jquery-3.3.1.min.js" type="text/javascript"></script> 
<script src="js/bootstrap.min.js" type="text/javascript"></script>    
<script src="js/headroom.js" type="text/javascript"></script>
<script src="js/jquery.fancybox.js" type="text/javascript"></script>
<script type="text/javascript" src="js/main.js?version=1.0.1"></script>

<script>
    (function() {
        var header = new Headroom(document.querySelector("#header"), {
            tolerance: 5,
            offset : 205,
            classes: {
              initial: "animated",
              pinned: "slideDown",
              unpinned: "slideUp"
            }
        });
        header.init();
    
    }());
</script>
 
	<script>
    // Cache selectors
    var lastId,
        topMenu = $("#top-menu"),
        topMenuHeight = topMenu.outerHeight(),
        // All list items
        menuItems = topMenu.find("a"),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function(){
          var item = $($(this).attr("href"));
          if (item.length) { return item; }
        });
    
    // Bind click handler to menu items
    // so we can get a fancy scroll animation
    menuItems.click(function(e){
      var href = $(this).attr("href"),
          offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
      $('html, body').stop().animate({ 
          scrollTop: offsetTop
      }, 500);
      e.preventDefault();
    });
    
    // Bind to scroll
    $(window).scroll(function(){
       // Get container scroll position
       var fromTop = $(this).scrollTop()+topMenuHeight;
       
       // Get id of current scroll item
       var cur = scrollItems.map(function(){
         if ($(this).offset().top < fromTop)
           return this;
       });
       // Get the id of the current element
       cur = cur[cur.length-1];
       var id = cur && cur.length ? cur[0].id : "";
                      
    });
    </script>
    
<script>
$(document).ready(function(){
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});
</script>


<!-- JS Create Account -->
<script>
  function checkIfVariableIsNullOrEmptyString(field,isValidate)
  {
    if(field == null || field == "" || field.length == 0)
    {
      isValidate += 1;
      return isValidate;
    }
    else
    {
      return isValidate;
    }
  }
  function createAccountValidation()
  {
    // First Level Validation
    let isValidatedAndCanProceedToNextLevel = 0;

    let register_username = $('#register_username').val();
    let register_email_user = $('#register_email_user').val();
    let register_password = $('#register_password').val();
    let register_retype_password = $('#register_retype_password').val();
    let register_email_referrer = $('#register_email_referrer').val();
    

    // console.log('Initial Counter = '+isValidatedAndCanProceedToNextLevel);

    isValidatedAndCanProceedToNextLevel = checkIfVariableIsNullOrEmptyString(register_username,isValidatedAndCanProceedToNextLevel);
    // console.log('counter = '+isValidatedAndCanProceedToNextLevel);
    
    isValidatedAndCanProceedToNextLevel = checkIfVariableIsNullOrEmptyString(register_email_user,isValidatedAndCanProceedToNextLevel);
    // console.log('counter = '+isValidatedAndCanProceedToNextLevel);
  
    isValidatedAndCanProceedToNextLevel = checkIfVariableIsNullOrEmptyString(register_password,isValidatedAndCanProceedToNextLevel);
    // console.log('counter = '+isValidatedAndCanProceedToNextLevel);
    
    isValidatedAndCanProceedToNextLevel = checkIfVariableIsNullOrEmptyString(register_retype_password,isValidatedAndCanProceedToNextLevel);
    // console.log('counter = '+isValidatedAndCanProceedToNextLevel);

    // Second Level Validation
    if(isValidatedAndCanProceedToNextLevel == 0)
    {
      // alert('can continue');
      if(register_password.length >= 6)
      {
        if(register_password == register_retype_password)
        {
          if(register_email_referrer == null || register_email_referrer == "" || register_email_referrer.length == 0)
          {

          }
          else
          {
            if(emailIsValid (register_email_referrer))
            {
              
            }
            else
            {
              alert('Referrer`s email is not valid ! Please try again ! ');
              event.preventDefault();
            }
          }
        }
        else
        {
          alert('Password does not match ! Please try again ! ');
          event.preventDefault();
        }
      }
      else
      {
        alert('Password must be more than 5 ! Please try again ! ');
        event.preventDefault();
      }
    }
    else
    {
      alert('Please enter all fields required ! ');
      event.preventDefault();
    }
  }
  function emailIsValid (email) 
  {
    return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)
  }
</script>
<script>
  viewPassword( document.getElementById('login_view_password'), document.getElementById('login_password'));
  viewPassword( document.getElementById('register_password_img'), document.getElementById('register_password'));
  viewPassword( document.getElementById('register_retype_password_img'), document.getElementById('register_retype_password'));
</script>


<!--- Modal Box --->
<script>
var menuadminmodal = document.getElementById("menu-admin-modal");
var openmenuadmin = document.getElementsByClassName("open-menu-admin")[0];
var closemenuadmin = document.getElementsByClassName("close-menu-admin")[0];

if(openmenuadmin){
openmenuadmin.onclick = function() {
  menuadminmodal.style.display = "block";
}
}
closemenuadmin.onclick = function() {
  menuadminmodal.style.display = "none";
}
window.onclick = function(event) {
  if (event.target == menuadminmodal) {
    menuadminmodal.style.display = "none";
  }
}
</script>
<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.dlmenu.js"></script>
		<script>
			$(function() {
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
				});
			});
		</script>