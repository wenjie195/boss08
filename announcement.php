<?php
if (session_id() == ""){
    session_start();
}

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    //todo validation on server side
    //TODO change login with email to use username instead
    //TODO add username field to register's backend
    $conn = connDB();

    if(isset($_POST['loginButton'])){
        $email = rewrite($_POST['email']);
        $password = $_POST['password'];

        $userRows = getUser($conn," WHERE username = ? ",array("username"),array($email),"s");
        if($userRows)
        {
            $user = $userRows[0];

            if($user->getisEmailVerified() == 1)
            {
                $tempPass = hash('sha256',$password);
                $finalPassword = hash('sha256', $user->getSalt() . $tempPass);
    
                if($finalPassword == $user->getPassword()) 
                {
                    if(isset($_POST['remember-me'])) 
                    {
                        
                        setcookie('email-oilxag', $email, time() + (86400 * 30), "/");
                        setcookie('password-oilxag', $password, time() + (86400 * 30), "/");
                        setcookie('remember-oilxag', 1, time() + (86400 * 30), "/");
                        // echo 'remember me';
                    }
                    else 
                    {
                        setcookie('email-oilxag', '', time() + (86400 * 30), "/");
                        setcookie('password-oilxag', '', time() + (86400 * 30), "/");
                        setcookie('remember-oilxag', 0, time() + (86400 * 30), "/");
                        // echo 'null';
                    }

                    $_SESSION['uid'] = $user->getUid();
                    echo '<script>window.location.replace("profile.php");</script>';
                }
                else 
                {
                    promptError("Incorrect email or password");
                }
            }
            else 
            {
                promptError("Please confirm your registration inside your email");
            }

        }
        else
        {
            promptError("This account does not exist");
        }
    }

    $conn->close();
}

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://bossinternational.asia/announcement.php" />
<meta property="og:title" content="Announcement | Boss" />
<title>Announcement | Boss</title>
<meta property="og:description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
<meta name="description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
<meta name="keywords" content="Impotence, Boss, man, men sex, sexual, cure, product, unable to ejaculate, Penile Erectile Dysfunction, Sexual Desire Disorder, Sexual Intercourse Disorder, low sexual desire,阳痿,性冷淡,性功能障碍,不举,  etc">
<link rel="canonical" href="https://bossinternational.asia/announcement.php" />
<?php include 'css.php'; ?>
<?php require_once dirname(__FILE__) . '/header.php'; ?>
</head>

<body class="body">
<!--
<div id="overlay">
 <div class="center-food"><img src="img/loading-gif.gif" class="food-gif"></div>
 <div id="progstat"></div>
 <div id="progress"></div>
</div>-->

<!-- Start Menu -->

<div class="yellow-body padding-from-menu same-padding">
	<?php include 'header-sherry.php'; ?>
    <h1 class="cart-h1 m-btm-0 announcement-h1 gold-text"><img src="img/announcement.png" alt="Announcement" title="Announcement" class="announcement-icon"> Announcement</h1>
    <div class="border-announcement">
    	<!-- <p class="announcement-p">Thanks for joining us.</p> -->
    </div>
</div>


<?php include 'js.php'; ?>
<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully register your profile! Please confirm your registration inside of your email.";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "There are no referrer with this email ! Please register again";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Successfully register your profile! Please confirm your registration inside of your email.";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "User password must be more than 5 !";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "User password does not match";
        }
        else if($_GET['type'] == 6)
        {
            $messageType = "Wrong email format.";
        }
        else if($_GET['type'] == 7)
        {
            $messageType = "There are no user with this email ! Please try again.";
        }
        else if($_GET['type'] == 8)
        {
            $messageType = "Successfully reset your password! Please check your email.";
        }
        else if($_GET['type'] == 9)
        {
            $messageType = "Successfully reset your password! ";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
if(isset($_GET['promptError']))
{
    $messageType = null;

    if($_GET['promptError'] == 1)
    {
        $messageType = "Error registering new account.The account already exist";
    }
    else if($_GET['promptError'] == 2)
    {
        $messageType = "Error assigning referral relationship. Please register again.";
    }
    echo '
    <script>
        putNoticeJavascript("Notice !! ","'.$messageType.'");
    </script>
    ';   
}
?>
</body>
</html>