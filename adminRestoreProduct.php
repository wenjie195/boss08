<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$conn = connDB();

$productsOrders =  getProductOrders($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://bossinternational.asia/adminRestoreProduct.php" />
    <meta property="og:title" content="Restore Product | Boss" />
    <title>Restore Product | Boss</title>
    <meta property="og:description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="keywords" content="Impotence, Boss, man, men sex, sexual, cure, product, unable to ejaculate, Penile Erectile Dysfunction, Sexual Desire Disorder, Sexual Intercourse Disorder, low sexual desire,阳痿,性冷淡,性功能障碍,不举,  etc">
    <link rel="canonical" href="https://bossinternational.asia/adminRestoreProduct.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">
	<h1 class="h1-title h1-before-border shipping-h1">Restore Product</h1>
    <!-- This is a filter for the table result -->

    <!-- <select class="filter-select clean">
    	<option class="filter-option">Latest Shipping</option>
        <option class="filter-option">Oldest Shipping</option>
    </select> -->

    <!-- End of Filter -->
    <div class="clear"></div>

    <div class="width100 shipping-div2">
        <?php $conn = connDB();?>
            <table class="shipping-table">
                <thead>
                    <tr>
                        <th>NO.</th>
                        <th>PRODUCT NAME</th>
                        <th>PRICE (RM)</th>
                        <!-- <th>STOCK</th> -->
                        <th>PRODUCT DESCRIPTION</th>
                        <th>EDIT</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    // for($cnt = 0;$cnt < count($productsOrders) ;$cnt++)
                    // {
                        $orderDetails = getProduct($conn, "WHERE display = '0'", array("display"),"i");
                        if($orderDetails != null)
                        {
                            for($cntAA = 0;$cntAA < count($orderDetails) ;$cntAA++)
                            {?>
                            <tr>
                                <!-- <td><?php //echo ($cntAA+1)?></td> -->
                                <td><?php echo $orderDetails[$cntAA]->getId();?></td>
                                <td><?php echo $orderDetails[$cntAA]->getName();?></td>
                                <td><?php echo $orderDetails[$cntAA]->getPrice();?></td>
                                <!-- <td><?php //echo $orderDetails[$cntAA]->getStock();?></td> -->
                                <td><?php echo $orderDetails[$cntAA]->getDescription();?></td>
                                <td>
                                    <form action="restoreProduct.php" method="POST">
                                        <button class="clean edit-anc-btn hover1" type="submit" name="order_id" value="<?php echo $orderDetails[$cntAA]->getName();?>">
                                            <img src="img/edit.png" class="edit-announcement-img hover1a" alt="UpdateShipping" title="reviewshippingRefund">
                                            <img src="img/edit2.png" class="edit-announcement-img hover1b" alt="UpdateShipping" title="reviewshippingRefund">
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            <?php
                            }
                        }
                         ?>

                </tbody>
            </table>
          <?php    if (!$orderDetails) { ?>
              <br><center>  <div class= "width100 oveflow">
                  <div class="width20">
                      <div class="white50div">
                  <?php echo "*There is No Product Available For Restore." ?>
                </div>
            </div>
          </div></center>
        <?php } ?>
        <?php $conn->close();?>
    </div>



</div>




<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Server currently fail. Please try again later.";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Successfully Delete Product.";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Error Deleting Product";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>
<script>
$(function () {
    $('.link-to-details').click(function () {
        window.location.href = $(this).data('url');
    });
})

</script>
</body>
</html>
