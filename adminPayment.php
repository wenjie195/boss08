<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php'; 
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';

//Product Order
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$conn = connDB();

// $productsOrders =  getProductOrders($conn," WHERE quantity > 2 ");
$productsOrders =  getProductOrders($conn);

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");

//$orderDetailsAAA =  getOrders($conn);

$orderDetailsAAA = getOrders($conn," WHERE payment_status = 'PENDING' ");

// $orderDetailsAAA = getOrders($conn," WHERE payment_status = ? ",array("payment_status"),array($payment_status),"s");

// $orderDetailsABC = getOrders($conn," WHERE payment_status = ? ",array("payment_status"),array($payment_status),"s");
// $orderDetailsAAA = $orderDetailsABC[0];

$conn->close();

function promptError($msg){
    echo ' 
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://bossinternational.asia/adminPayment.php" />
    <meta property="og:title" content="Payment Verification | Boss" />
    <title>Payment Verification | Boss</title>
    <meta property="og:description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="keywords" content="Impotence, Boss, man, men sex, sexual, cure, product, unable to ejaculate, Penile Erectile Dysfunction, Sexual Desire Disorder, Sexual Intercourse Disorder, low sexual desire,阳痿,性冷淡,性功能障碍,不举,  etc">
    <link rel="canonical" href="https://bossinternational.asia/adminPayment.php" />
    <?php include 'css.php'; ?>    
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">
    <!-- <h1 class="h1-title h1-before-border shipping-h1">Payment Verification | <a href="adminShippingComp.php" class="white-text title-tab-a">Completed</a></h1> -->
    <h1 class="h1-title h1-before-border shipping-h1">Payment Verification </h1>
  
    <div class="clear"></div>

   

    <div class="width100 shipping-div2">
        <?php $conn = connDB();?>
            <table class="shipping-table">    
                <thead>
                    <tr>
                        <th>NO.</th>
                        <th>ORDER NUMBER</th>
                        <th>USERNAME</th>
                        <th>ORDER DATE</th>
                        <th>PRICE (RM)</th>
                        <th>VERIFY</th>
                    </tr>
                </thead>
                <tbody>
                <?php

                if($orderDetailsAAA != null)
                {

                for($cnt = 0;$cnt < count($orderDetailsAAA) ;$cnt++)


                        {?>
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td>#<?php echo $orderDetailsAAA[$cnt]->getId();?></td>
                            <td><?php echo $orderDetailsAAA[$cnt]->getUsername();?></td>
                            <td><?php $dateCreated = date("Y-m-d",strtotime($orderDetailsAAA[$cnt]->getDateCreated()));
                                    echo $dateCreated;?></td>
                            <td><?php echo $orderDetailsAAA[$cnt]->getSubtotal();?></td>
                            <td>
                                <form action="paymentverification.php" method="POST">
                                    <button class="clean edit-anc-btn hover1" type="submit" name="order_id" value="<?php echo $orderDetailsAAA[$cnt]->getId();?>">
                                        <img src="img/verify-payment.png" class="edit-announcement-img hover1a" alt="Verify Payment" title="Verify Payment">
                                        <img src="img/verify-payment2.png" class="edit-announcement-img hover1b" alt="Verify Payment" title="Verify Payment">
                                    </button>
                                </form>
                            </td>
                        </tr>
                        <?php
                        }
                }
                ?>
                </tbody>
            </table>
        <?php $conn->close();?>
    </div> 


</div>


<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Shipping Details Update Successfully.";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "Fail To Update Data.";
        }
        if($_GET['type'] == 3)
        {
            $messageType = "Error";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

<script>
$(function () {
    $('.link-to-details').click(function () {
        window.location.href = $(this).data('url');
    });
})
</script>

</body>
</html>