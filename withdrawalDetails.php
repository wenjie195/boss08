<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Withdrawal.php';
// require_once dirname(__FILE__) . '/classes/Receipt.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';


$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
if(isset($_POST["withdrawal_details"])){
        $withdrawalStatus = rewrite($_POST["withdrawal_status"]);
        $withdrawalMethod = rewrite($_POST["withdrawal_method"]);
        $withdrawalAmount = rewrite($_POST["withdrawal_accepted"]);
        $withdrawalNote = rewrite($_POST["withdrawal_note"]);

        $withdrawalNumber = rewrite($_POST["withdrawal_number"]);
      }else {
        $withdrawalStatus = "";
        $withdrawalMethod = "";
        $withdrawalAmount = "";
        $withdrawalNote = "";

        $withdrawalNumber = "";
      }
    }

$products = getProduct($conn);
$withdrawArray = getWithdrawReq($conn,"WHERE withdrawal_number = ? ", array("withdrawal_number") ,array($_POST['withdrawal_number']),"i");

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://bossinternational.asia/withdrawalDetails.php" />
    <meta property="og:title" content="Withdrawal Details | Boss" />
    <title>Withdrawal Details | Boss</title>
    <meta property="og:description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="keywords" content="Impotence, Boss, man, men sex, sexual, cure, product, unable to ejaculate, Penile Erectile Dysfunction, Sexual Desire Disorder, Sexual Intercourse Disorder, low sexual desire,阳痿,性冷淡,性功能障碍,不举,  etc">
    <link rel="canonical" href="https://bossinternational.asia/withdrawalDetails.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<!--<form method="POST">-->
    <?php
//    if(isset($_POST['product-list-quantity-input'])){
//        createProductList($products,$_POST['product-list-quantity-input']);
//    }else{
//        createProductList($products);
//    }

    ?>
<!--    <button type="submit" name="addToCartButton" id="addToCartButton" >Add to cart</button>-->
<!--</form>-->
<div class="yellow-body padding-from-menu same-padding">
	<h1 class="details-h1" onclick="goBack()">
    	<a  class="black-white-link2 hover1">
    		<img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">
            <img src="img/back2.png" class="back-btn2 hover1b" alt="back" title="back">
        	<?php echo _MAINJS_ADMWD_WITHDRAWAL_NO ?>: #<?php echo $_POST['withdrawal_number'];?>
        </a>
    </h1>
    <table class="details-table">
    	<tbody>
        <?php
        if(isset($_POST['withdrawal_number']))
        {
            $conn = connDB();
            //Order
            $withdrawArray = getWithdrawReq($conn,"WHERE withdrawal_number = ? ", array("withdrawal_number") ,array($_POST['withdrawal_number']),"i");
            $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($withdrawArray[0]->getUid()),"s");
            $userDetails = $userRows[0];
            // $receiptArray = getReceipt($conn,"WHERE withdrawal_number = ? ", array("withdrawal_number") ,array($_POST['withdrawal_number']),"i");
            //OrderProduct
            // $orderProductArray = getProductOrders($conn,"WHERE order_id = ? ", array("order_id") ,array($_POST['order_id']),"i");
            //$orderDetails = $orderArray[0];

            if($withdrawArray != null && $userDetails !=null)
            {?>
              <tr>
                  <!-- <td>Name</td> -->
                  <td><?php echo _MAINJS_ADMWD_USERNAME ?></td>
                  <td>:</td>
                  <td><?php echo $withdrawArray[0]->getOwnerUsername(); ?></td>
              </tr>
              <tr>
                  <!-- <td>Contact</td> -->
                  <td><?php echo _MAINJS_ADMWD_CONTACT ?></td>
                  <td>:</td>
                  <td><?php echo $withdrawArray[0]->getContact();?></td>
              </tr>
              <tr>
                  <!-- <td>Bank</td> -->
                  <td><?php echo _MAINJS_ADMWD_BANK ?></td>
                  <td>:</td>
                  <td><?php echo $withdrawArray[0]->getBankName();?></td>
              </tr>
               <tr>
                  <!-- <td>Bank Name</td> -->
                  <td><?php echo _MAINJS_PROFILE_ACCOUNT_HOLDER_NAME ?></td>
                  <td>:</td>
                  <td><?php echo $withdrawArray[0]->getUsername();?></td>
              </tr>
              <tr>
                  <!-- <td>Bank Acc. No.</td> -->
                  <td><?php echo _MAINJS_PROFILE_ACCOUNT_NUMBER ?></td>
                  <td>:</td>
                  <td><?php echo $withdrawArray[0]->getAccNumber();?></td>
              </tr>
              <!-- <tr>
              	<td>Points</td>
                  <td>:</td>
                  <td><?php //echo $userDetails->getUserPoint();?></td>
              </tr> -->
              <tr>
                  <!-- <td>RM</td> -->
                  <td><?php echo _MAINJS_ADMWD_AMOUNT_4 ?></td>
                  <td>:</td>
                  <td><?php echo $withdrawArray[0]->getWithdrawRequestAmount();?></td>
              </tr>
              <tr>
                  <!-- <td>Requested Date</td> -->
                  <td><?php echo _MAINJS_ADMWD_REQUEST_DATE_2 ?></td>
                  <td>:</td>
                  <td><?php echo $withdrawArray[0]->getDateCreated();?></td>
                  <!-- <td><?php //$dateCreated = date("Y-m-d",strtotime($withdrawArray[0]->getDateCreated()));
                        //  echo $dateCreated;?></td> -->
              </tr>
              <tr>
                <!-- <td>Issue Date</td> -->
                <td><?php echo _MAINJS_ADMWD_ISSUE_DATE ?></td>
                  <td>:</td>
                  <td><?php echo $withdrawArray[0]->getDateUpdated();?></td>
                  <!-- <td><?php //$dateUpdated = date("Y-m-d",strtotime($withdrawArray[0]->getDateUpdated()));
                        //  echo $dateUpdated;?></td> -->
              </tr>
              <tr>
                  <!-- <td>Status</td> -->
                  <td><?php echo _MAINJS_ADMWD_STATUS_2 ?></td>
                  <td>:</td>
                  <td><?php echo $withdrawArray[0]->getWithdrawalRequestStatus();?></td>
              </tr>
              <tr>
                  <!-- <td>Bank In Reference</td> -->
                  <td><?php echo _MAINJS_ADMWD_REFERENCES ?></td>
                  <td>:</td>
                  <td><?php echo $withdrawArray[0]->getWithdrawalNote();?></td>
              </tr>
              <tr>
                  <!-- <td>Receipt</td> -->
                  <td><?php echo _MAINJS_ADMWD_RECEIPT ?></td>
                  <td>:</td>
                  <!-- <td><a class="img" href="<?php //echo $withdrawArray[0]->getWithdrawalReceipt();?>"  data-fancybox="images-preview1"  class="image-popout"><img src="<?php //echo $withdrawArray[0]->getWithdrawalReceipt();?>" class="details-img receipt-img"></a></td> -->
                  <td><?php

                $withdrawalNumberNew  = $withdrawArray[0]->getWithdrawalNumber();
                // Include the database configuration file


                // Get images from the database
                $query = $conn->query("SELECT receipt FROM withdrawal WHERE withdrawal_number = '$withdrawalNumberNew'");

                if($query->num_rows > 0){
                    while($row = $query->fetch_assoc()){
                        $imageURL = 'images/'.$row["receipt"];
                        if ($row["receipt"] != null) {

                        ?>


<a class="img" href="<?php echo $imageURL;?>"  data-fancybox="images-preview1"  class="image-popout"><img src="<?php echo $imageURL; ?>" class="details-img receipt-img"></a>
    <!-- <img src="<?php// echo $imageURL; ?>" alt="" /> -->
<?php }else {
  ?><p class="b">No Receipt Uploaded By Admin.</p><?php
}}
}else{ ?>
    <p>No image(s) found...</p>
<?php } ?>
                  </td>
              </tr>
            <?php
          }
      }
      else
      {}
      //$conn->close();
      ?>

        </tbody>
    </table>


    <div class="clear"></div>


</div>


<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>
<style>
p.a {
  font-style: normal;
}

p.b {
  font-style: italic;
}

p.c {
  font-style: oblique;
}
</style>
<script>
function goBack() {
  window.history.back();
}
</script>
</body>
</html>
