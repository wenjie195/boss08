<?php
if (session_id() == ""){
    session_start();
}

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://bossinternational.asia/" />
<meta property="og:title" content="Cure of Impotence | Boss" />
<title>Cure of Impotence | Boss</title>
<meta property="og:description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
<meta name="description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
<meta name="keywords" content="Impotence, Boss, man, men sex, sexual, cure, product, unable to ejaculate, Penile Erectile Dysfunction, Sexual Desire Disorder, Sexual Intercourse Disorder, low sexual desire,阳痿,性冷淡,性功能障碍,不举,  etc">
<link rel="canonical" href="https://bossinternational.asia/" />
<?php include 'css.php'; ?>
<?php require_once dirname(__FILE__) . '/header.php'; ?>
</head>

<body class="body">

	<?php include 'header-sherry.php'; ?>
    
    <!--<div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:1300px;height:500px;overflow:hidden;visibility:hidden;">
        <!-- Loading Screen -->
        <!--<div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
            <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="../svg/loading/static-svg/spin.svg" />
        </div>
        <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:1300px;height:500px;overflow:hidden;">
            <div>
                <img data-u="image" src="img/banner1.jpg" />
            </div>
            <div>
                <img data-u="image" src="img/banner2.jpg" />
            </div>
            <div>
                <img data-u="image" src="img/banner3.jpg" />
            </div>
        </div>
        <!-- Bullet Navigator -->
        <!--<div data-u="navigator" class="jssorb032" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
            <div data-u="prototype" class="i" style="width:16px;height:16px;">
                <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                </svg>
            </div>
        </div>
        <!-- Arrow Navigator -->
        <!--<div data-u="arrowleft" class="jssora051" style="width:65px;height:65px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
            <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
            </svg>
        </div>
        <div data-u="arrowright" class="jssora051" style="width:65px;height:65px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
            <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
            </svg>
        </div>
    </div>-->
	<div class="width100 same-padding gold-bg-img">
    	<div class="left-gold">
        	<img src="img/boss-product.png" class="product-img" alt="Boss" title="Boss">
        </div>
        <div class="right-gold">
        	<h1><?php echo _MAINJS_INDEX_BANNER ?></h1>
        </div>
    </div>
	<div class="clear"></div>
    <div class="width100 same-padding dark-div padding-top-30px rise-div">
    	<h1 class="gold-text boss-h1"><?php echo _MAINJS_INDEX_RISE ?></h1>
    	<div class="four-info">
        	<img src="img/rise1.png" class="four-info-img">
            <p class="four-info-p"><?php echo _MAINJS_INDEX_INCREASE_ENERGY_LEVEL ?></p>
        </div>
    	<div class="four-info second-four-info mid-four-info third-second">
        	<img src="img/rise2.png" class="four-info-img">
            <p class="four-info-p"><?php echo _MAINJS_INDEX_RESTORING_ALERTNESS ?></p>
        </div>    
    	<div class="four-info mid-four-info2">
        	<img src="img/rise3.png" class="four-info-img">
            <p class="four-info-p"><?php echo _MAINJS_INDEX_ENHANCE_THE_IMMUNE ?></p>
        </div>
    	<div class="four-info second-four-info">
        	<img src="img/rise4.png" class="four-info-img">
            <p class="four-info-p"><?php echo _MAINJS_INDEX_PROMOTE_KIDNEY ?></p>
        </div>  
    	<div class="four-info third-second">
        	<img src="img/rise5.png" class="four-info-img">
            <p class="four-info-p"><?php echo _MAINJS_INDEX_RELIEVE_FATUGUE ?></p>
        </div>        
    	<div class="four-info second-four-info mid-four-info">
        	<img src="img/rise6.png" class="four-info-img">
            <p class="four-info-p"><?php echo _MAINJS_INDEX_PROMOTE_HORMONE ?></p>
        </div>    
    	<div class="four-info mid-four-info2">
        	<img src="img/rise7.png" class="four-info-img">
            <p class="four-info-p"><?php echo _MAINJS_INDEX_PROMOTE_DIGESTION ?></p>
        </div>
    	<div class="four-info second-four-info third-second">
        	<img src="img/rise8.png" class="four-info-img">
            <p class="four-info-p"><?php echo _MAINJS_INDEX_IMPROVE_KIDNEY ?></p>
        </div>   
    	<div class="four-info">
        	<img src="img/rise9.png" class="four-info-img">
            <p class="four-info-p"><?php echo _MAINJS_INDEX_STIMULATING ?></p>
        </div>        
    	<div class="four-info second-four-info mid-four-info">
        	<img src="img/rise10.png" class="four-info-img">
            <p class="four-info-p"><?php echo _MAINJS_INDEX_RECOVERY ?></p>
        </div>    
    	<div class="four-info mid-four-info2 third-second">
        	<img src="img/rise11.png" class="four-info-img">
            <p class="four-info-p"><?php echo _MAINJS_INDEX_REJUVENATE ?></p>
        </div>
    	<div class="four-info second-four-info">
        	<img src="img/rise12.png" class="four-info-img">
            <p class="four-info-p"><?php echo _MAINJS_INDEX_STRENGTHEN_PHYSICAL ?></p>
        </div>                                       
    </div>
    <div class="clear"></div>
    <div class="width100 same-padding gold-bg2">
    	<h1 class="gold-text boss-h1"><?php echo _MAINJS_INDEX_MAIN_FORMULA ?></h1>
        <div class="gold2-first-div">
        	<div class="top-icon-div">
            	<img src="img/coffee.png" class="ingredient-img" alt="<?php echo _MAINJS_INDEX_COFFEE ?>" title="<?php echo _MAINJS_INDEX_COFFEE ?>">
                <h2 class="gold-text ingredient-h2"><?php echo _MAINJS_INDEX_COFFEE ?></h2>
            </div>
            <div class="mid-icon-div">
            	<img src="img/fine-sugar.png" class="ingredient-img" alt="<?php echo _MAINJS_INDEX_FINESUGAR ?>" title="<?php echo _MAINJS_INDEX_FINESUGAR ?>">
                <h2 class="gold-text ingredient-h2"><?php echo _MAINJS_INDEX_FINESUGAR ?></h2>
            </div>
            <div class="last-icon-div">
            	<img src="img/Cynomorium.png" class="ingredient-img" alt="<?php echo _MAINJS_INDEX_CYNOMORIUM ?>" title="<?php echo _MAINJS_INDEX_CYNOMORIUM ?>">
                <h2 class="gold-text ingredient-h2"><?php echo _MAINJS_INDEX_CYNOMORIUM ?></h2>
            </div>           
        </div>
        <div class="gold2-second-div">
        	<img src="img/boss-product2-02.png" alt="Boss" title="Boss" class="mid-product-img">
        </div>
        <div class="gold2-third-div">
         	<div class="top-icon-div2">
            	<img src="img/ginseng.png" class="ingredient-img" alt="<?php echo _MAINJS_INDEX_GINSENG ?>" title="<?php echo _MAINJS_INDEX_GINSENG ?>">
                <h2 class="gold-text ingredient-h2"><?php echo _MAINJS_INDEX_GINSENG ?></h2>
            </div>
            <div class="mid-icon-div mid-icon-div2">
            	<img src="img/tongkat-ali.png" class="ingredient-img" alt="<?php echo _MAINJS_INDEX_TONGKATALI ?>" title="<?php echo _MAINJS_INDEX_TONGKATALI ?>">
                <h2 class="gold-text ingredient-h2"><?php echo _MAINJS_INDEX_TONGKATALI ?></h2>
            </div>
            <div class="last-icon-div2">
            	<img src="img/maca.png" class="ingredient-img" alt="<?php echo _MAINJS_INDEX_MACA ?>" title="<?php echo _MAINJS_INDEX_MACA ?>">
                <h2 class="gold-text ingredient-h2"><?php echo _MAINJS_INDEX_MACA ?></h2>
            </div>         
        </div> 
    </div>
    <div class="clear"></div>
    <div class="width100 same-padding dark-div big-padding">
    	<div class="big-ing-div">
            <div class="width50-ing float-left-50 w50-img">
                <img src="img/cynomorium2.png" class="ing-img">
            </div>
            <div class="width50-ing float-right-50 w50-content-right">
                <h3 class="gold-text ing-h3"><?php echo _MAINJS_INDEX_CYNOMORIUM ?></h3>
                <h4 class="gold-text ing-h4"><?php echo _MAINJS_INDEX_CYNOMORIUM_SCIENCE ?></h4>
                <p class="white-text ing-p"><?php echo _MAINJS_INDEX_CYNOMORIUM_DESC ?></p>
            </div> 
        </div>
        <div class="clear"></div> 
        <div class="big-ing-div">  
            <div class="width50-ing float-right-50 w50-img">
                <img src="img/ginseng2.jpg" class="ing-img">
            </div>
            <div class="width50-ing float-left-50 w50-content-left">
                <h3 class="gold-text ing-h3"><?php echo _MAINJS_INDEX_RADIX_GINSENG ?></h3>
                <p class="white-text ing-p"><?php echo _MAINJS_INDEX_GINSENG_DESC ?></p>
            </div> 
        </div>
        <div class="clear"></div>   
    	<div class="big-ing-div">
            <div class="width50-ing float-left-50 w50-img">
                <img src="img/tongkat-ali2.jpg" class="ing-img">
            </div>
            <div class="width50-ing float-right-50 w50-content-right">
                <h3 class="gold-text ing-h3"><?php echo _MAINJS_INDEX_TONGKATALI ?></h3>
                <h4 class="gold-text ing-h4"><?php echo _MAINJS_INDEX_TONGKATALI_SCIENCE ?></h4>
                <p class="white-text ing-p"><?php echo _MAINJS_INDEX_TONGKATALI_DESC ?></p>
            </div> 
        </div>
        <div class="clear"></div> 
        <div class="big-ing-div">  
            <div class="width50-ing float-right-50 w50-img">
                <img src="img/maca2.jpg" class="ing-img">
            </div>
            <div class="width50-ing float-left-50 w50-content-left">
                <h3 class="gold-text ing-h3"><?php echo _MAINJS_INDEX_MACA ?></h3>
                <h4 class="gold-text ing-h4"><?php echo _MAINJS_INDEX_MACA_SCIENCE ?></h4>
                <p class="white-text ing-p"><?php echo _MAINJS_INDEX_MACA_DESC ?></p>
            </div> 
        </div>
        <div class="clear"></div>                  
    </div>
    <div class="clear"></div>
    <div class="width100 same-padding bg3">
    	<div class="content-div">
            <img src="img/korean-technology.png" alt="<?php echo _MAINJS_INDEX_KOREAN_TECHNOLOGY ?>" title="<?php echo _MAINJS_INDEX_KOREAN_TECHNOLOGY ?>" class="circle-img circle-img1">
            <img src="img/nano-tech.png" alt="<?php echo _MAINJS_INDEX_NANO_TECH ?>" title="<?php echo _MAINJS_INDEX_NANO_TECH ?>" class="circle-img circle-img2">
            <p class="gold-text tech-p"><?php echo _MAINJS_INDEX_KOREAN_DESC ?></p>
		</div>   
    </div>
    <div class="width100 same-padding bg4">
    	<h1 class="gold-text boss-h1"><?php echo _MAINJS_INDEX_CERT ?></h1>
    	<a href="./img/halal.jpg" data-fancybox="images-preview" title="<?php echo _MAINJS_INDEX_HALAL ?>" class="enlarge-a">
        	<div class="three-cert-div">
            	<img src="img/halal.jpg" alt="<?php echo _MAINJS_INDEX_HALAL ?>" title="<?php echo _MAINJS_INDEX_HALAL ?>" class="cert-img">
            	<h3 class="cert-h3 gold-text"><?php echo _MAINJS_INDEX_HALAL ?></h3>
            </div>
        </a>
    	<a href="./img/kgs-cert.jpg" data-fancybox="images-preview" title="<?php echo _MAINJS_INDEX_KGS ?>" class="enlarge-a">
        	<div class="three-cert-div mid-three-cer-div second-cert-div">
            	<img src="img/kgs-cert.jpg" alt="<?php echo _MAINJS_INDEX_KGS ?>" title="<?php echo _MAINJS_INDEX_KGS ?>" class="cert-img">
                <h3 class="cert-h3 gold-text"><?php echo _MAINJS_INDEX_KGS ?></h3>
            </div>
        </a>    
        <div class="two-div-clear"></div>
    	<a href="./img/charterprime.jpg" data-fancybox="images-preview" title="<?php echo _MAINJS_INDEX_CHARTER ?>" class="enlarge-a">
        	<div class="three-cert-div">
            	<img src="img/charterprime.jpg" alt="<?php echo _MAINJS_INDEX_CHARTER ?>" title="<?php echo _MAINJS_INDEX_CHARTER ?>" class="cert-img">
                <h3 class="cert-h3 gold-text"><?php echo _MAINJS_INDEX_CHARTER ?></h3>
            </div>
        </a>  
        <div class="cert-tempo-clear"></div>
    	<a href="./img/free-sale-export-cert.jpg" data-fancybox="images-preview" title="<?php echo _MAINJS_INDEX_FREE_SALES ?>" class="enlarge-a">
        	<div class="three-cert-div two-cert-div two-cert-div1  second-cert-div">
            	<img src="img/free-sale-export-cert.jpg" alt="<?php echo _MAINJS_INDEX_FREE_SALES ?>" title="<?php echo _MAINJS_INDEX_FREE_SALES ?>" class="cert-img">
                <h3 class="cert-h3 gold-text"><?php echo _MAINJS_INDEX_FREE_SALES ?></h3>
            </div>
        </a>     
    	<a href="./img/akta-makanan.jpg" data-fancybox="images-preview" title="<?php echo _MAINJS_INDEX_FOOD_CERT ?>" class="enlarge-a">
        	<div class="three-cert-div two-cert-div two-cert-div2">
            	<img src="img/akta-makanan.jpg" alt="<?php echo _MAINJS_INDEX_FOOD_CERT ?>" title="<?php echo _MAINJS_INDEX_FOOD_CERT ?>" class="cert-img">
            	<h3 class="cert-h3 gold-text"><?php echo _MAINJS_INDEX_FOOD_CERT ?></h3>
            </div>
        </a> 
        <div class="clear"></div>
        <div class="width100">
        	<h1 class="gold-text boss-h1"><?php echo _MAINJS_INDEX_REMINDER ?></h1>
            <div class="wn50">
            	<div class="reminder-img-div">
                	<img src="img/reminder.png" class="width100" alt="<?php echo _MAINJS_INDEX_CONSUMER_R ?>" title="<?php echo _MAINJS_INDEX_CONSUMER_R ?>">
                </div>
                <h3 class="gold-text reminder-h3"><?php echo _MAINJS_INDEX_CONSUMER_R ?> :</h3>
                <p class="white-text reminder-p"><?php echo _MAINJS_INDEX_CONSUMER_D ?></p>
            </div>
            <div class="wn50 wn50-2">
            	<div class="reminder-img-div">
                	<img src="img/disclaimer.png" class="width100" alt="<?php echo _MAINJS_INDEX_DISCLAIMER ?>" title="<?php echo _MAINJS_INDEX_DISCLAIMER ?>">
                </div>
                <h3 class="gold-text reminder-h3"><?php echo _MAINJS_INDEX_DISCLAIMER ?> :</h3>
                <p class="white-text reminder-p"><?php echo _MAINJS_INDEX_DISCLAIMER_D ?></p>
            </div>            
        </div>                       
    </div>



<div class="clear"></div>
<div class="width100 same-padding dark-div">
	<h1 class="gold-text text-center"><?php echo _MAINJS_INDEX_CONTACT_US ?></h1>
    <div class="boss-left-contact-div">
    	<img src="img/boss.png" alt="Boss" title="Boss" class="contact-logo">	
    	<!--<p class="white-text contact-p-des">
        	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum porttitor rutrum neque, quis ultrices lectus posuere eu. Suspendisse potenti. Vestibulum at metus tincidunt turpis finibus iaculis. Sed scelerisque faucibus massa.
        </p>-->
        <table class="contact-table">
        	<!--<tr>
            	<td><?php echo _MAINJS_INDEX_CONTACT_NO ?></td>
                <td>:</td>
                <td>010-XXX XXXX</td>
            </tr>-->
        	<tr>
            	<td><?php echo _MAINJS_INDEX_EMAIL ?></td>
                <td>:</td>
                <td>enquiry@bossinternational.asia</td>
            </tr>   
        	<!--<tr>
            	<td><?php echo _MAINJS_INDEX_ADDRESS ?></td>
                <td>:</td>
                <td>No., Street, City, Country</td>
            </tr>-->                  	
        </table>
        <p class="social-p">
        	<a href="#"><img src="img/social-fb.png" alt="Facebook" title="Facebook" class="social-img hover-opacity"></a>
            <a href="#"><img src="img/social-insta.png" alt="Instagram" title="Instagram" class="social-img social-img2 hover-opacity"></a>
        </p>
        <div class="map-div">
        	<iframe class="map-iframe" src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d15890.105904995818!2d100.4288273!3d5.336281149999999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2smy!4v1576832065666!5m2!1sen!2smy"  frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </div>
    </div>
    <div class="boss-right-contact-div">
    	        <form id="contactform" method="post" action="index.php" class="form-class"  >
                  
                  <input type="text" name="name" placeholder="<?php echo _MAINJS_INDEX_NAME ?>" class="input-name clean" required >
                  
                  <input type="email" name="email" placeholder="<?php echo _MAINJS_INDEX_EMAIL ?>" class="input-name clean" required >                  
                  
                  <input type="text" name="telephone" placeholder="<?php echo _MAINJS_INDEX_CONTACT_NO ?>" class="input-name clean" required >


                                  
                  <textarea name="comments" placeholder="Message" class="input-name input-message clean" ></textarea>
                  <div class="clear"></div>
                  <input type="radio" name="contact-option" value="contact-more-info" class="radio1 float-left clean" required><p class="opt-msg left"> <?php echo _MAINJS_INDEX_TO_BE_UPDATED ?></p>
                  <div class="clear"></div>
                  <input type="radio" name="contact-option" value="contact-on-request" class="radio1 float-left clean"  required><p class="opt-msg left"> <?php echo _MAINJS_INDEX_MY_REQUEST_ONLY ?></p>
                  <div class="clear"></div>
                   
                  <div class="res-div"><input type="submit" name="send_email_button" value="<?php echo _MAINJS_INDEX_SEND_US_A_MESSAGE ?>" class="input-submit white-text clean pointer"></div>
                </form> 
    </div>
</div>

<!--- Contact Form --->


<!-- CSS -->
<style>
.food-gif{
	width:100px;
	position:absolute;
	top:calc(50% - 150px);
	text-align:center;
}
.center-food{
	width:100%;
	text-align:center;
	margin-left:-50px;}
#container{
	margin-top:-20px;}
#overlay{
  position:fixed;
  z-index:99999;
  top:0;
  left:0;
  bottom:0;
  right:0;
  background:#7cd1d1;
  /*background: -moz-linear-gradient(left, #a9151c 0%, #d60d26 100%);
  background: -webkit-linear-gradient(left, #a9151c 0%,#d60d26 100%);
  background: linear-gradient(to right, #a9151c 0%,#d60d26 100%);*/
  transition: 1s 0.4s;
}
#progress{
  height:1px;
  background:#fff;
  position:absolute;
  width:0;
  top:50%;
}
#progstat{
  font-size:0.7em;
  letter-spacing: 3px;
  position:absolute;
  top:50%;
  margin-top:-40px;
  width:100%;
  text-align:center;
  color:#fff;
}
@media all and (max-width: 500px){
.food-gif{
	width:60px;
	top:calc(50% - 120px);
	text-align:center;
}
.center-food{
	margin-left:-30px;}	

}
</style>

<?php

if( $_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['send_email_button'])) {

    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = "enquiry@bossinternational.asia";
    $email_subject = "Contact Form via Boss website";
 
    function died($error) 
	{
        // your error code can go here
		echo '<script>alert("We are very sorry, but there were error(s) found with the form you submitted.\n\nThese errors appear below.\n\n';
		echo $error;
        echo '\n\nPlease go back and fix these errors.\n\n")</script>';
        die();
    }
 
 
    // validation expected data exists
    if(!isset($_POST['name']) ||
        !isset($_POST['email']) ||
		!isset($_POST['telephone'])) {
        died('We are sorry, but there appears to be a problem with the form you submitted.');       
    }
	
     
 
    $first_name = $_POST['name']; // required
    $email_from = $_POST['email']; // required
	$telephone = $_POST['telephone']; //required
    $comments = $_POST['comments']; 
    $contactOption = $_POST['contact-option']; // required
    $contactMethod = null;
	
	//$error_message = '<script>alert("The name you entered does not appear to be valid.");</script>';
	//if($first_name == ""){
	//	echo $error_message;
	//}

    if($contactOption == null || $contactOption == ""){
        $contactMethod = "don\'t bother me";
    }else if($contactOption == "contact-more-info"){
        $contactMethod = "I want to be contacted with more information about your company's offering marketing services and consulting";
    }else if($contactOption == "contact-on-request"){
        $contactMethod = "I just want to be contacted based on my request/ inquiry";
    }else{
        $contactMethod = "error getting contact options";
		$error_message .="Error getting contact options\n\n";
    }

    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
 
  if(!preg_match($email_exp,$email_from)) {
    $error_message .= 'The email address you entered does not appear to be valid.\n';
  }
 
 
    $string_exp = "/^[A-Za-z .'-]+$/";
 
  if(!preg_match($string_exp,$first_name)) {
    $error_message .= 'The name you entered does not appear to be valid.\n';
  }
 


 
  if(strlen($error_message) > 0) {
    died($error_message);
  }
 
    $email_message = "Form details below.\n\n";
 
     
    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }
 
    $email_message .= "Name: ".clean_string($first_name)."\n";
    $email_message .= "Email: ".clean_string($email_from)."\n";
	$email_message .= "Telephone: ".clean_string($telephone)."\n";   
    $email_message .= "Message : ".clean_string($comments)."\n";
    $email_message .= "Contact Option : ".clean_string($contactMethod)."\n";

// create email headers
$headers = 'From: '.$email_from."\r\n".
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, $email_subject, $email_message, $headers);  
echo '<script>alert("Thank you! We will be in contact shortly!")</script>';

?>
<!-- include your own success html here -->

<!--Thank you for contacting us. We will be in touch with you very soon.-->
<?php
 
}
?>
<?php include 'js.php'; ?>
<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            // $messageType = "Successfully register your profile! Please confirm your registration inside of your email.";
            $messageType = "Register Successfully!";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "There are no referrer with this email ! Please register again";
        }
        else if($_GET['type'] == 3)
        {
            // $messageType = "Successfully register your profile! Please confirm your registration inside of your email.";
            $messageType = "Register Successfully!";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "User password must be more than 5 !";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "User password does not match";
        }
        else if($_GET['type'] == 6)
        {
            $messageType = "Wrong email format.";
        }
        else if($_GET['type'] == 7)
        {
            $messageType = "There are no user with this email ! Please try again.";
        }
        else if($_GET['type'] == 8)
        {
            $messageType = "Successfully reset your password! Please check your email.";
        }
        else if($_GET['type'] == 9)
        {
            $messageType = "Successfully reset your password! ";
        }
        else if($_GET['type'] == 10)
        {
            $messageType = "Please confirm your registration inside your email! ";
        }
        else if($_GET['type'] == 11)
        {
            $messageType = "Incorrect email or password! ";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
if(isset($_GET['promptError']))
{
    $messageType = null;

    if($_GET['promptError'] == 1)
    {
        $messageType = "Error registering new account.The account already exist";
    }
    else if($_GET['promptError'] == 2)
    {
        $messageType = "Error assigning referral relationship. Please register again.";
    }
    echo '
    <script>
        putNoticeJavascript("Notice !! ","'.$messageType.'");
    </script>
    ';   
}
?>
<script src="js/jssor.slider.min.js" type="text/javascript"></script>
<script type="text/javascript">
        jssor_1_slider_init = function() {

            var jssor_1_options = {
              $AutoPlay: 1,
              $SlideDuration: 800,
              $SlideEasing: $Jease$.$OutQuint,
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 3000;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script>
    
    
    <script type="text/javascript">jssor_1_slider_init();</script>
    <!-- #endregion Jssor Slider End -->

</body>
</html>