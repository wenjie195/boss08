<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$productsOrders =  getProductOrders($conn);

$conn->close();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://bossinternational.asia/shippingOut.php" />
    <meta property="og:title" content="Shipping Out | Boss" />
    <title>Shipping Out | Boss</title>
    <meta property="og:description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="description" content="Impotence still happens on any race of men. The BOSS product was developed by the Korean Pharmaceutical Laboratory through several clinical trials and packaging by FCT IMPORT. South Korea is one of the top ten drug research countries in the world." />
    <meta name="keywords" content="Impotence, Boss, man, men sex, sexual, cure, product, unable to ejaculate, Penile Erectile Dysfunction, Sexual Desire Disorder, Sexual Intercourse Disorder, low sexual desire,阳痿,性冷淡,性功能障碍,不举,  etc">
    <link rel="canonical" href="https://bossinternational.asia/shippingOut.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php include 'header-sherry.php'; ?>

<div class="yellow-body padding-from-menu same-padding">

    <h1 class="details-h1" onclick="goBack()">
        <a class="black-white-link2 hover1">
            <img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">
            <img src="img/back2.png" class="back-btn2 hover1b" alt="back" title="back">
            Order Number : #<?php echo $_POST['order_id'];?>
        </a>
    </h1>


    <table class="details-table">
    	<tbody>
        <?php
        $conn = connDB();
        if(isset($_POST['order_id']))
        {
            // $conn = connDB();
            //Order
            $orderArray = getOrders($conn,"WHERE id = ? ", array("id") ,array($_POST['order_id']),"i");
            //OrderProduct
            $orderProductArray = getProductOrders($conn,"WHERE order_id = ? ", array("order_id") ,array($_POST['order_id']),"i");
            //$orderDetails = $orderArray[0];

            if($orderArray != null)
            {?>

                <tr>
                    <td>Name</td>
                    <td>:</td>
                    <td><?php echo $orderArray[0]->getName()?></td>
                </tr>
                <tr>
                    <td>Contact</td>
                    <td>:</td>
                    <td><?php echo $orderArray[0]->getContactNo()?></td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td>:</td>
                    <td><?php echo $orderArray[0]->getShippingStatus()?></td>
                </tr>
                <tr>
                    <td>Ship To</td>
                    <td>:</td>
                    <td>
                        <?php echo $orderArray[0]->getAddressLine1()?>
                        <?php echo $orderArray[0]->getAddressLine2()?>
                        <?php echo $orderArray[0]->getCity()?>
                        <?php echo $orderArray[0]->getZipcode()?>
                        <?php echo $orderArray[0]->getState()?>
                        <?php echo $orderArray[0]->getCountry()?>
                    </td>
                </tr>
                <tr>
                    <td>Method</td>
                    <td>:</td>
                    <td><?php echo $orderArray[0]->getShippingMethod()?></td>
                </tr>
                <tr>
                    <td>Tracking No.</td>
                    <td>:</td>
                    <td>#<?php echo $orderArray[0]->getTrackingNumber()?></td>
                </tr>
                <tr>
                    <td>Ship Out</td>
                    <td>:</td>
                    <td><?php echo $orderArray[0]->getShippingDate()?></td>
                </tr>
                <!-- <tr>
                    <td>Photo</td>
                    <td>:</td>
                    <td></td>
                </tr> -->

                <?php

            }
        }
        else
        {}
        $conn->close();
        ?>
        </tbody>
    </table>


    <div class="width100 shipping-div2">
    	<div class="overflow-scroll-div">
            <table class="shipping-table">
                    <thead>
                        <tr>
                            <th>NO.</th>
                            <th>PRODUCT NAME</th>
                            <th>QUANTITY</th>
                            <th>PRICE (RM)</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $conn = connDB();
                    if(isset($_POST['order_id']))
                    {
                        // $conn = connDB();
                        //Order
                        $orderArray = getOrders($conn,"WHERE id = ? ", array("id") ,array($_POST['order_id']),"i");
                        //OrderProduct
                        $orderProductArray = getProductOrders($conn,"WHERE order_id = ? ", array("order_id") ,array($_POST['order_id']),"i");
                        //$orderDetails = $orderArray[0];

    


                        if($orderArray != null)
                        {
                            for($cntAA = 0;$cntAA < count($orderProductArray) ;$cntAA++)
                            {
                                
                            $product = getProduct($conn ,"WHERE id = ? ", array("name"),array($orderProductArray[$cntAA]->getProductId()),"i");    
                                ?>
                            <tr>
                                <!-- <input type="hidden" name="order_id" value="<?php //echo $_POST['order_id'];?>">  -->
                                <td><?php echo ($cntAA+1)?></td>
                                <td><?php echo $product[0] -> getName(); ?></td>
                                <td><?php echo $orderProductArray[$cntAA]->getQuantity();?></td>
                                <td><?php echo $orderProductArray[$cntAA]->getTotalProductPrice();?></td>
                            </tr>
                            <?php
                            }
                        }


                    }
                    else
                    {}
                    $conn->close();
                    ?>
                    </tbody>
            </table>

            <table class="details-table details-table2">
                <tbody>
                    <?php
                    $conn = connDB();
                    if(isset($_POST['order_id']))
                    {
                        //Order
                        $orderArray = getOrders($conn,"WHERE id = ? ", array("id") ,array($_POST['order_id']),"i");
                        //OrderProduct
                        $orderProductArray = getProductOrders($conn,"WHERE order_id = ? ", array("order_id") ,array($_POST['order_id']),"i");
                        //$orderDetails = $orderArray[0];

                        if($orderArray != null)
                        {?>
                            <tr>
                                <td>Total</td>
                                <td>:</td>
                                <td>RM <?php echo $orderArray[0]->getSubtotal()?></td>
                            </tr>
                            <tr>
                                <td>Payment Method</td>
                                <td>:</td>
                                <td><?php echo $orderArray[0]->getPaymentMethod()?></td>
                            </tr>
                            <tr>
                                <td>Payment Amount</td>
                                <td>:</td>
                                <td>RM <?php echo $orderArray[0]->getPaymentAmount()?></td>
                            </tr>
                            <tr>
                                <td>Payment Reference</td>
                                <td>:</td>
                                <td><?php echo $orderArray[0]->getPaymentBankReference()?></td>
                            </tr>
                            <!-- <tr>
                                <td>Note</td>
                                <td>:</td>
                                <td></td>
                            </tr> -->
                            <tr>
                                <td>Payment Date</td>
                                <td>:</td>
                                <td>
                                    <?php $dateCr = date("Y-m-d",strtotime($orderArray[0]->getPaymentDate()));echo $dateCr;?>
                                </td>
                                <!-- <td><?php //echo $orderArray[0]->getShippingDate()?></td> -->
                            </tr>
                            <!-- <tr>
                                <td>Receipt</td>
                                <td>:</td>
                                <td></td>
                            </tr> -->
                            <?php
                        }
                    }
                    else
                    {}
                        $conn->close();
                    ?>
                </tbody>
            </table>

        </div>
    </div>

  
    <div class="clear"></div>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<script>
function goBack() {
  window.history.back();
}
</script>

</body>
</html>
